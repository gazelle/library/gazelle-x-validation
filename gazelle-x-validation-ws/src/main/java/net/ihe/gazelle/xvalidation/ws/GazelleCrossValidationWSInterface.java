/*
 * Copyright 2015 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package net.ihe.gazelle.xvalidation.ws;

import net.ihe.gazelle.xvalidation.core.exception.GazelleXValidationException;
import net.ihe.gazelle.xvalidation.core.model.GazelleCrossValidatorType;
import net.ihe.gazelle.xvalidation.dao.GazelleCrossValidatorTypeDAO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.soap.MTOM;
import java.util.List;

@javax.ejb.Remote
@MTOM
@WebService(name = "GazelleCrossValidationWS", serviceName = "GazelleCrossValidationWSService", portName = "GazelleCrossValidationWSPort", targetNamespace = "ws.xvalidation.gazelle.ihe.net")
public interface GazelleCrossValidationWSInterface {

	@WebMethod
	@WebResult(name = "Validators")
	public ValidatorListWrapper getAvailableValidators(@WebParam(name = "descriminator") String descriminator);

	@WebMethod
	@WebResult(name = "Result")
	public String validate(@WebParam(name = "files") FileListWrapper files, @WebParam(name="validator") String validator);
}
