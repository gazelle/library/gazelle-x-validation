package net.ihe.gazelle.xvalidation.ws;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.xml.bind.JAXBException;

import net.ihe.gazelle.assertion.ws.WsInterface;
import net.ihe.gazelle.assertion.ws.coverage.provider.data.CoveringEntity;
import net.ihe.gazelle.assertion.ws.coverage.provider.data.WsAssertion;
import net.ihe.gazelle.assertion.ws.coverage.provider.data.WsAssertionWrapper;
import net.ihe.gazelle.xvalidation.core.model.Assertion;
import net.ihe.gazelle.xvalidation.dao.AssertionDAO;

import org.jboss.seam.annotations.Name;
import org.jboss.seam.contexts.Lifecycle;

@Stateless
@Name("AssertionCoverageProvider")
public class AssertionCoverageProvider implements WsInterface {

	@Override
	public WsAssertion getATestAssertionCoverage(String idScheme, String assertionId) throws JAXBException {
		Lifecycle.beginCall();
		List<CoveringEntity> entities = AssertionDAO.instanceWithDefaultEntityManager()
				.getCoveringEntitiesForAssertion(idScheme, assertionId);
		Lifecycle.endCall();
		return new WsAssertion(idScheme, assertionId, entities);
	}

	@Override
	public WsAssertionWrapper getTestAssertionCoverageByIdScheme(String idScheme) throws JAXBException {
		Lifecycle.beginCall();
		List<Assertion> assertions = AssertionDAO.instanceWithDefaultEntityManager().getAssertionsByIdScheme(idScheme);
		if (assertions != null) {
			List<WsAssertion> assertionsToReturn = new ArrayList<WsAssertion>();
			for (Assertion assertion : assertions) {
				List<CoveringEntity> entities = AssertionDAO.instanceWithDefaultEntityManager()
						.getCoveringEntitiesForAssertion(assertion.getIdScheme(), assertion.getAssertionId());
				assertionsToReturn.add(new WsAssertion(assertion.getIdScheme(), assertion.getAssertionId(), entities));
			}
			Lifecycle.endCall();
			return new WsAssertionWrapper(assertionsToReturn);
		} else {
			Lifecycle.endCall();
			return null;
		}
	}

	@Override
	public WsAssertionWrapper getTestAssertionsCovered() throws JAXBException {
		return getTestAssertionCoverageByIdScheme(null);
	}

}
