package net.ihe.gazelle.xvalidation.ws;

import net.ihe.gazelle.xvalidation.core.exception.GazelleXValidationException;
import net.ihe.gazelle.xvalidation.core.model.GazelleCrossValidatorType;
import net.ihe.gazelle.xvalidation.dao.GazelleCrossValidatorTypeDAO;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.security.Identity;

import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.soap.MTOM;
import java.util.List;

@Stateless
@Name("GazelleCrossValidationWS")
@MTOM
@WebService(name = "GazelleCrossValidationWS", serviceName = "GazelleCrossValidationWSService", portName =
      "GazelleCrossValidationWSPort", targetNamespace = "ws.xvalidation.gazelle.ihe.net")
public class GazelleCrossValidationWS implements GazelleCrossValidationWSInterface {

   @WebMethod
   @WebResult(name = "Validators")
   public ValidatorListWrapper getAvailableValidators(@WebParam(name = "descriminator") String descriminator) {
      List<GazelleCrossValidatorType> validators = GazelleCrossValidatorTypeDAO.instanceWithDefaultEntityManager()
            .getValidatorsAvailableForTesting(descriminator);
      ValidatorListWrapper wrapper = new ValidatorListWrapper();
      if (validators != null && !validators.isEmpty()) {
         for (GazelleCrossValidatorType validatorType : validators) {
            wrapper.addValidator(validatorType.getName());
         }
      }
      return wrapper;
   }

   @WebMethod
   @WebResult(name = "Result")
   public String validate(@WebParam(name = "files") FileListWrapper fileListWrapper,
                          @WebParam(name = "validator") String validatorName) {
      if (validatorName == null || validatorName.isEmpty()) {
         return "You need to provide the name of a validator";
      } else if (fileListWrapper == null || fileListWrapper.getFilesToValidate() == null || fileListWrapper.getFilesToValidate()
            .isEmpty()) {
         return "You need to provide the files to be validated";
      } else {
         if (Identity.instance().getUsername()==null) {
            Identity.instance().setUsername("webservice");
         }
         GazelleCrossValidationService service = new GazelleCrossValidationService(Identity.instance().getUsername());

         GazelleCrossValidatorType validator = service.getValidatorByName(validatorName);

         try {
            return service.validate(
                  validator,
                  service.buildValidationInputs(
                        fileListWrapper,
                        service.getValidatorInputsForValidator(validator)
                  )
            );
         } catch (GazelleXValidationException e) {
            throw new RuntimeXValidationException(e);
         }
      }
   }

}
