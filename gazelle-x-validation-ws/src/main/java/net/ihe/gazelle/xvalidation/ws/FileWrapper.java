/*
 * Copyright 2015 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package net.ihe.gazelle.xvalidation.ws;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.xml.bind.annotation.XmlAttachmentRef;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.File;
import java.nio.charset.Charset;

/**
 * <b>Class Description : </b>FileWrapper<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 24/08/15
 * @class FileWrapper
 * @package net.ihe.gazelle.xvalidation.ws
 * @see anne-gaelle.berge@ihe-europe.net - http://gazelle.ihe.net
 */

@XmlRootElement
public class FileWrapper {

    private String reference;

    private byte[] document;

    public FileWrapper(String reference, String document){
        this.reference = reference;
        setDocument(document.getBytes(Charset.forName("UTF-8")));
    }

    public FileWrapper(String reference, byte[] document){
        this.reference = reference;
        this.document = document;
    }

    public FileWrapper(){

    }

    public byte[] getDocument() {
        return document;
    }

    public void setDocument(byte[] document) {
        this.document = document;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }
}
