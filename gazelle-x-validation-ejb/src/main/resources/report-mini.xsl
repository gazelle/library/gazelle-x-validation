<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">
    <xsl:output encoding="UTF-8" indent="yes" method="html"
                omit-xml-declaration="yes"/>
    <xsl:param name="assertionManagerPath">
        https://gazelle.ihe.net/AssertionManagerGui
    </xsl:param>
    <xsl:param name="xvalidationPath">
        http://localhost:8080/GazelleXValidatorRuleEditor
    </xsl:param>
    <xsl:template match="/">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Validator description</h3>
            </div>
            <div class="panel-body">
                <xsl:apply-templates
                        select="GazelleCrossValidationReport/ReportSummary/GazelleCrossValidator"
                />
            </div>
        </div>
        <br/>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Validation summary</h3>
            </div>
            <div class="panel-body">
                <xsl:apply-templates select="GazelleCrossValidationReport/ReportSummary"/>
            </div>
        </div>
        <br/>
        <xsl:if test="count(GazelleCrossValidationReport/XSDValidation) > 0">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">XSD Validation</h3>
                </div>
                <div class="panel-body">
                    <xsl:for-each select="GazelleCrossValidationReport/XSDValidation">
                        <xsl:apply-templates select="current()"/>
                    </xsl:for-each>
                </div>
            </div>
            <br/>
        </xsl:if>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Validation details</h3>
            </div>
            <div class="panel-body">
                <xsl:if test="count(GazelleCrossValidationReport/Notifications/Error)  &gt; 0">
                    <h3>Errors</h3>
                    <xsl:for-each select="GazelleCrossValidationReport/Notifications/Error">
                        <xsl:call-template name="Notification">
                            <xsl:with-param name="class">
                                <xsl:text>gzl-notification-red</xsl:text>
                            </xsl:with-param>
                        </xsl:call-template>
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="count(GazelleCrossValidationReport/Notifications/Warning)  &gt; 0">
                    <h3>Warnings</h3>
                    <xsl:for-each select="GazelleCrossValidationReport/Notifications/Warning">
                        <xsl:call-template name="Notification">
                            <xsl:with-param name="class">
                                <xsl:text>gzl-notification-orange</xsl:text>
                            </xsl:with-param>
                        </xsl:call-template>
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="count(GazelleCrossValidationReport/Notifications/Info)  &gt; 0">
                    <h3>Infos</h3>
                    <xsl:for-each select="GazelleCrossValidationReport/Notifications/Info">
                        <xsl:call-template name="Notification">
                            <xsl:with-param name="class">
                                <xsl:text>gzl-notification-blue</xsl:text>
                            </xsl:with-param>
                        </xsl:call-template>
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="count(GazelleCrossValidationReport/Notifications/Aborted)  &gt; 0">
                    <h3>Aborted rules</h3>
                    <xsl:for-each select="GazelleCrossValidationReport/Notifications/Aborted">
                        <xsl:call-template name="Notification">
                            <xsl:with-param name="class">
                                <xsl:text>gzl-notification-orange</xsl:text>
                            </xsl:with-param>
                        </xsl:call-template>
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="count(GazelleCrossValidationReport/Notifications/Report)  &gt; 0">
                    <h3>Reports</h3>
                    <xsl:for-each select="GazelleCrossValidationReport/Notifications/Report">
                        <xsl:call-template name="Notification">
                            <xsl:with-param name="class">
                                <xsl:text>gzl-notification-green</xsl:text>
                            </xsl:with-param>
                        </xsl:call-template>
                    </xsl:for-each>
                </xsl:if>
            </div>
        </div>
    </xsl:template>
    <xsl:template match="GazelleCrossValidator">
        <dl class="dl-horizontal">
            <dt>Name</dt>
            <dd>
                <xsl:value-of select="CalledValidator/@name"/>
            </dd>
            <dt>Affinity domain</dt>
            <dd>
                <xsl:value-of select="CalledValidator/@affinityDomain"/>
            </dd>
            <dt>Version</dt>
            <dd>
                <xsl:value-of select="CalledValidator/@version"/>
            </dd>
            <xsl:if test="count(LinkedValidator) > 0">
                <dt>Parent validators</dt>
                <dd>
                    <ul>
                        <xsl:for-each select="LinkedValidator">
                            <li>
                                <xsl:value-of select="@affinityDomain"/>
                                <xsl:text> - </xsl:text>
                                <xsl:value-of select="@name"/>
                                <xsl:text> (</xsl:text>
                                <xsl:value-of select="@version"/>
                                <xsl:text>)</xsl:text>
                            </li>
                        </xsl:for-each>
                    </ul>
                </dd>
            </xsl:if>
            <dt>Gazelle X Validator version</dt>
            <dd>
                <xsl:value-of select="@engineVersion"/>
            </dd>
        </dl>
    </xsl:template>
    <xsl:template match="ReportSummary">
        <dl class="dl-horizontal">
            <dt>Validation date</dt>
            <dd>
                <xsl:value-of select="ValidationDate"/>
            </dd>
            <dt>Validation status</dt>
            <dd>
                <xsl:if test="contains(ValidationResult, 'PASSED')">
                    <span class="gzl-text-green">
                        <xsl:value-of select="ValidationResult"/>
                    </span>
                </xsl:if>
                <xsl:if test="contains(ValidationResult, 'FAILED')">
                    <span class="gzl-text-red">
                        <xsl:value-of select="ValidationResult"/>
                    </span>
                </xsl:if>
                <xsl:if test="contains(ValidationResult, 'ABORTED')">
                    <span class="gzl-text-blue">
                        <xsl:value-of select="ValidationResult"/>
                    </span>
                </xsl:if>
            </dd>
        </dl>
    </xsl:template>
    <xsl:template match="XSDValidation">
        <dl class="dl-horizontal">
            <dt>File keyword</dt>
            <dd>
                <xsl:value-of select="@keyword"/>
            </dd>
            <xsl:if test="count(@file) > 0">
                <dt>File name</dt>
                <dd>
                    <xsl:value-of select="@file"/>
                </dd>
            </xsl:if>
            <dt>XSD</dt>
            <dd>
                <xsl:value-of select="@xsdLocation"/>
            </dd>
            <dt>Validation status</dt>
            <dd>
                <xsl:if test="contains(Result, 'PASSED')">
                    <span class="gzl-text-green">
                        <xsl:value-of select="Result"/>
                    </span>
                </xsl:if>
                <xsl:if test="contains(Result, 'FAILED')">
                    <span class="gzl-text-red">
                        <xsl:value-of select="Result"/>
                    </span>
                </xsl:if>
                <xsl:if test="contains(Result, 'ABORTED')">
                    <span class="gzl-text-blue">
                        <xsl:value-of select="Result"/>
                    </span>
                </xsl:if>
            </dd>
        </dl>
        <ul>
            <xsl:if test="count(Detail) > 0">
                <xsl:for-each select="Detail[@severity='error']">
                    <li>
                        <xsl:text>ERROR: </xsl:text>
                        <xsl:apply-templates select="current()"/>
                    </li>
                </xsl:for-each>
            </xsl:if>
            <xsl:if test="count(Detail) > 0">
                <xsl:for-each select="Detail[@severity='warning']">
                    <li>
                        <xsl:text>WARNING: </xsl:text>
                        <xsl:apply-templates select="current()"/>
                    </li>
                </xsl:for-each>
            </xsl:if>
        </ul>
    </xsl:template>
    <xsl:template match="Detail">
        <xsl:value-of select="Message"/>
        <xsl:text> [line: </xsl:text>
        <xsl:value-of select="@lineNumber"/>
        <xsl:text>, col: </xsl:text>
        <xsl:value-of select="@colNumber"/>
        <xsl:text>]</xsl:text>
    </xsl:template>
    <xsl:template name="Notification">
        <xsl:param name="class"/>
        <xsl:element name="div">
            <xsl:attribute name="class">
                <xsl:text>gzl-notification </xsl:text>
                <xsl:value-of select="$class"/>
            </xsl:attribute>
            <dl class="dl-horizontal">
                <dt>
                    Test
                </dt>
                <dd>
                    <xsl:value-of select="Test"/>
                </dd>
                <xsl:if test="count(ProcessedInput) > 0">
                    <dt>
                        Applies to
                    </dt>
                    <dd>
                        <xsl:for-each select="ProcessedInput">
                            <xsl:value-of select="text()"/>
                            <xsl:text> </xsl:text>
                        </xsl:for-each>
                    </dd>
                </xsl:if>
                <dt>
                    Description
                </dt>
                <dd>
                    <xsl:value-of select="Description"/>
                </dd>
                <dt>
                    Tested expression
                </dt>
                <dd>
                    <code><xsl:value-of select="Expression"/></code>
                </dd>

                <xsl:if test="count(Assertion) > 0">
                    <dt>
                        Covered assertions
                    </dt>
                    <dd>
                        <ul>
                            <xsl:for-each select="Assertion">
                                <li>
                                    <xsl:value-of select="@assertionId"/>
                                    <xsl:text> (</xsl:text>
                                    <xsl:value-of select="@idScheme"/>
                                    <xsl:text>) </xsl:text>
                                </li>
                            </xsl:for-each>
                        </ul>
                    </dd>
                </xsl:if>
            </dl>
        </xsl:element>
    </xsl:template>
</xsl:stylesheet>
