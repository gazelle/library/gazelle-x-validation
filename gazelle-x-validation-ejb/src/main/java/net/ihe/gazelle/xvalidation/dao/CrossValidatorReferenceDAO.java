package net.ihe.gazelle.xvalidation.dao;

import net.ihe.gazelle.xvalidation.core.model.CrossValidatorReference;
import net.ihe.gazelle.xvalidation.core.model.CrossValidatorReferenceQuery;
import net.ihe.gazelle.xvalidation.core.model.GazelleCrossValidatorType;

import javax.persistence.EntityManager;

public class CrossValidatorReferenceDAO extends DAO<CrossValidatorReference> {

	public CrossValidatorReferenceDAO() {
		super();
	}

	public CrossValidatorReferenceDAO(EntityManager entityManager) {
		super(entityManager);
	}

	public static CrossValidatorReferenceDAO instanceWithDefaultEntityManager() {
		return new CrossValidatorReferenceDAO();
	}

	@Override
	public Class<CrossValidatorReference> getEntityClass() {
		return CrossValidatorReference.class;
	}

	public CrossValidatorReference getReferenceToValidator(GazelleCrossValidatorType parent) {
		CrossValidatorReferenceQuery query = new CrossValidatorReferenceQuery();
		query.name().eq(parent.getName());
		query.affinityDomain().eq(parent.getAffinityDomain());
		CrossValidatorReference reference = query.getUniqueResult();
		if (reference == null){
			reference = new CrossValidatorReference(parent);
			reference = saveEntity(reference);
		}
		return reference;
	}

	public CrossValidatorReference getCrossValidatorReferenceByName(String referenceName) {
		CrossValidatorReferenceQuery query = new CrossValidatorReferenceQuery();
		query.name().eq(referenceName);
		return query.getUniqueResult();
	}

}
