package net.ihe.gazelle.xvalidation.model;

import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.xvalidation.core.report.ValidationResultType;
import net.ihe.gazelle.xvalidation.menu.XValidationPages;

import org.apache.commons.lang.RandomStringUtils;
import org.hibernate.annotations.Type;
import org.jboss.seam.security.Identity;

@Entity
@Table(name = "xval_cross_validation_log", schema = "public")
@SequenceGenerator(name = "xval_cross_validation_log_sequence", sequenceName = "xval_cross_validation_log_id_seq", allocationSize = 1)
public class CrossValidationLog implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 2478069673585129989L;
	private static final int PRIVACY_KEY_LENGTH = 16;

	@Id
	@GeneratedValue(generator = "xval_cross_validation_log_sequence", strategy = GenerationType.SEQUENCE)
	@Column(name = "id", nullable = false, unique = true)
	private Integer id;

	@Column(name = "oid")
	private String oid;

	@Column(name = "gazelle_cross_validator")
	private String gazelleCrossValidator;

	@Column(name = "affinity_domain")
	private String affinityDomain;

	@Column(name = "username")
	private String username;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "timestamp")
	private Date timestamp;

	@Column(name = "is_public")
	private Boolean isPublic;

	@Lob
	@Type(type = "text")
	@Column(name = "detailed_result")
	private String detailedResult;

	@Column(name = "privacy_key")
	private String privacyKey;

	@Column(name = "result")
	private ValidationResultType result;

	@Column(name="calling_tool_oid")
	private String callingToolOid;

	@Column(name="external_id")
	private String externalId;

	@Column(name="message_type_proxy")
	private String messageTypeFromProxy;


	@OneToMany(cascade = { CascadeType.ALL }, mappedBy = "log")
	private List<UploadedFile> validatedFiles;

	public CrossValidationLog() {
		this.isPublic = true;
		this.timestamp = new Date();
	}

	@PrePersist
	public void setUsername() {
		if (((this.username == null) || this.username.isEmpty())  && Identity.instance()!=null && Identity.instance().isLoggedIn()) {
			if (Identity.instance().getCredentials()!=null) {
				this.username = Identity.instance().getCredentials().getUsername();
			}
		}
	}

	private String computePermanentLink(){
		StringBuilder builder = new StringBuilder(PreferenceService.getString("application_url"));
		builder.append(XValidationPages.VAL_REPORT.getSeamLink());
		builder.append("?oid=");
		builder.append(oid);
		if (privacyKey != null && !privacyKey.isEmpty()){
			builder.append("&privacyKey=");
			builder.append(privacyKey);
		}
		return builder.toString();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getGazelleCrossValidator() {
		return gazelleCrossValidator;
	}

	public void setGazelleCrossValidator(String gazelleCrossValidator) {
		this.gazelleCrossValidator = gazelleCrossValidator;
	}

	public String getAffinityDomain() {
		return affinityDomain;
	}

	public void setAffinityDomain(String affinityDomain) {
		this.affinityDomain = affinityDomain;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public Boolean getIsPublic() {
		return isPublic;
	}

	public void setIsPublic(Boolean isPublic) {
		this.isPublic = isPublic;
	}

	public String getDetailedResult() {
		return detailedResult;
	}

	public void setDetailedResult(String detailedResult) {
		this.detailedResult = detailedResult;
	}

	public String getPrivacyKey() {
		return privacyKey;
	}

	public void setPrivacyKey(String privacyKey) {
		this.privacyKey = privacyKey;
	}

	public ValidationResultType getResult() {
		return result;
	}

	public void setResult(ValidationResultType result) {
		this.result = result;
	}

	public List<UploadedFile> getValidatedFiles() {
		return validatedFiles;
	}

	public void setValidatedFiles(List<UploadedFile> validatedFiles) {
		this.validatedFiles = validatedFiles;
	}

	public String getCallingToolOid() {
		return callingToolOid;
	}

	public void setCallingToolOid(String callingToolOid) {
		this.callingToolOid = callingToolOid;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public String getMessageTypeFromProxy() {
		return messageTypeFromProxy;
	}

	public void setMessageTypeFromProxy(String messageTypeFromProxy) {
		this.messageTypeFromProxy = messageTypeFromProxy;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int hashcode = 1;
		hashcode = (prime * hashcode) + ((affinityDomain == null) ? 0 : affinityDomain.hashCode());
		hashcode = (prime * hashcode) + ((gazelleCrossValidator == null) ? 0 : gazelleCrossValidator.hashCode());
		hashcode = (prime * hashcode) + ((timestamp == null) ? 0 : timestamp.hashCode());
		hashcode = (prime * hashcode) + ((username == null) ? 0 : username.hashCode());
		return hashcode;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		CrossValidationLog other = (CrossValidationLog) obj;
		if (affinityDomain == null) {
			if (other.affinityDomain != null) {
				return false;
			}
		} else if (!affinityDomain.equals(other.affinityDomain)) {
			return false;
		}
		if (gazelleCrossValidator == null) {
			if (other.gazelleCrossValidator != null) {
				return false;
			}
		} else if (!gazelleCrossValidator.equals(other.gazelleCrossValidator)) {
			return false;
		}
		if (timestamp == null) {
			if (other.timestamp != null) {
				return false;
			}
		} else if (!timestamp.equals(other.timestamp)) {
			return false;
		}
		if (username == null) {
			if (other.username != null) {
				return false;
			}
		} else if (!username.equals(other.username)) {
			return false;
		}
		return true;
	}

	public boolean currentUserIsAuthorized(String privacyKey2) {
		String loggedInUser = null;
		if (Identity.instance().isLoggedIn()) {
			loggedInUser = Identity.instance().getCredentials().getUsername();
		}
		if (privacyKey == null) {
			if (isPublic) {
				return true;
			} else if (loggedInUser != null) {
				return (loggedInUser.equals(username) || Identity.instance().hasRole("admin_role") || Identity
						.instance().hasRole("monitor_role"));
			} else {
				return false;
			}
		} else if (privacyKey.equals(privacyKey2)) {
			return true;
		} else if (loggedInUser == null) {
			return false;
		} else {
			return ((loggedInUser.equals(username) || Identity.instance().hasRole("admin_role") || Identity.instance()
					.hasRole("monitor_role")));
		}
	}

	public static void setOidFromRootOid(CrossValidationLog validationLog) {
		if (validationLog.getId() != null) {
			String root = PreferenceService.getString("x_validation_log_root_oid");
			validationLog.setOid(root + validationLog.getId());
		}
	}

	public void protectByKey() {
		setIsPublic(false);
		setPrivacyKey(RandomStringUtils.randomAlphanumeric(PRIVACY_KEY_LENGTH));
		computePermanentLink();
	}

	public void markPublic() {
		setIsPublic(true);
		setPrivacyKey(null);
		computePermanentLink();
	}

	public void markPrivate() {
		setIsPublic(false);
		setPrivacyKey(null);
		computePermanentLink();
	}

	public String getOid() {
		return oid;
	}

	public void setOid(String oid) {
		this.oid = oid;
	}

	public byte[] getDetailedResultAsByteArray() {
		if (detailedResult != null) {
			return detailedResult.getBytes(Charset.forName("UTF-8"));
		} else {
			return null;
		}
	}

	public String getPermanentLink() {
		return computePermanentLink();
	}

}
