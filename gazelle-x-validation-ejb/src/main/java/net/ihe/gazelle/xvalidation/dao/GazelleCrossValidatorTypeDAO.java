package net.ihe.gazelle.xvalidation.dao;

import gnu.trove.map.hash.THashMap;
import junit.framework.AssertionFailedError;
import net.ihe.gazelle.evsclient.interlay.gui.GuiMessage;
import net.ihe.gazelle.hql.HQLRestriction;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.xvalidation.core.model.*;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.security.Identity;
import org.jfree.util.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class GazelleCrossValidatorTypeDAO extends DAO<GazelleCrossValidatorType> {

	private static Logger log = LoggerFactory.getLogger(GazelleCrossValidatorTypeDAO.class);
	Map<String,DateFormatType> dateFormatRegistry;

	public GazelleCrossValidatorTypeDAO() {
		super();
	}

	public GazelleCrossValidatorTypeDAO(EntityManager entityManager) {
		super(entityManager);
	}

	public static GazelleCrossValidatorTypeDAO instanceWithDefaultEntityManager() {
		return new GazelleCrossValidatorTypeDAO();
	}

	public void persistEntity(GazelleCrossValidatorType validator) {
		entityManager.persist(validator);
		entityManager.flush();
	}

	public boolean suchValidatorExists(GazelleCrossValidatorType validator) {
		// check that a validator with that name/affinitydomain does not exist
		GazelleCrossValidatorTypeQuery query = new GazelleCrossValidatorTypeQuery(entityManager);
		query.name().like(validator.getName());
		if (validator.getAffinityDomain() != null) {
			query.affinityDomain().like(validator.getAffinityDomain());
		}
		if (query.getCount() > 0) {
			// if exists check the version number is different
			query.version().like(validator.getVersion());
			if (query.getCount() > 0) {
				GuiMessage.logMessage(StatusMessage.Severity.WARN, "A Gazelle Cross Validator with this name, affinity domain, and version already exists");
				return true;
			} else {
				GuiMessage.logMessage(StatusMessage.Severity.WARN, "A Gazelle Cross Validator with this name and affinity domain already exists");
				return true;
			}
		} else {
			return false;
		}
	}

	public GazelleCrossValidatorType saveImportedEntity(GazelleCrossValidatorType validator) {
		if (suchValidatorExists(validator)) {
			return null;
		} else {
			validator.setLastModifiedDate(validator.getLastModified());
			boolean canSave = true;
			// if a keyword matched a referenced object in DB, use the one in DB
			for (ValidatorInput input : validator.getValidatedObjects()) {
				checkValidatorInput(input);
			}
			// check unicity of rule keywords, in DB and within the file
			List<String> ruleKeywords = new ArrayList<String>();
			for (Rule rule : validator.getRules()) {
				canSave = canSave && checkRule(rule, ruleKeywords);
			}
			if (canSave) {
				persistEntity(validator);
				// then we need to set the validator in each rule, validator input and validator configuration
				for (Rule rule : validator.getRules()) {
					rule.setGazelleCrossValidator(validator);
					for (Assertion assertion : rule.getAssertions()) {
						assertion.getRules().add(rule);
					}
				}
				if (validator.getConfiguration() != null) {
					validator.getConfiguration().setValidator(validator);
					if (!validator.getConfiguration().getNamespaces().isEmpty()) {
						List<Namespace> namespaces = new ArrayList<Namespace>();
						for (Namespace namespace : validator.getConfiguration().getNamespaces()) {
							namespaces.add(NamespaceDAO.instanceWithDefaultEntityManager().saveEntity(namespace));
						}
						validator.getConfiguration().setNamespaces(namespaces);
					}
				}
				for (ValidatorInput input : validator.getValidatedObjects()) {
					input.setGazelleCrossValidator(validator);
				}
				return saveEntity(validator);
			} else {
				entityManager.getTransaction().rollback();
				return null;
			}
		}
	}

	private void checkValidatorInput(ValidatorInput input) {
		if ((input.getMinQuantity() == null) || (input.getMinQuantity() == 0)) {
			input.setMinQuantity(1);
		}
		if ((input.getMaxQuantity() == null) || (input.getMaxQuantity() == 0)) {
			input.setMaxQuantity(1);
		}
		ReferencedObject reference = input.getReferencedObject();
		ReferencedObject referencedFromDB = ReferencedObjectDAO.instanceWithDefaultEntityManager()
				.getReferencedObjectByKeyword(reference.getKeyword());
		if (referencedFromDB != null) {
			GuiMessage.logMessage(StatusMessage.Severity.INFO,
					"Reference with keyword " + referencedFromDB.getKeyword()
					+ " matches an existing referenced object");
			input.setReferencedObject(referencedFromDB);
		}
	}

	private boolean checkRule(Rule rule, List<String> ruleKeywords) {
		boolean canSave = true;
		rule.setLastModifiedDate(rule.getLastModified());
		if (!RuleDAO.instanceWithDefaultEntityManager().isKeywordAvailable(rule.getKeyword(), null)) {
			GuiMessage.logMessage(StatusMessage.Severity.WARN, "Rule keyword : " + rule.getKeyword() + " is already used");
			log.error("Rule keyword : " + rule.getKeyword() + " is already used");
			canSave = false;
		} else if (ruleKeywords.contains(rule.getKeyword())) {
			GuiMessage.logMessage(StatusMessage.Severity.WARN, "Rule keyword : " + rule.getKeyword() + " is used twice in this file");
			log.error("Rule keyword : " + rule.getKeyword() + " is used twice in this file");
			canSave = false;
		} else {
			try {
				rule.testExpressionIsCorrect();
				ruleKeywords.add(rule.getKeyword());
				// if an assertion already exists in DB, use this one
				// we save the assertion even if we do not know the outcome of the checks because an assertion can be used
				// more than once in a single file
				for (Assertion assertion : rule.getAssertions()) {
					Assertion assertionFromDB = AssertionDAO.instanceWithDefaultEntityManager()
							.getAssertionByIdAndIdScheme(assertion.getIdScheme(), assertion.getAssertionId());
					if (assertionFromDB != null) {
						assertion = assertionFromDB;
					} else {
						assertion = AssertionDAO.instanceWithDefaultEntityManager().saveEntity(assertion);
					}
				}
				// enforce date formats exists
				mergeDateFormats(rule);
			} catch (AssertionFailedError e) {
				canSave = false;
				log.error(rule.getKeyword() + ": " + e.getMessage());
				GuiMessage.logMessage(StatusMessage.Severity.ERROR, e.getMessage());
			}
		}
		return canSave;
	}

	private void mergeDateFormats(Rule rule) {
		if (rule.getExpression()!=null&&DateComparison.class.isInstance(rule.getExpression().getExpression())) {
			DateFormatTypeDAO dao = DateFormatTypeDAO.instanceWithDefaultEntityManager();
			if (dateFormatRegistry == null) {
				dateFormatRegistry = new THashMap<>();
				for (DateFormatType dft : dao.getAllEntities()) {
					dateFormatRegistry.put(dft.getLabel(), dft);
				}
			}
			DateComparison dc = (DateComparison) rule.getExpression().getExpression();
			mergeDateFormats(dao, dc.getDateFormats1());
			mergeDateFormats(dao, dc.getDateFormats2());
		}
	}

	private void mergeDateFormats(DateFormatTypeDAO dao, List<DateFormatType> dateformats) {
		DateFormatType[] dtfs = dateformats.toArray(new DateFormatType[]{});
		dateformats.clear();
		for (DateFormatType df:dtfs) {
			DateFormatType f = dateFormatRegistry.get(df.getLabel());
			if (f==null) {
				df = dao.saveEntity(df);
				dateFormatRegistry.put(df.getLabel(),df);
				dateformats.add(df);
			} else {
				dateformats.add(f);
			}
		}
	}

	public GazelleCrossValidatorType getValidatorByUrlParameters(Integer id, String oid, String name, String affinityDomain,
			String version) {
		GazelleCrossValidatorTypeQuery query = new GazelleCrossValidatorTypeQuery(entityManager);
		// access is restricted to AVAILABLE validators for basic users
		if (!Identity.instance().isLoggedIn()
				|| (Identity.instance().isLoggedIn() && !(Identity.instance().hasRole("admin_role") || Identity
						.instance().hasRole("tests_editor_role")))) {
			query.status().eq(Status.AVAILABLE);
		}
		if (id != null) {
			query.id().eq(id);
			return query.getUniqueResult();
		}
		if (oid != null) {
			query.oid().eq(oid);
			return query.getUniqueResult();
		}
		if ((name != null) && !name.isEmpty()) {
			query.name().eq(name);
		}
		if ((affinityDomain != null) && !affinityDomain.isEmpty()) {
			query.affinityDomain().eq(affinityDomain);
		}
		if ((version != null) && !version.isEmpty()) {
			query.version().eq(version);
		}
		return query.getUniqueResult();
	}

	public Integer countEntries() {
		GazelleCrossValidatorTypeQuery query = new GazelleCrossValidatorTypeQuery(entityManager);
		return query.getCount();
	}

	public List<GazelleCrossValidatorType> getValidatorForReferencedObject(ReferencedObject reference) {
		GazelleCrossValidatorTypeQuery query = new GazelleCrossValidatorTypeQuery(entityManager);
		query.validatedObjects().referencedObject().keyword().eq(reference.getKeyword());
		query.affinityDomain().order(true);
		query.name().order(true);
		if (!Identity.instance().isLoggedIn()
				|| (Identity.instance().isLoggedIn() && !(Identity.instance().hasRole("admin_role") || Identity
						.instance().hasRole("tests_editor_role")))) {
			query.status().eq(Status.AVAILABLE);
		}
		return query.getList();
	}

	public List<GazelleCrossValidatorType> getValidatorsAvailableForTesting(String affinityDomain) {
		GazelleCrossValidatorTypeQuery query = new GazelleCrossValidatorTypeQuery(entityManager);
		if ((affinityDomain != null) && !affinityDomain.isEmpty()) {
			query.affinityDomain().eq(affinityDomain);
		}
		query.status().eq(Status.AVAILABLE);
		query.affinityDomain().order(true);
		query.name().order(true);
		return query.getList();
	}

	public List<GazelleCrossValidatorType> getValidatorsAvailableForRemote() {
		GazelleCrossValidatorTypeQuery query = new GazelleCrossValidatorTypeQuery(entityManager);
		query.status().eq(Status.AVAILABLE);
		query.affinityDomain().order(true);
		query.name().order(true);
		return query.getList();
	}

	@Override
	public Class<GazelleCrossValidatorType> getEntityClass() {
		return GazelleCrossValidatorType.class;
	}

	/**
	 * Returns the list of available validators which are not the current one
	 *
	 * @param validator
	 * @return
	 */
	public List<GazelleCrossValidatorType> getValidatorsToInheritFrom(GazelleCrossValidatorType validator) {
		GazelleCrossValidatorTypeQuery query = new GazelleCrossValidatorTypeQuery(entityManager);
		query.status().eq(Status.AVAILABLE);
		if (validator.getId() != null) {
			query.id().neq(validator.getId());
		}
		return query.getList();
	}
	
	public List<GazelleCrossValidatorType> getValidatorParents(GazelleCrossValidatorType validator){
		GazelleCrossValidatorTypeQuery query = new GazelleCrossValidatorTypeQuery(entityManager);
		List<HQLRestriction> restrictions = new ArrayList<HQLRestriction>();
		if (!validator.getValidatorParents().isEmpty()) {
			for (CrossValidatorReference reference : validator.getValidatorParents()) {
				restrictions.add(HQLRestrictions.and(query.affinityDomain()
						.eqRestriction(reference.getAffinityDomain()), query.name()
						.eqRestriction(reference.getName())));
			}
		}
		query.addRestriction(HQLRestrictions.or(restrictions.toArray(new HQLRestriction[restrictions.size()])));
		return query.getListDistinct();
	}

	public List<String> getUsedAffinityDomains() {
		GazelleCrossValidatorTypeQuery query = new GazelleCrossValidatorTypeQuery(entityManager);
		return query.affinityDomain().getListDistinct();
	}

	public boolean isNameAvailable(String name, String affinityDomain, Integer validatorId) {
		GazelleCrossValidatorTypeQuery query = new GazelleCrossValidatorTypeQuery(entityManager);
		query.name().eq(name);
		if ((affinityDomain != null) && !affinityDomain.isEmpty()) {
			query.affinityDomain().eq(affinityDomain);
		}
		if (validatorId != null) {
			query.id().neq(validatorId);
		}
		return query.getCount() == 0;
	}

	public void delete(GazelleCrossValidatorType oldValidator) {
		GazelleCrossValidatorType todelete = find(oldValidator.getId());
		entityManager.remove(todelete);
		entityManager.flush();		
	}
	
	public List<GazelleCrossValidatorType> listValidatorsWhichInheritFromValidator(GazelleCrossValidatorType parent){
		GazelleCrossValidatorTypeQuery query = new GazelleCrossValidatorTypeQuery();
		query.validatorParents().name().eq(parent.getName());
		query.validatorParents().affinityDomain().eq(parent.getAffinityDomain());
		return query.getList();
	}

	public GazelleCrossValidatorType getValidatorByOid(String oid) {
		GazelleCrossValidatorTypeQuery query = new GazelleCrossValidatorTypeQuery();
		query.oid().eq(oid);
		GazelleCrossValidatorType gazelleCrossValidatorType = query.getUniqueResult();
		if (gazelleCrossValidatorType != null) {
			return gazelleCrossValidatorType;
		} else {
			return null;
		}
	}

	public String getValidatorNameByOid(String oid) {
		GazelleCrossValidatorTypeQuery query = new GazelleCrossValidatorTypeQuery();
		query.oid().eq(oid);
		GazelleCrossValidatorType gazelleCrossValidatorType = query.getUniqueResult();
		if (gazelleCrossValidatorType != null) {
			return gazelleCrossValidatorType.getName();
		} else {
			return "";
		}
	}

	public String getValidatorDescriptionByOid(String oid) {
		GazelleCrossValidatorTypeQuery query = new GazelleCrossValidatorTypeQuery();
		query.oid().eq(oid);
		GazelleCrossValidatorType gazelleCrossValidatorType = query.getUniqueResult();
		if (gazelleCrossValidatorType != null) {
			return gazelleCrossValidatorType.getDescription();
		} else {
			return "This cross validator is not existing.";
		}
	}

	public String getValidatorStatusByOid(String oid) {
		GazelleCrossValidatorTypeQuery query = new GazelleCrossValidatorTypeQuery();
		query.oid().eq(oid);
		GazelleCrossValidatorType gazelleCrossValidatorType = query.getUniqueResult();
		if (gazelleCrossValidatorType != null) {
			return gazelleCrossValidatorType.getStatus().value();
		} else {
			return null;
		}
	}

	public GazelleCrossValidatorType getValidatorFromName(String name) {
	    GazelleCrossValidatorTypeQuery query = new GazelleCrossValidatorTypeQuery();
	    query.name().eq(name);
		GazelleCrossValidatorType gazelleCrossValidatorType = query.getUniqueResult();
		if (gazelleCrossValidatorType != null) {
			return gazelleCrossValidatorType;
		} else {
			return null;
		}
    }

    public GazelleCrossValidatorType getValidatorToReference(CrossValidatorReference reference) {
        GazelleCrossValidatorTypeQuery query = new GazelleCrossValidatorTypeQuery();
        query.name().eq(reference.getName());
        query.affinityDomain().eq(reference.getAffinityDomain());
        GazelleCrossValidatorType validator = query.getUniqueResult();
        if (validator == null){
            validator = new GazelleCrossValidatorType();
            validator = saveEntity(validator);
        }
        return validator;
    }

    public List<String> listDeprecatedParentValidatorsName(List<String> deprecatedValidatorParentsName, List<CrossValidatorReference> validatorParentReferences) {

        List<GazelleCrossValidatorType> analyzedValidators = new ArrayList<>();

		iterateThroughParentValidators(deprecatedValidatorParentsName, validatorParentReferences, analyzedValidators);

		return deprecatedValidatorParentsName;
	}

	private void iterateThroughParentValidators(List<String> deprecatedValidatorParentsName, List<CrossValidatorReference> validatorParentReferences, List<GazelleCrossValidatorType> analyzedValidators) {

		for (CrossValidatorReference reference : validatorParentReferences) {
			GazelleCrossValidatorType currentCrossValidatorParent = getValidatorToReference(reference);
			Status status = currentCrossValidatorParent.getStatus();
			if (!deprecatedValidatorParentsName.contains(currentCrossValidatorParent.getName() + " - " + currentCrossValidatorParent.getAffinityDomain()) && !analyzedValidators.contains(currentCrossValidatorParent)) {
                analyzedValidators.add(currentCrossValidatorParent);
                if (status.equals(Status.DEPRECATED)) {
					deprecatedValidatorParentsName.add(currentCrossValidatorParent.getName() + " - " + currentCrossValidatorParent.getAffinityDomain());
                    Log.warn(currentCrossValidatorParent.getName() + " is deprecated !");
				}

				List<CrossValidatorReference> currentValidatorParentReferences = currentCrossValidatorParent.getValidatorParents();
                iterateThroughParentValidators(deprecatedValidatorParentsName, currentValidatorParentReferences, analyzedValidators);
			}
        }
	}

}
