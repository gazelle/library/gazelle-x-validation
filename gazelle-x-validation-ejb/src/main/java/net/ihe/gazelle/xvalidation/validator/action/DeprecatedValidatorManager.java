package net.ihe.gazelle.xvalidation.validator.action;

import java.io.*;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.evsclient.interlay.gui.GuiMessage;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.xvalidation.dao.DeprecatedValidatorVersionDAO;
import net.ihe.gazelle.xvalidation.menu.XValidationPages;
import net.ihe.gazelle.xvalidation.model.DeprecatedValidatorVersion;
import net.ihe.gazelle.xvalidation.model.DeprecatedValidatorVersionQuery;
import net.ihe.gazelle.xvalidation.utils.Exporter;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Name("deprecatedValidatorManager")
@Scope(ScopeType.PAGE)
public class DeprecatedValidatorManager extends Exporter implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8705548010826844860L;

	private static Logger log = LoggerFactory.getLogger(DeprecatedValidatorManager.class);
	private Filter<DeprecatedValidatorVersion> filter;
	private boolean empty;

	@Create
	public void init() {
		empty = !DeprecatedValidatorVersionDAO.instanceWithDefaultEntityManager().hasEntries();
	}

	public Filter<DeprecatedValidatorVersion> getFilter() {
		if (filter == null) {
			filter = new Filter<DeprecatedValidatorVersion>(getHqlCriterions());
		}
		return filter;
	}

	public void reset() {
		filter.clear();
	}

	private HQLCriterionsForFilter<DeprecatedValidatorVersion> getHqlCriterions() {
		DeprecatedValidatorVersionQuery query = new DeprecatedValidatorVersionQuery();
		HQLCriterionsForFilter<DeprecatedValidatorVersion> criteria = query.getHQLCriterionsForFilter();
		criteria.addPath("name", query.name());
		criteria.addPath("afDomain", query.affinityDomain());
		criteria.addPath("username", query.lastModifier());
		criteria.addPath("timestamp", query.lastModified());
		criteria.addPath("version", query.version());
		return criteria;
	}

	public FilterDataModel<DeprecatedValidatorVersion> getFilteredValues() {
		return new FilterDataModel<DeprecatedValidatorVersion>(getFilter()){
                    @Override
        protected Object getId(DeprecatedValidatorVersion t) {
        // TODO Auto-generated method stub
        return t.getId();
        }
        };
	}

	public void download(DeprecatedValidatorVersion validator) {
		displayOrDownload(validator, true);
	}

	private void displayOrDownload(DeprecatedValidatorVersion validator, boolean download) {
		File file = new File(new File(DeprecatedValidatorVersion.getDirectoryForArchives()), validator.getFilePath());
		String contentType = "application/xml";
		if (file.exists()) {
			long length = file.length();
			byte[] fileContent = new byte[(int) length];
			try {
				FileInputStream fis = null;
				try {
					if (file.exists()) {
						fis = new FileInputStream(file);
						fis.read(fileContent);
					}
				} catch (IOException e) {
					log.error(e.getMessage(), e);
					GuiMessage.logMessage(StatusMessage.Severity.ERROR, e.getMessage());
				} finally {
					if (fis != null) {
						fis.close();
					}
				}
				getOutputStream().write(fileContent);
				if (download) {
					redirectExport(contentType, validator.getFilePath());
				} else {
					displayExport(contentType);
				}
			} catch (FileNotFoundException e1) {
				log.error(e1.getMessage());
			} catch (IOException e) {
				GuiMessage.logMessage(StatusMessage.Severity.ERROR, e.getMessage());
				log.error(e.getMessage(), e);
			}
		} else {
			GuiMessage.logMessage(StatusMessage.Severity.ERROR, "The system has not been able to retrieve the archived file");
		}
	}

	public String validate(DeprecatedValidatorVersion validator) {
		return null;
	}

	public void display(DeprecatedValidatorVersion validator) {
		displayOrDownload(validator, false);
	}

	public String listCurrentValidators() {
		return XValidationPages.ADMIN_VALIDATORS.getLink();
	}

	public boolean isEmpty() {
		return empty;
	}

}
