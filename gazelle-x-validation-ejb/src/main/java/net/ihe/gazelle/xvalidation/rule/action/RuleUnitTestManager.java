package net.ihe.gazelle.xvalidation.rule.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.ihe.gazelle.validator.ut.action.UnitTestManager;
import net.ihe.gazelle.validator.ut.dao.UnitTestLogDAO;
import net.ihe.gazelle.validator.ut.model.UnitTest;
import net.ihe.gazelle.validator.ut.model.UnitTestFile;
import net.ihe.gazelle.validator.ut.model.UnitTestLog;
import net.ihe.gazelle.validator.ut.model.UnitTestResult;
import net.ihe.gazelle.xvalidation.core.exception.GazelleXValidationException;
import net.ihe.gazelle.xvalidation.core.model.GazelleCrossValidatorType;
import net.ihe.gazelle.xvalidation.core.model.Kind;
import net.ihe.gazelle.xvalidation.core.model.Rule;
import net.ihe.gazelle.xvalidation.core.report.Aborted;
import net.ihe.gazelle.xvalidation.core.report.Notification;
import net.ihe.gazelle.xvalidation.core.report.Report;
import net.ihe.gazelle.xvalidation.core.utils.FileConverter;
import net.ihe.gazelle.xvalidation.dao.RuleUnitTestDAO;
import net.ihe.gazelle.xvalidation.menu.XValidationPages;
import net.ihe.gazelle.xvalidation.model.RuleUnitTest;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.w3c.dom.Node;

@Name("ruleUnitTestManager")
@Scope(ScopeType.STATELESS)
public class RuleUnitTestManager extends UnitTestManager<RuleUnitTest> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3258729114956565360L;

	public void executeAllTests(Rule rule) {
		List<RuleUnitTest> unitTests = RuleUnitTestDAO.instanceWithDefaultEntityManager().getUnitTestsForRule(rule);
		executeList(unitTests);
	}

	public String createNewTest(Rule rule) {
		return XValidationPages.ADMIN_RULE_UNIT_TEST.getSeamLink() + "?rule=" + rule.getKeyword();
	}

	public String editUnitTest(RuleUnitTest unitTest) {
		return XValidationPages.ADMIN_RULE_UNIT_TEST.getSeamLink() + "?unitTest=" + unitTest.getKeyword();
	}

	public String displayUnitTestsForValidator(GazelleCrossValidatorType validator) {
		return XValidationPages.ADMIN_BROWSE_UT.getSeamLink() + "?validator=" + validator.getId();
	}

	@Override
	public String displayUnitTestLogs(UnitTest unitTest) {
		return XValidationPages.ADMIN_RULE_UNIT_TEST_LOGS.getSeamLink() + "?unitTest=" + unitTest.getId();
	}

	public String displayUnitTestForRule(Rule rule) {
		return XValidationPages.ADMIN_BROWSE_UT.getSeamLink() + "?rule=" + rule.getId();
	}

	public String displayRuleUnitTestLogs(Rule rule) {
		return XValidationPages.ADMIN_RULE_UNIT_TEST_LOGS.getSeamLink() + "?rule=" + rule.getId();
	}

	public String displayRuleVersionUnitTestLogs(Rule rule) {
		return XValidationPages.ADMIN_RULE_UNIT_TEST_LOGS.getSeamLink() + "?rule=" + rule.getId() + "&testedVersion="
				+ rule.getVersion();
	}

	public String listUnitTests() {
		return XValidationPages.ADMIN_BROWSE_UT.getLink();
	}

	public String executeUnitTestsForValidator(GazelleCrossValidatorType validator) {
		List<RuleUnitTest> unitTests = RuleUnitTestDAO.instanceWithDefaultEntityManager().getUnitTestsForValidator(
				validator);
		executeList(unitTests);
		return displayUnitTestsForValidator(validator);
	}

	@Override
	protected void execute(RuleUnitTest inTest) {
		RuleUnitTestDAO dao = RuleUnitTestDAO.instanceWithDefaultEntityManager();
		RuleUnitTest test = dao.find(inTest.getId());
		Map<String, Node> nodes = new HashMap<String, Node>();
		UnitTestLog testLog = new UnitTestLog(test);
		testLog.setTestedVersion(inTest.getTestedRule().getVersion());
		String errorMessage = null;
		boolean canRunTest = true;
		for (UnitTestFile file : test.getFiles()) {
			try {
				Node node = FileConverter.file2Node(Kind.fromValue(file.getInputType().getValue()),
						file.getAbsoluteFilePath(), test.getTestedRule().getGazelleCrossValidator().getDicomLibrary());
				nodes.put(file.getInputKeyword(), node);
			} catch (Exception e) {
				testLog.setEffectiveResult(UnitTestResult.ABORTED);
				errorMessage = "Error when processing file with keyword: " + file.getInputKeyword() + ": "
						+ e.getMessage();
				canRunTest = false;
				break;
			}
		}
		if (canRunTest) {
			Rule testedRule = test.getTestedRule();
			Notification result = testedRule.evaluate(nodes, testedRule.getValidatorConfiguration());
			if (result instanceof Report) {
				testLog.setEffectiveResult(UnitTestResult.PASSED);
			} else if (result instanceof Aborted){
				testLog.setEffectiveResult(UnitTestResult.ABORTED);
				errorMessage = ((Aborted) result).getReason();
				if (result.getProcessedInputs() != null) {
					errorMessage = errorMessage + " (Executed XPath: " + result.getProcessedInputs() + ")";
				}
			} else {
				testLog.setEffectiveResult(UnitTestResult.FAILED);
			}
			testLog.setAdditionalInfo(result.getXpathReport());
		}
		testLog.setSuccess(test.getExpectedResult().equals(testLog.getEffectiveResult()));
		if (!testLog.isSuccess() && errorMessage == null) {
			errorMessage = "Returned <" + testLog.getEffectiveResult().getValue() + "> but <"
					+ test.getExpectedResult().getValue() + "> was expected";
		}
		testLog.setReasonForFailure(errorMessage);
		test.setLastResult(testLog.isSuccess());
		test.setLastRunDate(testLog.getRunDate());
		test.setLastTestedVersion(testLog.getTestedVersion());
		test.setReasonForFailure(testLog.getReasonForFailure());
		dao.saveEntity(test);
		UnitTestLogDAO.instanceWithDefaultEntityManager().saveEntity(testLog);
	}

	@Override
	protected void save(RuleUnitTest unitTest) {
		RuleUnitTestDAO.instanceWithDefaultEntityManager().saveEntity(unitTest);
	}

	@Override
	protected RuleUnitTest createCopyForTest(RuleUnitTest unitTest) {
		return new RuleUnitTest(unitTest);
	}
}
