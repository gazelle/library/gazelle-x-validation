package net.ihe.gazelle.xvalidation.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import net.ihe.gazelle.assertion.ws.coverage.provider.data.CoveringEntity;
import net.ihe.gazelle.hql.restrictions.HQLRestrictionLikeMatchMode;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.xvalidation.core.model.Assertion;
import net.ihe.gazelle.xvalidation.core.model.AssertionQuery;
import net.ihe.gazelle.xvalidation.core.model.GazelleCrossValidatorType;
import net.ihe.gazelle.xvalidation.core.model.Rule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AssertionDAO extends DAO<Assertion> {

	private static Logger log = LoggerFactory.getLogger(AssertionDAO.class);

	public AssertionDAO() {
		super();
	}

	public AssertionDAO(EntityManager entityManager) {
		super(entityManager);
	}

	public static AssertionDAO instanceWithDefaultEntityManager() {
		return new AssertionDAO();
	}

	public Assertion getAssertionByIdAndIdScheme(String idScheme, String assertionId) {
		AssertionQuery query = new AssertionQuery(entityManager);
		query.idScheme().eq(idScheme);
		query.assertionId().eq(assertionId);
		return query.getUniqueResult();
	}

	public List<Assertion> getAssertionsCoveredByValidator(GazelleCrossValidatorType validator) {
		List<Integer> rules = RuleDAO.instanceWithDefaultEntityManager().getActiveRuleIdsForValidator(validator);
		if ((rules != null) && !rules.isEmpty()) {
			AssertionQuery query = new AssertionQuery(entityManager);
			query.addRestriction(HQLRestrictions.in("rules.id", rules));
			return query.getList();
		} else {
			return null;
		}
	}

	/**
	 * returns all the assertions registered with the given idScheme, if idScheme is null, returns all the assertions stored in DB
	 *
	 * @param idScheme
	 * @return
	 */
	public List<Assertion> getAssertionsByIdScheme(String idScheme) {
		AssertionQuery query = new AssertionQuery(entityManager);
		if (idScheme != null) {
			query.idScheme().eq(idScheme);
		}
		return query.getListNullIfEmpty();
	}

	/**
	 * Returns the list of rules covered by an assertion as a list of CoveringEntity
	 * 
	 * @param idScheme
	 * @param assertionId
	 * @return
	 */
	public List<CoveringEntity> getCoveringEntitiesForAssertion(String idScheme, String assertionId) {
		Assertion assertion = getAssertionByIdAndIdScheme(idScheme, assertionId);
		if (assertion != null) {
			List<Rule> rules = assertion.getRules();
			if (!rules.isEmpty()) {
				List<CoveringEntity> entities = new ArrayList<CoveringEntity>();
				for (Rule rule : rules) {
					entities.add(RuleDAO.toCoveringEntity(rule));
				}
				return entities;
			} else {
				log.info("no rule for assertion " + idScheme + " - " + assertionId);
			}
		} else {
			log.info("no assertion for : " + idScheme + " - " + assertionId);
		}
		return null;
	}

	@Override
	public Class<Assertion> getEntityClass() {
		return Assertion.class;
	}

	public List<String> getAssertionIdsForIdScheme(String idScheme) {
		AssertionQuery query = new AssertionQuery(entityManager);
		query.assertionId().order(true);
		if (idScheme != null){
			query.idScheme().like(idScheme, HQLRestrictionLikeMatchMode.EXACT);
		}
		return query.assertionId().getListDistinct();
	}
	
	public List<String> getIdSchemes(){
		AssertionQuery query = new AssertionQuery(entityManager);
		query.idScheme().order(true);
		return query.idScheme().getListDistinct();
	}
}
