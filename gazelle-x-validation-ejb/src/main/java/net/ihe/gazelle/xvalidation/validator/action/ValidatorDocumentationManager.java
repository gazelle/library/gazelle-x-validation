package net.ihe.gazelle.xvalidation.validator.action;

import java.util.Map;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.xvalidation.core.model.GazelleCrossValidatorType;
import net.ihe.gazelle.xvalidation.core.model.GazelleCrossValidatorTypeQuery;
import net.ihe.gazelle.xvalidation.core.model.Status;
import net.ihe.gazelle.xvalidation.menu.XValidationPages;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Name("validatorDocumentationManager")
@Scope(ScopeType.PAGE)
public class ValidatorDocumentationManager extends AbstractValidatorBrowser {

	/**
	 *
	 */
	private static final long serialVersionUID = -7949902873319527972L;

	@Override
	protected void addSpecificHQLCriterions(HQLCriterionsForFilter<GazelleCrossValidatorType> criterions,
			GazelleCrossValidatorTypeQuery query) {
		// no specific criterions by now
	}

	@Override
	public void modifyQuery(HQLQueryBuilder<GazelleCrossValidatorType> queryBuilder,
			Map<String, Object> filterValuesApplied) {
		super.modifyQuery(queryBuilder, filterValuesApplied);
		queryBuilder.addEq("status", Status.AVAILABLE);
	}

	@Override
	public String listRules() {
		return XValidationPages.DOC_RULES.getLink();
	}

	@Override
	public String listReferencedObjects() {
		return XValidationPages.DOC_REFERENCES.getLink();
	}
}
