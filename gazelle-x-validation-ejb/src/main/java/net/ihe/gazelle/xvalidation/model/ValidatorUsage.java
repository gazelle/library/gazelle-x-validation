package net.ihe.gazelle.xvalidation.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import javax.validation.constraints.NotNull;
import org.jboss.seam.annotations.Name;

/**
 * <b>Class description</b>: ValidatorUsage
 *
 * This class is used to be able to report statistics on the usage of the validators
 *
 * @package net.ihe.gazelle.simulator.common.model
 * @class ValidatorUsage
 * @author Anne-Gaëlle Bergé / IHE Europe
 * @version 1.0 - August 3rd 2012
 *
 */

@Entity
@Name("validatorUsage")
@Table(name = "xval_validator_usage", schema = "public")
@SequenceGenerator(name = "xval_validator_usage_sequence", sequenceName = "xval_validator_usage_id_seq", allocationSize = 1)
public class ValidatorUsage implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 4386779544113481025L;

	@Id
	@GeneratedValue(generator = "xval_validator_usage_sequence", strategy = GenerationType.SEQUENCE)
	@NotNull
	@Column(name = "id", nullable = false, unique = true)
	private Integer id;

	@Column(name = "date")
	@Temporal(TemporalType.DATE)
	private Date date;

	@Column(name = "status")
	private String status;

	@Column(name = "type")
	private String type;

	@Column(name = "caller")
	private String caller;

	public ValidatorUsage() {

	}

	public ValidatorUsage(Date validationDate, String validationStatus, String validationType, String caller) {
		this.date = validationDate;
		this.status = validationStatus;
		this.caller = caller;
		this.type = validationType;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setCaller(String caller) {
		this.caller = caller;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public String getStatus() {
		return status;
	}

	public String getType() {
		return type;
	}

	public String getCaller() {
		return caller;
	}

}
