package net.ihe.gazelle.xvalidation.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

public abstract class Exporter {

	private transient ByteArrayOutputStream outputStream;

	public void redirectExport(String contentType, String fileName) throws IOException {
		export(contentType, fileName);
	}

	public void displayExport(String contentType) throws IOException {
		export(contentType, null);
	}

	private void export(String contentType, String fileName) throws IOException {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ExternalContext externalContext = facesContext.getExternalContext();

		HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();
		response.setContentType(contentType);
		if (fileName != null) {
			response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
		}
		ServletOutputStream servletOutputStream = response.getOutputStream();
		write(servletOutputStream);
		servletOutputStream.flush();
		servletOutputStream.close();
		facesContext.responseComplete();
	}

	private void write(OutputStream os) throws IOException {
		os.write(getOutputStream().toByteArray(), 0, getOutputStream().size());
		getOutputStream().reset();
		getOutputStream().close();
	}

	public ByteArrayOutputStream getOutputStream() {
		if (outputStream == null) {
			outputStream = new ByteArrayOutputStream();
		}
		return outputStream;
	}

	public void setOutputStream(ByteArrayOutputStream outputStream) {
		this.outputStream = outputStream;
	}

}
