/*
 * Copyright 2016 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package net.ihe.gazelle.xvalidation.utils;

import org.primefaces.model.chart.LegendPlacement;
import org.primefaces.model.chart.PieChartModel;

import java.io.Serializable;
import java.util.List;

/**
 * <b>Class Description : </b>AbstractPieChart<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 17/02/16
 * @class AbstractPieChart
 * @package net.ihe.gazelle.xvalidation.utils
 * @see anne-gaelle.berge@ihe-europe.net - http://gazelle.ihe.net
 */
public class AbstractPieChart implements Serializable {

    protected PieChartModel createPieChart(String title, String legendPlacement) {
        PieChartModel pieChartModel = new PieChartModel();
        pieChartModel.setTitle(title);
        pieChartModel.setLegendPosition(legendPlacement);
        pieChartModel.setShowDataLabels(true);
        pieChartModel.setLegendPlacement(LegendPlacement.INSIDE);
        return pieChartModel;
    }

    protected void formatUnitTestStat(PieChartModel pieChartModel, List<Object[]> stats) {
        for (Object[] stat : stats) {
            Boolean pass = (Boolean) stat[0];
            String label;
            if (pass == null) {
                label = "NOT RUN";
            } else if (pass) {
                label = "SUCCESS";
            } else {
                label = "FAILURE";
            }
            pieChartModel.set(label, (Number) stat[1]);
        }
    }
}
