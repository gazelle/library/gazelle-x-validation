package net.ihe.gazelle.xvalidation.action;


import net.ihe.gazelle.evsclient.interlay.gui.GuiMessage;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.xvalidation.attachmentanalyzer.MTOMSplitter;
import net.ihe.gazelle.xvalidation.core.exception.GazelleXValidationException;
import net.ihe.gazelle.xvalidation.core.model.GazelleCrossValidatorType;
import net.ihe.gazelle.xvalidation.core.model.GazelleCrossValidatorTypeQuery;
import net.ihe.gazelle.xvalidation.core.model.MessageType;
import net.ihe.gazelle.xvalidation.core.model.Status;
import net.ihe.gazelle.xvalidation.core.model.ValidatorInput;
import net.ihe.gazelle.xvalidation.core.utils.FileConverter;
import net.ihe.gazelle.xvalidation.dao.CrossValidationLogDAO;
import net.ihe.gazelle.xvalidation.dao.GazelleCrossValidatorTypeDAO;
import net.ihe.gazelle.xvalidation.dao.UploadedFileDAO;
import net.ihe.gazelle.xvalidation.dao.ValidatorInputDAO;
import net.ihe.gazelle.xvalidation.model.CrossValidationLog;
import net.ihe.gazelle.xvalidation.model.UploadedFile;
import net.ihe.gazelle.xvalidation.servlet.Upload;
import net.ihe.gazelle.xvalidation.utils.ValidationEngineWithDB;
import org.apache.commons.fileupload.util.Streams;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.richfaces.event.FileUploadEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.xml.bind.DatatypeConverter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static org.apache.commons.codec.binary.Base64.isArrayByteBase64;

@Name("remoteXValidator")
@Scope(ScopeType.PAGE)
public class RemoteXValidator extends AbstractResultDisplay implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(RemoteXValidator.class);
    private static final String TOOL_OID = "toolOid";

    private String client;
    private String fileName;
    private String[] keys;
    private String externalId;
    private String toolOid;
    private static String messageTypeFromProxy;

    private boolean displayResult = false;

    private GazelleCrossValidatorType validator;

    private List<GazelleCrossValidatorType> validators;

    private static transient File baseDir;

    private UploadedFile currentFile;
    private List<UploadedFile> files;

    private boolean allInputsWithFiles = true;
    private boolean containsFileAsAttachment;
    private boolean validatorHasSwitched;

    ///////////////////////////////////////////////////////////////////////////////
    ////////////////////    Getters and setters     /////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String[] getKeys() {
        return keys;
    }

    public void setKeys(String[] keys) {
        this.keys = keys;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getToolOid() {
        return toolOid;
    }

    public void setToolOid(String toolOid) {
        this.toolOid = toolOid;
    }

    public GazelleCrossValidatorType getValidator() {
        return this.validator;
    }

    public void setValidator(GazelleCrossValidatorType validator) {
        this.validator = validator;
    }

    public List<GazelleCrossValidatorType> getValidators() {
        return validators;
    }

    public void setValidators(List<GazelleCrossValidatorType> validators) {
        this.validators = validators;
    }

    public boolean isDisplayResult() {
        return displayResult;
    }

    public void setDisplayResult(boolean displayResult) {
        this.displayResult = displayResult;
    }

    public boolean areAllInputsWithFiles() {
        return allInputsWithFiles;
    }

    private void setAllInputsWithFiles(boolean allInputsWithFiles) {
        this.allInputsWithFiles = allInputsWithFiles;
    }

    public UploadedFile getCurrentFile() {
        return currentFile;
    }


    public boolean isContainsFileAsAttachment() {
        return containsFileAsAttachment;
    }

    public void setContainsFileAsAttachment(boolean containsFileAsAttachment) {
        this.containsFileAsAttachment = containsFileAsAttachment;
    }

    public static String getMessageTypeFromProxy() {
        return messageTypeFromProxy;
    }

    public static File getBaseDir() {
        return baseDir;
    }

    public static void setBaseDir(File baseDir) {
        RemoteXValidator.baseDir = baseDir;
    }

    public boolean getValidatorHasSwitched() {
        return validatorHasSwitched;
    }

    public void setValidatorHasSwitched(boolean validatorHasSwitched) {
        this.validatorHasSwitched = validatorHasSwitched;
    }

    public List<UploadedFile> getFiles() {
        files = UploadedFileDAO.instanceWithDefaultEntityManager().getFilesForValidation(getValidationLog());
        return files;
    }

    public void setCurrentFile(UploadedFile currentFile) {
        this.currentFile = currentFile;
    }

    public RemoteXValidator() {
    }

    /**
     * Identify client which called cross validation from url parameters
     *
     */

    @Create
    public void identifyClient() {
        Map<String, String> urlParameters = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (urlParameters != null) {
            setValidationLog(new CrossValidationLog());
            setValidatorHasSwitched(false);
            if (urlParameters.containsKey("xvaloid")) {
                if (!urlParameters.get("xvaloid").equals("") && !urlParameters.get("externalId").equals("") && !urlParameters.get("file").equals("")
                        && !urlParameters.get(TOOL_OID).equals("")) {
                    if (urlParameters.get(TOOL_OID).equals("MCA")) {
                        LOG.warn("We are coming from MCA");
                        setClient("MCA");
                    } else {
                        LOG.warn("We are coming from GWT");
                        setClient("GWT");
                    }
                } else {
                    FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.INFO, "One of the parameters is null, cross validation failed");
                }
                extractParametersAndInitialize(urlParameters, client);
            } else if (urlParameters.containsKey("messageId") && urlParameters.containsKey("type")) {
                LOG.warn("We are coming from Gazelle Proxy");
                if (!urlParameters.get("messageId").equals("") && !urlParameters.get("file").equals("") && !urlParameters.get(TOOL_OID).equals("") && !urlParameters.get("type").equals("")) {
                    setClient("Proxy");
                } else {
                    FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.INFO, "One of the parameters is null, cross validation failed");
                }
                extractParametersAndInitialize(urlParameters, client);
            } else {
                FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, "Client is unknown, there must be a problem...");
            }
        } else {
            FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.INFO, "Cannot retrieve parameters from Client, cross validation failed");
        }
    }

    /**
     * Initialize validator and validation log with url parameters depending on client
     * @param urlParameters Parameters inserted in url
     * @param client Client identified from url
     */

    private void extractParametersAndInitialize(Map<String, String> urlParameters, String client) {
        if (client.equals("GWT") || client.equals("MCA")) {
            String xvalidatorOid = urlParameters.get("xvaloid");
            setValidator(GazelleCrossValidatorTypeDAO.instanceWithDefaultEntityManager().getValidatorByOid(xvalidatorOid));
            getValidationLog().setExternalId(urlParameters.get("externalId"));
        } else if (client.equals("Proxy")) {
            messageTypeFromProxy = urlParameters.get("type");
            getValidationLog().setMessageTypeFromProxy(messageTypeFromProxy);
            getValidationLog().setExternalId(urlParameters.get("messageId"));
        }
        setExternalId(getValidationLog().getExternalId());
        setKeys(urlParameters.get("file").split(" "));
        setToolOid(urlParameters.get(TOOL_OID));
        getValidationLog().setCallingToolOid(getToolOid());
        setValidators(GazelleCrossValidatorTypeDAO.instanceWithDefaultEntityManager()
                .getValidatorsAvailableForRemote());
        if (keys != null && validators != null) {
            initializeValidator();
        } else {
            FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.INFO, "Cannot process X Validation, one or more of its parameters is null");
        }
    }

    /**
     * Initialize validator and validation log with url parameters depending on client
     *
     */
    private void initializeValidator() {

        if (validator != null) {
            if (getValidationLog().getCallingToolOid() ==  null || getValidationLog().getExternalId() == null) {
                getValidationLog().setExternalId(externalId);
                getValidationLog().setCallingToolOid(toolOid);
                if (client.equals("Proxy")) {
                    getValidationLog().setMessageTypeFromProxy(messageTypeFromProxy);
                }
            }
            getValidationLog().setAffinityDomain(validator.getAffinityDomain());
            getValidationLog().setGazelleCrossValidator(validator.getName());
            setValidationLog(CrossValidationLogDAO.instanceWithDefaultEntityManager().saveEntity(getValidationLog()));
            CrossValidationLog.setOidFromRootOid(getValidationLog());
            createDirectory();

            Map<String, File> filesMap = new HashMap<>();
            insertFilesIntoMap(filesMap);
            removeAttachmentFromMapIfNotNeeded(filesMap);

            setValidatorHasSwitched(true);

            List<ValidatorInput> inputs = getValidatorInputsForValidator();
            if ((inputs != null) && !inputs.isEmpty()) {
                files = new ArrayList<>();
                for (ValidatorInput input : inputs) {
                    boolean isFileUploaded = false;
                    LOG.warn("input.getKeyword() : " + input.getKeyword());
                    UploadedFile upload = new UploadedFile(input, baseDir.getAbsolutePath());
                    upload.setLog(getValidationLog());

                    Iterator<Map.Entry<String, File>> i = filesMap.entrySet().iterator();

                    if (filesMap.size() == 0) {
                        //upload = new UploadedFile(input, baseDir.getAbsolutePath());
                        //upload.setLog(getValidationLog());
                        files.add(UploadedFileDAO.instanceWithDefaultEntityManager().saveEntity(upload));
                    }
                    while (i.hasNext() && !isFileUploaded) {
                        Map.Entry<String, File> entry = i.next();
                        setFileName(entry.getKey());
                        LOG.warn("File Name : " + getFileName());
                        if ((fileName.contains(input.getKeyword()) && fileName.contains(input.getMessageType().getValue()))
                                || (!fileName.contains(input.getKeyword()) && fileName.contains(input.getMessageType().getValue()))) {
                            persistFile(upload, filesMap);
                            isFileUploaded = true;
                        }
                        /*switch (input.getMessageType()) {
                            case REQUEST:
                                // save request file
                                if (fileName.contains(input.getKeyword()) || fileName.contains("request")) {
                                    prepareAndPersist(upload, filesMap);
                                    isFileUploaded = true;
                                }
                                break;

                            case RESPONSE:
                                // save response file
                                if (fileName.contains(input.getKeyword()) || fileName.contains("response")) {
                                    prepareAndPersist(upload, filesMap);
                                    isFileUploaded = true;
                                }
                                break;

                            case MANIFEST:
                                // save manifest file
                                if (fileName.contains(input.getKeyword()) || fileName.contains("manifest")) {
                                    prepareAndPersist(upload, filesMap);
                                    isFileUploaded = true;
                                }
                                break;

                            case ATTACHMENT:
                                //if files have no attachment(s) but validator needs one
                                if (!containsFileAsAttachment && testIfValidatorNeedsAttachment()) {
                                    upload = new UploadedFile(input, baseDir.getAbsolutePath());
                                    upload.setLog(getValidationLog());
                                    files.add(UploadedFileDAO.instanceWithDefaultEntityManager().saveEntity(upload));
                                    isFileUploaded = true;
                                } else {
                                    // save attachment file
                                    if (fileName.contains("extracted_attachment")) {
                                        prepareAndPersist(upload, filesMap);
                                        isFileUploaded = true;
                                    }
                                }
                                break;

                                default:
                        }*/
                    }
                    if (!isFileUploaded && !filesMap.isEmpty()) {
                        files.add(UploadedFileDAO.instanceWithDefaultEntityManager().saveEntity(upload));
                    }
                }
                //remove unuploaded files from the map
                if (!filesMap.isEmpty()) {
                    saveAndRemoveAllUnuploadedFilesFromMap(filesMap);
                }
                testIfAllInputsHaveFile();
            } else {
                LOG.error("No input defined");
            }
        }
    }

    /**
     * Insert files contained in the string table keys inside a map (<file name, file content>)
     * @param filesMap Map of files sent by client
     *
     */
    private void insertFilesIntoMap(Map<String, File> filesMap) {
        int fileIndex = 0;
        File fileToUpload;
        int keysLength = keys.length;
        if (getValidatorHasSwitched() && containsFileAsAttachment) {
            keysLength = getFilesList().size();
        }
        setContainsFileAsAttachment(false);
        while (fileIndex < keysLength) {
            if (!getValidatorHasSwitched()) {
                fileToUpload = new File(Upload.getFile(this.keys[fileIndex]));
                filesMap.put(fileToUpload.getName(), fileToUpload);
                getFilesList().add(fileToUpload);

                MTOMSplitter mtomSplitter = new MTOMSplitter();
                List<File> attachmentFiles = mtomSplitter.extractAttachment(fileToUpload);
                if (attachmentFiles != null) {
                    for (File attachmentFile : attachmentFiles) {
                        filesMap.put(attachmentFile.getName(), attachmentFile);
                        getFilesList().add(attachmentFile);
                        setContainsFileAsAttachment(true);
                    }
                }
            } else {
                fileToUpload = getFilesList().get(fileIndex);
                if (fileToUpload.getName().contains("EXTRACTED_ATTACHMENT")) {
                    setContainsFileAsAttachment(true);
                }
                filesMap.put(fileToUpload.getName(), fileToUpload);
            }
            fileIndex++;
        }
    }

    /**
     * Save and remove all the files from the map, espacially when switching from validators in the menu
     * @param filesMap Map of files sent by client
     *
     */
    private void saveAndRemoveAllUnuploadedFilesFromMap(Map<String, File> filesMap) {
        Iterator<Map.Entry<String, File>> i = filesMap.entrySet().iterator();
        while (i.hasNext()) {
            Map.Entry<String, File> entry = i.next();
            File fileToEmpty = entry.getValue();
            if (!files.isEmpty()) {
                String newLocation = files.get(0).getDirectory() + File.separatorChar + entry.getKey();
                try {
                    saveFileUploaded(newLocation, fileToEmpty);
                } catch (IOException e) {
                    LOG.info("Error in empty function");
                }
                filesMap.remove(entry.getKey());
            }
        }
    }

    /**
     * Remove useless extracted attachments from map
     * @param filesMap Map of files sent by client
     *
     */
    private void removeAttachmentFromMapIfNotNeeded(Map<String, File> filesMap) {
        if (containsFileAsAttachment) {
            if (!testIfValidatorNeedsAttachment()) {
                filesMap.remove("EXTRACTED_ATTACHMENT." + messageTypeFromProxy);
                LOG.info("Attachment removed in filesMap because not needed by X Validator");
            }
        }
    }

    /**
     * Test if one of the validator's inputs needs an attachment
     *
     */
    private boolean testIfValidatorNeedsAttachment() {
        List<ValidatorInput> inputs = getValidatorInputsForValidator();
        for (ValidatorInput input : inputs) {
            if (input.getMessageType().equals(MessageType.ATTACHMENT)) {
                LOG.info("Attachment needed for this X Validator");
                return true;
            }
        }
        LOG.info("No attachment needed for this X Validator");
        return false;
    }

    /**
     * Test if all validator's inputs are filled with files from client
     *
     */
    private void testIfAllInputsHaveFile() {
        int nbFilesInput = getValidator().getValidatedObjects().size();
        int nbFilesUploaded = 0;
        for (int index = 0;index<nbFilesInput && index<this.files.size();index++) {
            List<String> files = this.files.get(index).getFiles();
            if (files.size() != 0) {
                nbFilesUploaded++;
            }
        }
        if (nbFilesUploaded<nbFilesInput) {
            setAllInputsWithFiles(false);
        } else {
            setAllInputsWithFiles(true);
        }
    }

    /**
     * Persist file by adding it to the Uploaded file list and removing it from files map
     * @param upload Uploaded file to be persisted
     * @param filesMap Map of files sent by client
     *
     */
    private void persistFile(UploadedFile upload, Map<String, File> filesMap) {
        upload.addFile(fileName);
        setCurrentFile(upload);
        File file = filesMap.get(fileName);
        String newLocation = currentFile.getDirectory() + File.separatorChar + fileName;
        try {
            saveFileUploaded(newLocation, file);
        } catch (IOException e) {
            LOG.error("Error with file while saving" + e.getMessage());
        }
        files.add(UploadedFileDAO.instanceWithDefaultEntityManager().saveEntity(currentFile));
        filesMap.remove(fileName);
    }

    /**
     * Save file uploaded
     * @param newLocation Local file location
     * @param tmpFile Temporary file created to
     *
     */
    private void saveFileUploaded(String newLocation, File tmpFile) throws IOException {
        String stringFromFile = convertBase64ToString(tmpFile);
        InputStream inputStream = new FileInputStream(tmpFile);
        writeInFile(newLocation, inputStream, stringFromFile);
    }

    /**
     * Write file content in local location
     * @param newLocation Local file location
     * @param inputStream Input stream to write file content
     * @param str File which content has to be converted
     *
     */
    private void writeInFile(String newLocation, InputStream inputStream, String str) {
        java.io.File fileToStore = new java.io.File(newLocation);
        fileToStore.getParentFile().mkdirs();
        if (fileToStore.getParentFile().mkdirs()) {
            LOG.info("Directory missing :" + fileToStore.getParentFile().getAbsolutePath());
        }
        FileOutputStream fos = null;
        try {
            if (fileToStore.exists()) {
                boolean isDeleted = fileToStore.delete();
                if (!isDeleted) {
                    LOG.error("Unable to delete " + fileToStore.getParentFile().getAbsolutePath());
                }
            }
            fos = new FileOutputStream(fileToStore);
            fos.write(str.getBytes(StandardCharsets.UTF_8));
        } catch (FileNotFoundException e) {
            LOG.error(e.getMessage(), e);
            GuiMessage.logMessage(StatusMessage.Severity.ERROR, e.getMessage());
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
            GuiMessage.logMessage(StatusMessage.Severity.ERROR, e.getMessage());
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    LOG.error("" + e.getMessage());
                }
            }
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    LOG.error("" + e.getMessage());
                }
            }
        }
    }

    /**
     * Convert file base64 encoded file content to string
     * @param file File which content has to be converted
     *
     */
    private String convertBase64ToString(File file) throws IOException {

        InputStream inputStream = new FileInputStream(file);
        String stringFromFile = Streams.asString(inputStream);
        boolean isBase64 = isArrayByteBase64(stringFromFile.getBytes(StandardCharsets.UTF_8));
        inputStream.close();
        if (isBase64) {
            byte[] decodedStrBytes = DatatypeConverter.parseBase64Binary(stringFromFile);
            stringFromFile = new String(decodedStrBytes, StandardCharsets.UTF_8);
            return stringFromFile;
        } else {
            LOG.info("Could not convert from Base64 to string : your file isn't Base64 encoded");
            return stringFromFile;
        }
    }

    /**
     * Retrieve inputs from selected validator
     *
     */
    public List<ValidatorInput> getValidatorInputsForValidator() {
        return ValidatorInputDAO.instanceWithDefaultEntityManager().getInputsForGazelleCrossValidator(validator);
    }

    /**
     * Create directory from preference xvalidator_directory
     *
     */
    private void createDirectory() {
        String baseDirectory = PreferenceService.getString("xvalidator_directory");
        StringBuilder dir = new StringBuilder(baseDirectory);
        dir.append('/');
        dir.append(UploadedFile.BASE_DIR);
        dir.append('/');
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        dir.append(sdf.format(new Date()));
        baseDir = new File(dir.toString());
        if (!baseDir.mkdirs()) {
            LOG.error("Unable to create " + baseDir.getAbsolutePath());
        }
    }

    /**
     * Validate method called when performing a click on Validate button
     *
     */
    public String validate() {
        if (allInputsWithFiles) {
            Map<String, Object> filesToValidate = new HashMap<>();
            for (UploadedFile file : files) {
                for (String filename : file.getFilesAbsolutePaths()) {
                    try {
                        filesToValidate
                                .put(file.getInputKeyword(),
                                        FileConverter.file2Object(file.getInputObjectType(), filename,
                                                validator.getDicomLibrary()));
                    } catch (GazelleXValidationException e) {
                        GuiMessage.logMessage(StatusMessage.Severity.ERROR, e.getMessage());
                        LOG.error(e.getMessage(), e);
                    } catch (FileNotFoundException e) {
                        GuiMessage.logMessage(StatusMessage.Severity.ERROR, "An error occurred when reading file " + file.getInputKeyword());
                    }
                }
            }
            try {
                GazelleCrossValidatorType reloadValidator = GazelleCrossValidatorTypeDAO.instanceWithDefaultEntityManager()
                        .getValidatorByUrlParameters(validator.getId(), validator.getOid(), null, null, null);
                ValidationEngineWithDB engine = new ValidationEngineWithDB(reloadValidator, filesToValidate);
                engine.validate();
                CrossValidationLogDAO dao = CrossValidationLogDAO.instanceWithDefaultEntityManager();
                setValidationLog(dao.find(getValidationLog().getId()));
                setDetailedResult(engine.getReportAsString());
                setValidationResult(engine.getStatus());
                dao.persistEntity(getValidationLog());
                CrossValidationLog.setOidFromRootOid(getValidationLog());
                getValidationLog().setValidatedFiles(files);
                dao.saveEntity(getValidationLog());
                displayResult = true;
                GuiMessage.logMessage(StatusMessage.Severity.INFO, "Cross Validation Performed");
                return getValidationLog().getPermanentLink();
            } catch (GazelleXValidationException e) {
                LOG.error(e.getMessage(), e);
                GuiMessage.logMessage(StatusMessage.Severity.ERROR, e.getMessage());
            } catch (IOException e) {
                LOG.error(e.getMessage(), e);
                GuiMessage.logMessage(StatusMessage.Severity.ERROR, e.getMessage());
            }
        } else {
            LOG.error("All inputs have not been filled");
            GuiMessage.logMessage(StatusMessage.Severity.ERROR, "All inputs have not been filled");
        }
        return null;
    }

    /**
     * Listener called when the validators menu value changes
     * @param event
     *
     */
    public void validatorSwitchListener(ValueChangeEvent event) {
        GazelleCrossValidatorType newVal = (GazelleCrossValidatorType) event.getNewValue();
        GazelleCrossValidatorType oldVal = (GazelleCrossValidatorType) event.getOldValue();
        reset();
        if (newVal != null) {
            if (!newVal.equals(oldVal)) {
                setValidator(newVal);
                if (isDisplayResult()) {
                    setDisplayResult(false);
                }
                setValidationLog(new CrossValidationLog());
                initializeValidator();
            }
        }
    }

    /**
     * Listener called when uploading a file with an input
     * @param event
     *
     */
     public void fileUploadListener(FileUploadEvent event) throws IOException {
        String filename = event.getUploadedFile().getName();
        File tmpFile = File.createTempFile("uploaded", null);
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(tmpFile);
            fos.write(event.getUploadedFile().getData());
        } catch (Exception e){
            LOG.error("Error in fos remotexvalidator: " + e.getMessage());
        } finally {
            if (fos != null) {
                fos.close();
            }
        }
        String newLocation = currentFile.getDirectory() + File.separatorChar + filename;
        saveFileUploaded(newLocation, tmpFile);
        currentFile.addFile(filename);
        files.add(UploadedFileDAO.instanceWithDefaultEntityManager().saveEntity(currentFile));
        testIfAllInputsHaveFile();
    }

    public int getMaxInput() {
        if (this.currentFile == null) {
            return 0;
        }
        if (this.currentFile.getInputMax() < 0) {
            return 100000;
        }
        return this.currentFile.getInputMax();
    }

    public boolean isUploadNeeded() {
        if (this.currentFile == null) {
            return false;
        }
        if (this.currentFile.getInputMax() < 0) {
            return true;
        }
        return this.currentFile.getFiles().size() < this.currentFile.getInputMax();
    }

    @Destroy
    @Override
    public void reset() {
        LOG.info("reset method has been called");
        super.reset();
        files = null;
        validator = null;
        displayResult = false;
    }


}
