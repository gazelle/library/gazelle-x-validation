package net.ihe.gazelle.xvalidation.menu;

import net.ihe.gazelle.common.pages.Authorization;
import net.ihe.gazelle.common.pages.Page;

public enum XValidationPages implements
		Page {

	ADMIN_VALIDATORS("/xvalidation/admin/validators", "fa-gears", "net.ihe.gazelle.xval.ManageGazelleCrossValidators", XValidationAuthorizations.TEST_DESIGNER, XValidationAuthorizations.EDITOR),
	ADMIN_RULES("/xvalidation/admin/rules", "fa-pencil", "Manage rules", XValidationAuthorizations.TEST_DESIGNER, XValidationAuthorizations.EDITOR),
	ADMIN_ARCHIVES("/xvalidation/admin/archives", "fa-archive", "Browse deprecated validators", XValidationAuthorizations.ADMIN, XValidationAuthorizations.EDITOR),
	ADMIN_STATS("/xvalidation/admin/stats", "fa-pie-chart", "Usage statistics", XValidationAuthorizations.TEST_DESIGNER, XValidationAuthorizations.EDITOR),
	ADMIN_REFERENCES("/xvalidation/admin/referencedObjects", "fa-file", "Manage referenced objects", XValidationAuthorizations.TEST_DESIGNER, XValidationAuthorizations.EDITOR),
	ADMIN_EDIT_VALIDATOR("/xvalidation/admin/editValidator", "fa-pencil", "Edit validator", XValidationAuthorizations.TEST_DESIGNER, XValidationAuthorizations.EDITOR),
	ADMIN_EDIT_INPUT("/xvalidation/admin/editInput", "fa-pencil", "Edit input", XValidationAuthorizations.TEST_DESIGNER, XValidationAuthorizations.EDITOR),
	ADMIN_EDIT_RULE("/xvalidation/admin/editRule", "fa-pencil", "Edit rule", XValidationAuthorizations.TEST_DESIGNER, XValidationAuthorizations.EDITOR),
	ADMIN_EDIT_REFOBJECT("/xvalidation/admin/editReferencedObject", "fa-pencil", "Edit referenced object", XValidationAuthorizations.TEST_DESIGNER, XValidationAuthorizations.EDITOR),
	ADMIN_IMPORT_VALIDATORS("/xvalidation/admin/importValidators", "fa-plus", "net.ihe.gazelle.xval.ImportUpdateGazelleXValidators", XValidationAuthorizations.ADMIN),
	ADMIN_RULE_UNIT_TEST("/xvalidation/testing/ruleUnitTestEditor", "fa-plus", "Unit tests for rule editor", XValidationAuthorizations.TEST_DESIGNER),
	ADMIN_RULE_UNIT_TEST_LOGS("/xvalidation/testing/ruleUnitTestLogs", "fa-check-square-o", "Rule unit test logs", XValidationAuthorizations.TEST_DESIGNER),
	ADMIN_RULE_UNIT_TEST_LOG_DETAILS("/xvalidation/testing/ruleUnitTestLogDetails", "fa-check-square-o", "Rule unit test log details", XValidationAuthorizations.TEST_DESIGNER),
	ADMIN_BROWSE_UT("/xvalidation/testing/browseUnitTests", "fa-check-square-o", "Browse unit tests for rules", XValidationAuthorizations.TEST_DESIGNER),
	DOC_HOME("/xvalidation/doc/home", "fa-book", "net.ihe.gazelle.xval.doc"),
	DOC_RULES("/xvalidation/doc/rules", "fa-book", "X Validation Rules"),
	DOC_VALIDATOR("/xvalidation/doc/validator", "fa-book", "Validator details"),
	DOC_RULE("/xvalidation/doc/rule", "fa-book", "Rule details"),
	DOC_REFERENCE("/xvalidation/doc/referencedObject", "fa-book", "Referenced object details"),
	DOC_REFERENCES("/xvalidation/doc/referencedObjects", "fa-book", "Referenced objects"),
	VAL_VALIDATION("/xvalidation/validate/validator", "fa-play", "net.ihe.gazelle.xval.GazelleCrossValidator", XValidationAuthorizations.VALIDATOR),
	VAL_LOGS("/xvalidation/validate/logs", "fa-list-alt", "net.ihe.gazelle.xval.logs", XValidationAuthorizations.VALIDATOR),
	VAL_REPORT("/xvalidation/validate/report", "fa-list-alt", "Validation report", XValidationAuthorizations.VALIDATOR),
	VAL_REMOTE("/xvalidation/validate/remoteXValidator", "", "", XValidationAuthorizations.VALIDATOR);

	private String link;

	private Authorization[] authorizations;

	private String label;

	private String icon;

	XValidationPages(String link, String icon, String label) {
		this(link, icon, label, XValidationAuthorizations.LOGGED);
	}

	XValidationPages(String link, String icon, String label, Authorization... authorizations) {
		this.link = link;
		this.authorizations = authorizations;
		this.label = label;
		this.icon = icon;
	}

	@Override
	public String getId() {
		return name();
	}

	public String getMenuLink() {
		return link + ".seam";
	}

	@Override
	public String getLink() {
		return geXhtmltLink();
	}

	public String geXhtmltLink() {
		return link + ".xhtml";
	}

	public String getSeamLink() {
		return link + ".seam";
	}

	@Override
	public String getIcon() {
		return icon;
	}

	@Override
	public Authorization[] getAuthorizations() {
		return authorizations;
	}

	@Override
	public String getLabel() {
		return label;
	}

}
