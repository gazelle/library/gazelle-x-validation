package net.ihe.gazelle.xvalidation.refobject.action;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.xvalidation.core.model.ReferencedObject;
import net.ihe.gazelle.xvalidation.core.model.ReferencedObjectQuery;

import java.io.Serializable;

public abstract class AbstractReferencedObjectBrowser implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private Filter<ReferencedObject> filter;

    public Filter<ReferencedObject> getFilter() {
        if (filter == null) {
            this.filter = new Filter<ReferencedObject>(getHQLCriterionsForFilter());
        }
        return this.filter;
    }

    public void resetFilter() {
        getFilter().clear();
    }

    private HQLCriterionsForFilter<ReferencedObject> getHQLCriterionsForFilter() {
        ReferencedObjectQuery query = new ReferencedObjectQuery();
        HQLCriterionsForFilter<ReferencedObject> criterions = query.getHQLCriterionsForFilter();
        criterions.addPath("kind", query.objectType());
        return criterions;
    }

    public FilterDataModel<ReferencedObject> getReferencedObjects() {
        return new FilterDataModel<ReferencedObject>(getFilter()) {
            @Override
            protected Object getId(ReferencedObject t) {
                // TODO Auto-generated method stub
                return t.getId();
            }
        };
    }

    public abstract String listValidators();

    public abstract String listRules();
}
