package net.ihe.gazelle.xvalidation.action;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;

import net.ihe.gazelle.xvalidation.dao.CrossValidationLogDAO;
import net.ihe.gazelle.xvalidation.model.UploadedFile;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Name("gcvResultDisplayManager")
@Scope(ScopeType.PAGE)
public class GazelleCrossValidatorResultDisplayManager extends AbstractResultDisplay implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 5401851682540370255L;

	private static Logger log = LoggerFactory.getLogger(GazelleCrossValidatorResultDisplayManager.class);

	@Create
	public void initializePage() {
		Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		String oid = params.get("oid");
		if (oid != null) {
			setValidationLog(CrossValidationLogDAO.instanceWithDefaultEntityManager().getLogByOid(oid));
			String privacyKey = null;
			if (params.containsKey("privacyKey")) {
				privacyKey = params.get("privacyKey");
			}
			setUserAllowed(currentUserIsAllowedToAccessLog(privacyKey));
		} else {
			log.error("oid parameter is missing");
		}
	}

	public List<UploadedFile> getValidatedFiles() {
		return getValidationLog().getValidatedFiles();
	}
}
