package net.ihe.gazelle.xvalidation.attachmentanalyzer;

import net.ihe.gazelle.evsclient.interlay.gui.GuiMessage;
import net.ihe.gazelle.xvalidation.action.RemoteXValidator;
import org.apache.commons.fileupload.util.Streams;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.apache.commons.codec.binary.Base64.isArrayByteBase64;

public class MTOMSplitter {

    private static final Logger LOGGER = LoggerFactory.getLogger(MTOMSplitter.class);

    private static String MTOM_FIRST_LINE_MATCH = "^([ ])*--([^>].+)";

    private final static String ATTACHMENT_TAG = "Include" + ".*" + "href=";
    private TagDetector tagDetector;

    public List<File> extractAttachment(File file) {

        String fileContent = null;

        try {
            fileContent = convertBase64ToString(file);
        } catch (IOException e) {
            LOGGER.info("Couldn't extract file content");
        }

        if (fileContent != null) {

            String[] string = splitMTOM(fileContent);

            if (string != null) {
                List<File> files = createFilesFromString(string,file);
                return files;
            } else {
                return null;
            }

        } else {
            return null;
        }

    }

    private List<File> createFilesFromString(String[] fileContent, File file) {

        List<File> files = new ArrayList<>();

        for (int index = 0; index < fileContent.length; index++) {

            if (fileContent[index] != null) {

                //Create file
                try {
                    File newFile = new File(file.getAbsolutePath());
                    OutputStream fos = new FileOutputStream(newFile);
                    try {
                        fos.write(fileContent[index].getBytes(StandardCharsets.UTF_8));
                    } catch (IOException e) {
                        LOGGER.error("Error while writing in createFile function", e);
                        GuiMessage.logMessage(StatusMessage.Severity.ERROR, e.getMessage());
                        return null;
                    } finally {
                        try {
                            fos.close();
                        } catch (IOException e) {
                            LOGGER.error("Impossible to close stream error", e);
                            GuiMessage.logMessage(StatusMessage.Severity.ERROR, e.getMessage());
                        }
                    }

                    String attachmentTag = detectAttachment(fileContent[index]);

                    if (attachmentTag != null) {
                        //we have an attachments
                        File attachmentFile = createFileFromAttachment(fileContent[index+1]);
                        index++;
                        files.add(attachmentFile);
                    }

                } catch (FileNotFoundException e) {
                    LOGGER.error("File not found in createFile function", e);
                    GuiMessage.logMessage(StatusMessage.Severity.ERROR, e.getMessage());
                    return null;
                }
            }
        }
        return files;
    }


    private String convertBase64ToString(File file) throws IOException {

        InputStream inputStream = new FileInputStream(file);
        String stringFromFile = Streams.asString(inputStream);
        boolean isBase64 = isArrayByteBase64(stringFromFile.getBytes(StandardCharsets.UTF_8));
        inputStream.close();
        if (isBase64) {
            //Convert from base64
            byte[] decodedStrBytes = DatatypeConverter.parseBase64Binary(stringFromFile);
            stringFromFile = new String(decodedStrBytes, StandardCharsets.UTF_8);
            return stringFromFile;
        } else {
            LOGGER.info("Could not convert from Base64 to string : your file isn't Base64 encoded");
            return stringFromFile;
        }
    }

    private String [] splitMTOM(String messageContent) {

        int end, begin;
        String firstLineMatch;
        final ArrayList<Integer> offsetStartTab = new ArrayList<>();
        final ArrayList<Integer> offsetEndTab = new ArrayList<>();

        Pattern regex = Pattern.compile(MTOM_FIRST_LINE_MATCH);
        Matcher regexMatcher = regex.matcher(messageContent);
        if (regexMatcher.find()) {

            LOGGER.info("We got a first line match !");

            firstLineMatch = regexMatcher.group();

            final String mtomHeader = "(" + firstLineMatch + ")((\\n|\\r|\\r\\n| )*((((C|c)ontent-.*(\\n|\\r|\\r\\n| )*))*)|--)";
            regex = Pattern.compile(mtomHeader, Pattern.MULTILINE);
            regexMatcher = regex.matcher(messageContent);
            while (regexMatcher.find()) {
                LOGGER.info("group : {} start :{} end :{}",
                        new Object[]{regexMatcher.group(), Integer.valueOf(regexMatcher.start()), Integer.valueOf(regexMatcher.end())});

                offsetStartTab.add(regexMatcher.start());
                offsetEndTab.add(regexMatcher.end());
            }
            if (offsetStartTab.size() > 1) {
                // Remove the last element
                offsetEndTab.remove(offsetEndTab.size() - 1);
            }

            String [] fileContentSplitted = new String[offsetEndTab.size()];


            for (int j = 0; j < offsetEndTab.size(); j++) {

                begin = offsetEndTab.get(j);
                end = offsetStartTab.get(j+1);

                fileContentSplitted[j] = messageContent.substring(begin,end);

            }
            return fileContentSplitted;
        }
        return null;
    }

    private String detectAttachment(String stringFromFile) {


        //Attachment detection
        tagDetector = new TagDetector();

        String attachmentTag = tagDetector.detectTags(stringFromFile, ATTACHMENT_TAG);
        if (attachmentTag != null) {
            //We have an attachment in either the request or the response
            LOGGER.info("We have an attachment inside this file");
            return attachmentTag;
        } else {
            return null;
        }

    }

    private File createFileFromAttachment(String tagResult) {
        try {
            File attachmentFile = new File(RemoteXValidator.getBaseDir() + File.separator + "EXTRACTED_ATTACHMENT." + RemoteXValidator.getMessageTypeFromProxy());
            OutputStream fos = new FileOutputStream(attachmentFile);
            try {
                fos.write(tagResult.getBytes(StandardCharsets.UTF_8));
            } catch (IOException e) {
                LOGGER.error("Error while writing in createFileFromAttachment function", e);
                GuiMessage.logMessage(StatusMessage.Severity.ERROR, e.getMessage());
                return null;
            } finally {
                try {
                    fos.close();
                } catch (IOException e) {
                    LOGGER.error("Impossible to close stream error", e);
                    GuiMessage.logMessage(StatusMessage.Severity.ERROR, e.getMessage());
                }
            }
            return attachmentFile;
        } catch (IOException e) {
            LOGGER.error("Couldn't create temp files", e);
            GuiMessage.logMessage(StatusMessage.Severity.ERROR, e.getMessage());
            return null;
        }
    }
}
