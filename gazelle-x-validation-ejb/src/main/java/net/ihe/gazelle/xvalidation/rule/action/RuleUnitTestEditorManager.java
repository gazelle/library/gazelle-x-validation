package net.ihe.gazelle.xvalidation.rule.action;

import net.ihe.gazelle.evsclient.interlay.gui.GuiMessage;
import net.ihe.gazelle.validator.ut.action.UnitTestEditorManager;
import net.ihe.gazelle.validator.ut.model.FileType;
import net.ihe.gazelle.validator.ut.model.UnitTestFile;
import net.ihe.gazelle.xvalidation.core.model.Rule;
import net.ihe.gazelle.xvalidation.core.model.ValidatorInput;
import net.ihe.gazelle.xvalidation.dao.RuleDAO;
import net.ihe.gazelle.xvalidation.dao.RuleUnitTestDAO;
import net.ihe.gazelle.xvalidation.dao.ValidatorInputDAO;
import net.ihe.gazelle.xvalidation.model.RuleUnitTest;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import java.util.List;
import java.util.Map;

@Name("ruleUnitTestEditorManager")
@Scope(ScopeType.PAGE)
public class RuleUnitTestEditorManager extends UnitTestEditorManager<RuleUnitTest> {

    /**
     *
     */
    private static final long serialVersionUID = 4542574468783272339L;

    private Rule rule;
    private List<ValidatorInput> inputsForRule = null;
    private boolean displayListOfInputs;
    private String oldKeyword;

    @Override
    @Create
    public void init() {
        Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (urlParams.containsKey("rule")) {
            rule = RuleDAO.instanceWithDefaultEntityManager().getRuleByKeyword(urlParams.get("rule"));
            inputsForRule = rule.getGazelleCrossValidator().getValidatedObjects();
        } else {
            rule = null;
        }
        if (urlParams.containsKey("unitTest")) {
            setUnitTest(RuleUnitTestDAO.instanceWithDefaultEntityManager().getUnitTestByKeyword(
                    urlParams.get("unitTest")));
            oldKeyword = getUnitTest().getKeyword();
        } else {
            setUnitTest(null);
        }
        if (rule == null && getUnitTest() == null) {
            GuiMessage.logMessage(StatusMessage.Severity.WARN, "At least the rule keyword or the unit test keyword is requested");
        } else if (getUnitTest() == null) {
            setUnitTest(new RuleUnitTest(rule));
            oldKeyword = getUnitTest().getKeyword();
        } else if (rule == null) {
            rule = getUnitTest().getTestedRule();
            inputsForRule = getInputsForRule();
        }
    }

    public void save() {
        setUnitTest(RuleUnitTestDAO.instanceWithDefaultEntityManager().saveEntity(getUnitTest()));
        GuiMessage.logMessage(StatusMessage.Severity.INFO, "Unit test has been saved");
    }

    public Rule getRule() {
        return rule;
    }

    public List<ValidatorInput> getInputsForRule() {
        if (inputsForRule == null && rule != null) {
            inputsForRule = rule.getGazelleCrossValidator().getValidatedObjects();
        }
        return inputsForRule;
    }

    public void createNewUnitTestFile(ValidatorInput input) {
        setUnitTestFile(new UnitTestFile(getUnitTest(), input.getKeyword(), FileType.getFileTypeByValue(input
                .getReferencedObject().getObjectType().getValue())));
        this.displayListOfInputs = false;
        inputsForRule.remove(input);
    }

    public boolean isDisplayListOfInputs() {
        return displayListOfInputs;
    }

    public void setDisplayListOfInputs(boolean displayListOfInputs) {
            this.displayListOfInputs = displayListOfInputs;
    }

    public void removeUnitTestFileFromInput(UnitTestFile unitTestFile) {
        deleteUnitTestFile(unitTestFile);
        ValidatorInput input = ValidatorInputDAO.instanceWithDefaultEntityManager().getInputByKeyword(unitTestFile.getInputKeyword(), rule);
        if (!inputsForRule.contains(input) && input.getKeyword() != null) {
            inputsForRule.add(input);
        }
    }

}
