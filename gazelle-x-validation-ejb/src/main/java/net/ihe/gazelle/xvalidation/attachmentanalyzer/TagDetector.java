package net.ihe.gazelle.xvalidation.attachmentanalyzer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TagDetector {


    private static final Logger LOGGER = LoggerFactory.getLogger(TagDetector.class);
    private File parentFile;

    public File getParentFile() {
        return parentFile;
    }

    public void setParentFile(File parentFile) {
        this.parentFile = parentFile;
    }


    public String detectTags(final String messageStringContent,String tag){

        final String regex = detectTagsInConfig(messageStringContent, tag);
        if (regex != null) {
            return regex;
        }

        return null;
    }

    private String detectTagsInConfig(final String messageStringContent, final String tag) {

        LOGGER.info("Search for tag \"{}\" in xml file", tag);

        // First search for a pattern of type <tag/>
        String dsubReg = "<((([^<]+-)+)?[^<]\\w+:)?\\b"+ tag;

        // Search for a pattern <tag>...</tag>
        //dsubReg = "<((([^<]+-)+)?[^<]\\w+:)?\\b"+tag+"\\b([\\r|\\n|\\w|\\W])+.+?/((([^<]+-)+)?[^<]\\w+:)?\\b"+tag+">";

        Pattern regex = Pattern.compile(dsubReg, Pattern.DOTALL);
        Matcher regexMatcher = regex.matcher(messageStringContent);

        if (regexMatcher.find()) {
            LOGGER.info("Tag Found in splitted form : {}",regexMatcher.group());
            return regexMatcher.group();
        }

        return null;
    }

}