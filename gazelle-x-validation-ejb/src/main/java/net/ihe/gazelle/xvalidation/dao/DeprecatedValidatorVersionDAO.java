package net.ihe.gazelle.xvalidation.dao;

import javax.persistence.EntityManager;

import net.ihe.gazelle.xvalidation.model.DeprecatedValidatorVersion;
import net.ihe.gazelle.xvalidation.model.DeprecatedValidatorVersionQuery;

public class DeprecatedValidatorVersionDAO extends DAO<DeprecatedValidatorVersion> {

	public DeprecatedValidatorVersionDAO() {
		super();
	}

	public DeprecatedValidatorVersionDAO(EntityManager entityManager) {
		super(entityManager);
	}

	public static DeprecatedValidatorVersionDAO instanceWithDefaultEntityManager() {
		return new DeprecatedValidatorVersionDAO();
	}

	@Override
	public Class<DeprecatedValidatorVersion> getEntityClass() {
		return DeprecatedValidatorVersion.class;
	}

	public boolean hasEntries(){
		DeprecatedValidatorVersionQuery query = new DeprecatedValidatorVersionQuery(entityManager);
		return query.getCount() > 0;
	}
}
