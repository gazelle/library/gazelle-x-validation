package net.ihe.gazelle.xvalidation.dao;

import net.ihe.gazelle.xvalidation.core.model.DateFormatType;
import net.ihe.gazelle.xvalidation.core.model.DateFormatTypeQuery;
import net.ihe.gazelle.xvalidation.core.model.ReferencedObject;

import java.util.List;

public class DateFormatTypeDAO extends DAO<DateFormatType> {

    @Override
    public Class<DateFormatType> getEntityClass() {
        return DateFormatType.class;
    }

    public List<DateFormatType> getAllEntities() {
        DateFormatTypeQuery query = new DateFormatTypeQuery(entityManager);
        query.label().order(true);
        return query.getList();
    }

    public static DateFormatTypeDAO instanceWithDefaultEntityManager() {
        return new DateFormatTypeDAO();
    }

}
