package net.ihe.gazelle.xvalidation.dao;

import java.util.List;

import javax.persistence.EntityManager;

import net.ihe.gazelle.xvalidation.core.model.Namespace;
import net.ihe.gazelle.xvalidation.core.model.NamespaceQuery;

public class NamespaceDAO extends DAO<Namespace> {

	public NamespaceDAO() {
		super();
	}

	public NamespaceDAO(EntityManager entityManager) {
		super(entityManager);
	}

	public static NamespaceDAO instanceWithDefaultEntityManager() {
		return new NamespaceDAO();
	}

	@Override
	public Class<Namespace> getEntityClass() {
		return Namespace.class;
	}

	public List<String> getUsedNamespaceUri() {
		NamespaceQuery query = new NamespaceQuery(entityManager);
		return query.uri().getListDistinct();
	}

}
