package net.ihe.gazelle.xvalidation.action;

import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.xvalidation.model.CrossValidationLog;
import net.ihe.gazelle.xvalidation.utils.Exporter;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.XMLConstants;
import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.*;

@Name("reportIOManager")
@Scope(ScopeType.STATELESS)
public class ReportIOManager extends Exporter implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 6947549824934918814L;
	private static Transformer transformer;

	private static Logger log = LoggerFactory.getLogger(ReportIOManager.class);

	private static void initializeTransformer() {
		TransformerFactory factory = TransformerFactory.newInstance();
		// to be compliant, prohibit the use of all protocols by external entities (if possible)
		try {
			factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
			factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
		} catch (IllegalArgumentException iae) {
			log.warn("initialize-transformer-warn "+iae.getMessage());
		}
        String xvalidatorXsl = PreferenceService.getString("x_validation_report_xsl_url");
		try {
		    transformer = factory.newTransformer(new StreamSource(xvalidatorXsl));
			transformer.setParameter("xvalidationPath", PreferenceService.getString("application_url"));
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF8");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		} catch (TransformerConfigurationException e) {
			transformer = null;
			log.error("Cannot instanciate transformer: " + e.getMessage(), e);
		}
	}

	static {
		initializeTransformer();
	}

	public void exportReportToFile(final CrossValidationLog validationLog) {
		if ((validationLog != null) && (validationLog.getDetailedResult() != null)) {
			exportReportToFile(validationLog.getDetailedResultAsByteArray(),validationLog.getOid() + ".xml");
		}
	}
	public void exportReportToFile(final byte[] report, String filename) {
		if ((report != null)) {
			try {
				getOutputStream().write(report);
				redirectExport("application/xml", filename);
			} catch (IOException e) {
				log.error(e.getMessage(), e);
			}
		}
	}

	public String exportReportToHtml(final CrossValidationLog validationLog) {
		if ((validationLog == null) || (validationLog.getDetailedResult() == null)) {
			return null;
		}
		return exportReportToHtml(validationLog.getDetailedResultAsByteArray());
	}

	public String exportReportToHtml(final byte[] report) {
		if (report == null) {
			return null;
		}
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		StreamResult out = new StreamResult(baos);
		InputStream is = new ByteArrayInputStream(report);
		try {
			if (transformer == null) {
				initializeTransformer();
			}
			synchronized (transformer) {
				transformer.transform(new javax.xml.transform.stream.StreamSource(is), out);
			}
		} catch (TransformerException e) {
			log.error(e.getMessage(), e);
			return null;
		}
		String tmp = new String(baos.toByteArray());
		try {
			baos.close();
			is.close();
		} catch (IOException e) {
			log.warn(e.getMessage(), e);
		}
		return tmp;
	}
}
