package net.ihe.gazelle.xvalidation.action;

import java.util.Map;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.xvalidation.model.CrossValidationLog;
import net.ihe.gazelle.xvalidation.model.CrossValidationLogQuery;
import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.security.Identity;

@Name("gcvReportBrowser")
@Scope(ScopeType.PAGE)
public class GazelleCrossValidationReportBrowser implements QueryModifier<CrossValidationLog>, UserAttributeCommon {

	private static final long serialVersionUID = 7503025945155452870L;
	private Filter<CrossValidationLog> filter;

	@In(value="gumUserService")
	private UserService userService;

	public Filter<CrossValidationLog> getFilter() {
		if (filter == null) {
			filter = new Filter<CrossValidationLog>(getHQLCriterionsForFilter());
		}
		return filter;
	}

	private HQLCriterionsForFilter<CrossValidationLog> getHQLCriterionsForFilter() {
		CrossValidationLogQuery query = new CrossValidationLogQuery();
		HQLCriterionsForFilter<CrossValidationLog> criteria = query.getHQLCriterionsForFilter();
		criteria.addPath("validator", query.gazelleCrossValidator());
		criteria.addPath("affinityDomain", query.affinityDomain());
		criteria.addPath("result", query.result());
		criteria.addPath("timestamp", query.timestamp());
		criteria.addQueryModifier(this);
		return criteria;
	}

	public FilterDataModel<CrossValidationLog> getCrossValidationLogs() {
		return new FilterDataModel<CrossValidationLog>(getFilter()){
                    @Override
        protected Object getId(CrossValidationLog t) {
        // TODO Auto-generated method stub
        return t.getId();
        }
        };
	}

	public void reset() {
		getFilter().clear();
	}

	@Override
	public void modifyQuery(HQLQueryBuilder<CrossValidationLog> queryBuilder, Map<String, Object> filterValuesApplied) {
		if (!Identity.instance().isLoggedIn()) {
			queryBuilder.addEq("isPublic", true);
		} else if (!(Identity.instance().hasRole("admin_role") || Identity.instance().hasRole("monitor_role"))) {
			queryBuilder.addRestriction(HQLRestrictions.or(HQLRestrictions.eq("isPublic", true),
					HQLRestrictions.eq("username", Identity.instance().getCredentials().getUsername())));
		}
		queryBuilder.addOrder("timestamp", false);
		queryBuilder.addRestriction(HQLRestrictions.isNotNull("oid"));
	}

	@Override
	public String getUserName(String userId) {
		return userService.getUserDisplayNameWithoutException(userId);
	}

}
