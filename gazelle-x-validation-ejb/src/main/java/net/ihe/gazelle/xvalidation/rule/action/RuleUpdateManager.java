package net.ihe.gazelle.xvalidation.rule.action;

import net.ihe.gazelle.evsclient.interlay.gui.GuiMessage;
import net.ihe.gazelle.xvalidation.core.model.GazelleCrossValidatorType;
import net.ihe.gazelle.xvalidation.core.model.Rule;
import net.ihe.gazelle.xvalidation.core.model.RuleStatus;
import net.ihe.gazelle.xvalidation.core.model.Status;
import net.ihe.gazelle.xvalidation.dao.DeprecatedValidatorVersionDAO;
import net.ihe.gazelle.xvalidation.dao.GazelleCrossValidatorTypeDAO;
import net.ihe.gazelle.xvalidation.dao.RuleDAO;
import net.ihe.gazelle.xvalidation.menu.XValidationPages;
import net.ihe.gazelle.xvalidation.model.DeprecatedValidatorVersion;
import net.ihe.gazelle.xvalidation.validator.action.ValidatorIOManager;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

@Name("ruleUpdateManager")
@Scope(ScopeType.PAGE)
public class RuleUpdateManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3525857356343943726L;
    private Rule rule;
    private GazelleCrossValidatorType validator;
    private transient String comment;
    private boolean putValidatorInMaintenance = false;

    private String oldVersion;
    private String newVersion;

    public String editRule(Rule rule) {
        return XValidationPages.ADMIN_EDIT_RULE.getSeamLink() + "?id=" + rule.getId();
    }

    public String displayRule(Rule rule) {
        return XValidationPages.DOC_RULE.getSeamLink() + "?keyword=" + rule.getKeyword();
    }

    private String editValidator() {
        return XValidationPages.ADMIN_EDIT_VALIDATOR.getSeamLink() + "?id=" + validator.getId();
    }

    public void deprecateRule() {
        changeRuleStatus(RuleStatus.DEPRECATED);
        editValidator();
    }

    public void restoreRule() {
        changeRuleStatus(RuleStatus.INACTIVE);
        editRule(rule);
    }

    public String deactivateRule() {
        this.rule.setStatus(RuleStatus.INACTIVE);
        RuleDAO.instanceWithDefaultEntityManager().saveEntity(rule);
        return null;
    }

    public String activateRule() {
//        if (rule.getVersion().contains(GazelleCrossValidatorType.DEV_SUFFIX) && !oldVersion.contains(GazelleCrossValidatorType.COPY_SUFFIX)) {
//            oldVersion = rule.getVersion().replace(GazelleCrossValidatorType.DEV_SUFFIX, "");
//        } else if (rule.getVersion().contains(GazelleCrossValidatorType.COPYDEV_SUFFIX)) {
//            oldVersion = rule.getVersion().replace(GazelleCrossValidatorType.COPYDEV_SUFFIX, "0");
//        }
//        if (oldVersion.equals(newVersion) || (newVersion == null || newVersion.isEmpty())) {
//            GuiMessage.logMessage(StatusMessage.Severity.WARN, "The version of the rule shall be incremented");
//            return null;
//        } else {
//            rule.setVersion(newVersion);
//        }
//        this.rule.setStatus(RuleStatus.ACTIVE);
//        RuleDAO.instanceWithDefaultEntityManager().saveEntity(rule);
//        return displayRule(rule);
        if (rule.getVersion().contains(GazelleCrossValidatorType.DEV_SUFFIX)) {
            String oldVersion = rule.getVersion().replace(GazelleCrossValidatorType.DEV_SUFFIX, "");
            if (oldVersion.equals(newVersion) || (newVersion == null || newVersion.isEmpty())) {
                GuiMessage.logMessage(StatusMessage.Severity.WARN,"The version of the rule shall be incremented");
                return null;
            } else {
                rule.setVersion(newVersion);
            }
        }
        this.rule.setStatus(RuleStatus.ACTIVE);
        rule.setGazelleCrossValidator(GazelleCrossValidatorTypeDAO.instanceWithDefaultEntityManager().saveEntity(validator));
        RuleDAO.instanceWithDefaultEntityManager().saveEntity(rule);
        return displayRule(rule);
    }

    public String availableStatus(Rule rule) {
        if (rule.isActive()) {
            return "Inactive";
        } else {
            return "Active";
        }
    }

    public String activateOrDeactivateRule() {
        if (rule.isActive()) {
            return deactivateRule();
        } else {
            return activateRule();
        }
    }

    public boolean requestNewVersion() {
        return (rule.isInactive() && rule.getVersion().contains(GazelleCrossValidatorType.DEV_SUFFIX));
    }

    private void changeRuleStatus(RuleStatus newStatus) {
        comment = "Rule " + rule.getKeyword() + " has been set as " + newStatus.getValue();
        validator = GazelleCrossValidatorTypeDAO.instanceWithDefaultEntityManager().saveEntity(validator);
        if (validator.isAvailable()) {
            putValidatorInMaintenanceMode();
            FacesMessages
                    .instance()
                    .add("This validator has been turned into maintenance mode because one of its rules has been deprecated");
        }
        this.rule.setStatus(newStatus);
        RuleDAO.instanceWithDefaultEntityManager().saveEntity(rule);
        GazelleCrossValidatorTypeDAO.instanceWithDefaultEntityManager().saveEntity(validator);
    }

    public String updateRule() {
        if (putValidatorInMaintenance) {
            putValidatorInMaintenanceMode();
        }
        if (validator.isUnderMaintenance() && rule.isActive()) {
            this.rule.setStatus(RuleStatus.INACTIVE);
            oldVersion = rule.getVersion();
            this.rule.setVersion(oldVersion + GazelleCrossValidatorType.DEV_SUFFIX);
            RuleDAO.instanceWithDefaultEntityManager().saveEntity(rule);
        }
        return editRule(rule);
    }

    public String duplicateRule(Rule selectedRule) {
        RuleDAO dao = RuleDAO.instanceWithDefaultEntityManager();
        SimpleDateFormat sdf = new SimpleDateFormat("MMddHHmmss");
        String suffix = sdf.format(new Date());
        Rule copy = new Rule(dao.find(selectedRule.getId()));
        copy.setKeyword(selectedRule.getKeyword() + Rule.COPY_SUFFIX + suffix);
        copy = dao.saveEntity(copy);
        return XValidationPages.ADMIN_EDIT_RULE.getSeamLink() + "?id=" + copy.getId();
    }

    private boolean putValidatorInMaintenanceMode() {
        String filename = ValidatorIOManager.archiveValidator(validator);
        if (filename != null) {
            DeprecatedValidatorVersion deprecated = new DeprecatedValidatorVersion(validator, filename);
            if (comment != null && !comment.isEmpty()) {
                deprecated.appendComment(comment);
            }
            DeprecatedValidatorVersionDAO.instanceWithDefaultEntityManager().saveEntity(deprecated);
            oldVersion = validator.getVersion();
            validator.setVersion(oldVersion + GazelleCrossValidatorType.DEV_SUFFIX);
            validator.setStatus(Status.MAINTENANCE);
            validator = GazelleCrossValidatorTypeDAO.instanceWithDefaultEntityManager().saveEntity(validator);
            return true;
        } else {
            GuiMessage.logMessage(StatusMessage.Severity.ERROR, "Back up of the current version did not work");
            return false;
        }
    }

    public Rule getRule() {
        return rule;
    }

    public void setRule(Rule rule) {
//        this.rule = rule;
//        this.validator = rule.getGazelleCrossValidator();
//        this.putValidatorInMaintenance = validator.isAvailable() && rule.isActive();
//        oldVersion = rule.getVersion();
//        if (oldVersion.contains(GazelleCrossValidatorType.DEV_SUFFIX) && !oldVersion.contains(GazelleCrossValidatorType.COPY_SUFFIX)) {
//            String oldVersionModified = oldVersion.replace(GazelleCrossValidatorType.DEV_SUFFIX, "");
//            newVersion = String.valueOf(Integer.decode(oldVersionModified) + 1);
//        } else if (oldVersion.contains(GazelleCrossValidatorType.COPYDEV_SUFFIX)) {
//            String oldVersionModified = oldVersion.replace(GazelleCrossValidatorType.COPYDEV_SUFFIX, "0");
//            newVersion = String.valueOf(Integer.decode(oldVersionModified) + 1);
//        } else if (oldVersion.contains(Rule.COPY_SUFFIX)) {
//            newVersion = Rule.COPY_SUFFIX;
//        } else {
//            newVersion = String.valueOf(Integer.decode(oldVersion) + 1);
//        }
        this.rule = rule;
        this.validator = rule.getGazelleCrossValidator();
        this.putValidatorInMaintenance = validator.isAvailable() && rule.isActive();
        oldVersion = rule.getVersion();
    }

    public GazelleCrossValidatorType getValidator() {
        return validator;
    }

    public boolean isPutValidatorInMaintenance() {
        return putValidatorInMaintenance;
    }

    public String getNewVersion() {
        return newVersion;
    }

    public void setNewVersion(String newVersion) {
        this.newVersion = newVersion;
    }

    public String getOldVersion() {
        return oldVersion;
    }

    public void setOldVersion(String oldVersion) {
        this.oldVersion = oldVersion;
    }

    public void cancelUpdate() {
        rule = null;
        validator = null;
    }
}
