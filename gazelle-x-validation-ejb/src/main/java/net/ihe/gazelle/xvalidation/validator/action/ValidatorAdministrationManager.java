package net.ihe.gazelle.xvalidation.validator.action;

import net.ihe.gazelle.evsclient.interlay.gui.GuiMessage;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.xvalidation.core.model.GazelleCrossValidatorType;
import net.ihe.gazelle.xvalidation.core.model.GazelleCrossValidatorTypeQuery;
import net.ihe.gazelle.xvalidation.core.model.Status;
import net.ihe.gazelle.xvalidation.dao.DeprecatedValidatorVersionDAO;
import net.ihe.gazelle.xvalidation.dao.GazelleCrossValidatorTypeDAO;
import net.ihe.gazelle.xvalidation.menu.XValidationPages;
import net.ihe.gazelle.xvalidation.model.DeprecatedValidatorVersion;

import net.ihe.gazelle.xvalidation.model.RuleUnitTestQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

@Name("validatorAdministrationManager")
@Scope(ScopeType.PAGE)
public class ValidatorAdministrationManager extends AbstractValidatorBrowser {

	/**
	 *
	 */
	private static final long serialVersionUID = 1411881124459558128L;
	private static Logger log = LoggerFactory.getLogger(ValidatorAdministrationManager.class);

	private String comment;
	private GazelleCrossValidatorType validatorForUpdate;

    public boolean isOnlyWithFailedUT() {
        return onlyWithFailedUT;
    }

    public void setOnlyWithFailedUT(boolean onlyWithFailedUT) {
        this.onlyWithFailedUT = onlyWithFailedUT;
    }

    private boolean onlyWithFailedUT = false;

	@Override
	protected void addSpecificHQLCriterions(HQLCriterionsForFilter<GazelleCrossValidatorType> criterions,
			GazelleCrossValidatorTypeQuery query) {
		criterions.addPath("modifier", query.lastModifier());
		criterions.addPath("timestamp", query.lastModifiedDate());
		criterions.addPath("status", query.status());
	}

	public String editValidator(GazelleCrossValidatorType validator) {
		return XValidationPages.ADMIN_EDIT_VALIDATOR.getSeamLink() + "?id=" + validator.getId();
	}

	public String createValidator() {
		return XValidationPages.ADMIN_EDIT_VALIDATOR.getSeamLink();
	}

	@Override
	public String listRules() {
		return XValidationPages.ADMIN_RULES.getLink();
	}

	@Override
	public String listReferencedObjects() {
		return XValidationPages.ADMIN_REFERENCES.getLink();
	}

	public String importValidator() {
		return XValidationPages.ADMIN_IMPORT_VALIDATORS.getLink();
	}

	public String updateValidator() {
		String filename = ValidatorIOManager.archiveValidator(validatorForUpdate);
		if (filename != null) {
			DeprecatedValidatorVersion deprecated = new DeprecatedValidatorVersion(validatorForUpdate, filename);
			if (comment != null && !comment.isEmpty()) {
				deprecated.setComment(comment);
			}
			DeprecatedValidatorVersionDAO.instanceWithDefaultEntityManager().saveEntity(deprecated);
			validatorForUpdate.setVersion(validatorForUpdate.getVersion() + GazelleCrossValidatorType.DEV_SUFFIX);
			validatorForUpdate.setStatus(Status.MAINTENANCE);
			GazelleCrossValidatorTypeDAO.instanceWithDefaultEntityManager().saveEntity(validatorForUpdate);
			return editValidator(validatorForUpdate);
		} else {
			GuiMessage.logMessage(StatusMessage.Severity.ERROR, "Back up of the current version did not work");
			return null;
		}
	}

	public GazelleCrossValidatorType getValidatorForUpdate() {
		return validatorForUpdate;
	}

	public void setValidatorForUpdate(GazelleCrossValidatorType validatorForUpdate) {
		this.validatorForUpdate = validatorForUpdate;
	}

	public void deprecateValidator() {
		validatorForUpdate.setStatus(Status.DEPRECATED);
		GazelleCrossValidatorTypeDAO.instanceWithDefaultEntityManager().saveEntity(validatorForUpdate);
	}

	public void markAsAvailable() {
		if (validatorForUpdate.getVersion().contains(GazelleCrossValidatorType.DEV_SUFFIX)) {
			String version = validatorForUpdate.getVersion().replace(GazelleCrossValidatorType.DEV_SUFFIX, "");
			validatorForUpdate.setVersion(version);
		}
		validatorForUpdate.setStatus(Status.AVAILABLE);
		GazelleCrossValidatorTypeDAO.instanceWithDefaultEntityManager().saveEntity(validatorForUpdate);
        validatorForUpdate = null;
	}

	public void markAsMaintenance() {
		if (validatorForUpdate.getVersion().contains(GazelleCrossValidatorType.DEV_SUFFIX)) {
			validatorForUpdate.setStatus(Status.MAINTENANCE);
			GazelleCrossValidatorTypeDAO.instanceWithDefaultEntityManager().saveEntity(validatorForUpdate);
		} else {
			updateValidator();
		}
        validatorForUpdate = null;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

    @Override
    public void modifyQuery(HQLQueryBuilder<GazelleCrossValidatorType> queryBuilder,
                            Map<String, Object> filterValuesApplied){
        super.modifyQuery(queryBuilder, filterValuesApplied);
        if (onlyWithFailedUT){
            RuleUnitTestQuery utQuery = new RuleUnitTestQuery();
            utQuery.lastResult().eq(false);
            List<Integer> failed = utQuery.testedRule().gazelleCrossValidator().id().getListDistinct();
            if (failed != null && !failed.isEmpty()){
                GazelleCrossValidatorTypeQuery query = new GazelleCrossValidatorTypeQuery();
                queryBuilder.addRestriction(HQLRestrictions.not(query.id().inRestriction(failed)));
            }
        }
    }

	public void cancelUpdate() {
		validatorForUpdate = null;
	}
}
