package net.ihe.gazelle.xvalidation.dao;

import javax.persistence.EntityManager;

import net.ihe.gazelle.hql.providers.EntityManagerService;

public abstract class DAO<T> {

	protected EntityManager entityManager;

	public DAO() {
		entityManager = EntityManagerService.provideEntityManager();
	}

	public DAO(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public T saveEntity(T entity) {
		T savedEntity = entityManager.merge(entity);
		entityManager.flush();
		return savedEntity;
	}

	public T find(Integer id) {
		return entityManager.find(getEntityClass(), id);
	}

	public abstract Class<T> getEntityClass();
}
