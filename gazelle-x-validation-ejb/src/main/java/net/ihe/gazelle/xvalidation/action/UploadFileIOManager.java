package net.ihe.gazelle.xvalidation.action;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.evsclient.interlay.gui.GuiMessage;
import net.ihe.gazelle.validator.ut.model.UnitTestFile;
import net.ihe.gazelle.xvalidation.model.UploadedFile;
import net.ihe.gazelle.xvalidation.utils.Exporter;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jfree.util.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.net.URLConnection;

@Stateful
@Name("uploadFileIOManager")
@Scope(ScopeType.APPLICATION)
@GenerateInterface("UploadFileIOManagerLocal")
public class UploadFileIOManager extends Exporter implements Serializable, UploadFileIOManagerLocal {

	/**
	 *
	 */

	private static Logger log = LoggerFactory.getLogger(UploadFileIOManager.class);
	private static final long serialVersionUID = 3563125654697098212L;

	public void exportValidatedFile(UploadedFile uploadFile, String filename) {
		File file = new File(new File(uploadFile.getDirectory()), filename);
		if (file.exists()) {
			long length = file.length();
			byte[] fileContent = new byte[(int) length];
			FileInputStream fis = null;
			try {
				fis = new FileInputStream(file);
				fis.read(fileContent);
				String contentType = URLConnection.guessContentTypeFromStream(fis);
				fis.close();
				getOutputStream().write(fileContent);
				redirectExport(contentType, filename);
			} catch (FileNotFoundException e1) {
				log.error(e1.getMessage());
			} catch (IOException e) {
				GuiMessage.logMessage(StatusMessage.Severity.ERROR, e.getMessage());
				log.error(e.getMessage(), e);
				if (fis != null) {
					try {
						fis.close();
					} catch (IOException e1) {
						log.warn("The system has not been able to close an input stream", e1);
					}
				}
			} finally {
				try {
					if (fis != null) {
						fis.close();
					}
				} catch (IOException e) {
					log.error("Error while closing fis : " + e.getMessage());
				}
			}
		} else {
			GuiMessage.logMessage(StatusMessage.Severity.ERROR, "The system has not been able to retrieve the file");
		}
	}

	public void exportFile(UnitTestFile unitTestFile, String filename) {
		File file = new File(new File(unitTestFile.getDirectory()), filename);
		if (file.exists()) {
			long length = file.length();
			byte[] fileContent = new byte[(int) length];
			FileInputStream fis = null;
			try {
				fis = new FileInputStream(file);
				fis.read(fileContent);
				String contentType = URLConnection.guessContentTypeFromStream(fis);
				fis.close();
				getOutputStream().write(fileContent);
				redirectExport(contentType, filename);
			} catch (FileNotFoundException e1) {
				Log.error(e1.getMessage());
			} catch (IOException e) {
				GuiMessage.logMessage(StatusMessage.Severity.ERROR, e.getMessage());
				Log.error(e.getMessage(), e);
				if (fis != null) {
					try {
						fis.close();
					} catch (IOException e1) {
						Log.warn("The system has not been able to close an input stream", e1);
					}
				}
			} finally {
				try {
					if (fis != null) {
						fis.close();
					}
				} catch (IOException e) {
					Log.error("Error while closing fis : " + e.getMessage());
				}
			}
		} else {
			GuiMessage.logMessage(StatusMessage.Severity.ERROR, "The system has not been able to retrieve the file");
		}
	}

	@Destroy
	@Remove
	public void remove() {

	}
}
