package net.ihe.gazelle.xvalidation.action;

import java.io.Serializable;

import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.xvalidation.core.model.Assertion;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Name("assertionManager")
@Scope(ScopeType.STATELESS)
public class AssertionManager implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 978456827426359918L;

	public String getAssertionLink(Assertion assertion) {
		StringBuilder url = new StringBuilder();
		url.append(PreferenceService.getString("assertion_manager_url"));
		url.append("/assertions/show.seam?assertionId=");
		url.append(assertion.getAssertionId());
		url.append("&idScheme=");
		url.append(assertion.getIdScheme());
		return url.toString();
	}
}
