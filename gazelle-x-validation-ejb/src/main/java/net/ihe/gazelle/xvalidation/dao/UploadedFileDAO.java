package net.ihe.gazelle.xvalidation.dao;

import java.util.List;

import javax.persistence.EntityManager;

import net.ihe.gazelle.xvalidation.model.CrossValidationLog;
import net.ihe.gazelle.xvalidation.model.UploadedFile;
import net.ihe.gazelle.xvalidation.model.UploadedFileQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UploadedFileDAO extends DAO<UploadedFile> {

	private static Logger log = LoggerFactory.getLogger(UploadedFileDAO.class);

	public UploadedFileDAO() {
		super();
	}

	public UploadedFileDAO(EntityManager entityManager) {
		super(entityManager);
	}

	public static UploadedFileDAO instanceWithDefaultEntityManager() {
		return new UploadedFileDAO();
	}

	
	@Override
	public Class<UploadedFile> getEntityClass() {
		return UploadedFile.class;
	}

	public List<UploadedFile> getFilesForValidation(CrossValidationLog log){
		UploadedFileQuery query = new UploadedFileQuery();
		query.log().eq(log);
		return query.getList();
	}

	public void setFiles(UploadedFile file) {
		UploadedFileQuery query = new UploadedFileQuery();
		query.inputKeyword().eq(file.getInputKeyword());
		UploadedFile uploadedFile = query.getUniqueResult();
		if (uploadedFile != null) {
			uploadedFile.setFiles(file.getFiles());
		}
	}

	public List<UploadedFile> getOptionalFilesFromList(CrossValidationLog log) {
		UploadedFileQuery query = new UploadedFileQuery();
		query.log().eq(log);
		query.inputMin().eq(0);
		return query.getList();
	}
}
