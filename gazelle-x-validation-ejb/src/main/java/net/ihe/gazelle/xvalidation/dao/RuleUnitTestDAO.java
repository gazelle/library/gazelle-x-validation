package net.ihe.gazelle.xvalidation.dao;

import java.util.List;

import javax.persistence.EntityManager;

import net.ihe.gazelle.xvalidation.core.model.GazelleCrossValidatorType;
import net.ihe.gazelle.xvalidation.core.model.Rule;
import net.ihe.gazelle.xvalidation.core.model.RuleStatus;
import net.ihe.gazelle.xvalidation.model.RuleUnitTest;
import net.ihe.gazelle.xvalidation.model.RuleUnitTestQuery;

public class RuleUnitTestDAO extends DAO<RuleUnitTest>{
	
	public RuleUnitTestDAO() {
		super();
	}

	public RuleUnitTestDAO(EntityManager entityManager) {
		super(entityManager);
	}

	public static RuleUnitTestDAO instanceWithDefaultEntityManager() {
		return new RuleUnitTestDAO();
	}


	@Override
	public Class<RuleUnitTest> getEntityClass() {
		return RuleUnitTest.class;
	}

	public List<RuleUnitTest> getUnitTestsForRule(Rule inRule){
		RuleUnitTestQuery query = new RuleUnitTestQuery(entityManager);
		query.testedRule().id().eq(inRule.getId());
		//query.active().eq(true);
		return query.getList();
	}

	public RuleUnitTest getUnitTestByKeyword(String keyword) {
		RuleUnitTestQuery query = new RuleUnitTestQuery(entityManager);
		query.keyword().eq(keyword);
		return query.getUniqueResult();
	}

	public List<RuleUnitTest> getUnitTestsForValidator(GazelleCrossValidatorType validator) {
		RuleUnitTestQuery query = new RuleUnitTestQuery(entityManager);
		query.active().eq(true);
		query.testedRule().gazelleCrossValidator().id().eq(validator.getId());
		//TODO Check status restriction
		query.testedRule().status().eq(RuleStatus.ACTIVE);
		return query.getList();
	}
}
