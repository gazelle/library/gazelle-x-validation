package net.ihe.gazelle.xvalidation.rule.action;

import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.validator.ut.action.UnitTestBrowser;
import net.ihe.gazelle.xvalidation.model.RuleUnitTest;
import net.ihe.gazelle.xvalidation.model.RuleUnitTestQuery;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Name("ruleUnitTestBrowser")
@Scope(ScopeType.PAGE)
public class RuleUnitTestBrowser extends UnitTestBrowser<RuleUnitTest, RuleUnitTestQuery>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8560047077261653356L;

	@Override
	protected RuleUnitTestQuery createNewQuery() {
		return new RuleUnitTestQuery();
	}

	@Override
	protected void appendPath(HQLCriterionsForFilter<RuleUnitTest> criteria, RuleUnitTestQuery query) {
		criteria.addPath("rule", query.testedRule());
		criteria.addPath("validator", query.testedRule().gazelleCrossValidator());
	}
}
