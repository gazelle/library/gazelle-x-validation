/*
 * Copyright 2016 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package net.ihe.gazelle.xvalidation.validator.action;

import net.ihe.gazelle.xvalidation.core.model.GazelleCrossValidatorType;
import net.ihe.gazelle.xvalidation.core.model.RuleQuery;
import net.ihe.gazelle.xvalidation.core.model.RuleStatus;
import net.ihe.gazelle.xvalidation.model.RuleUnitTestQuery;
import net.ihe.gazelle.xvalidation.utils.AbstractPieChart;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.primefaces.model.chart.PieChartModel;

import java.util.List;

/**
 * <b>Class Description : </b>CrossValidatorStatistics<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 17/02/16
 * @class CrossValidatorStatistics
 * @package net.ihe.gazelle.xvalidation.validator.action
 * @see anne-gaelle.berge@ihe-europe.net - http://gazelle.ihe.net
 */

@Name("crossValidatorStatistics")
@Scope(ScopeType.STATELESS)
public class CrossValidatorStatistics extends AbstractPieChart {

    public PieChartModel getPieChartRuleStatus(GazelleCrossValidatorType validator) {
        PieChartModel pieChartModel = createPieChart("Status of rules", "s");
        if (validator != null) {
            RuleQuery query = new RuleQuery();
            query.gazelleCrossValidator().id().eq(validator.getId());
            List<Object[]> stats = query.status().getStatistics();
            for (Object[] stat : stats) {
                RuleStatus status = (RuleStatus) stat[0];
                pieChartModel.set(status.getValue(), (Number) stat[1]);
            }
        }
        return pieChartModel;
    }

    public PieChartModel getPieChartCoverage(GazelleCrossValidatorType validator) {
        PieChartModel pieChartModel = createPieChart("Rules linked to assertions", "s");
        if (validator != null) {
            pieChartModel.set("Linked", countRules(true, validator));
            pieChartModel.set("Not linked", countRules(false, validator));
        }
        return pieChartModel;
    }

    private Integer countRules(boolean linkedToAssertions, GazelleCrossValidatorType validator) {
        RuleQuery query = new RuleQuery();
        query.gazelleCrossValidator().id().eq(validator.getId());
        query.status().eq(RuleStatus.ACTIVE);
        if (linkedToAssertions) {
            query.assertions().isNotEmpty();
        } else {
            query.assertions().isEmpty();
        }
        return query.getCount();
    }

    public PieChartModel getPieChartUnitTest(GazelleCrossValidatorType validator) {
        PieChartModel pieChartModel = createPieChart("Status of unit tests", "s");
        if (validator != null) {
            RuleUnitTestQuery query = new RuleUnitTestQuery();
            query.testedRule().gazelleCrossValidator().id().eq(validator.getId());
            query.testedRule().status().eq(RuleStatus.ACTIVE);
            List<Object[]> stats = query.lastResult().getStatistics();
            formatUnitTestStat(pieChartModel, stats);
        }
        return pieChartModel;
    }

    public PieChartModel getCoverageByUnitTests(GazelleCrossValidatorType validatorType){
        PieChartModel pieChartModel = createPieChart("Coverage by unit tests", "s");
        if (validatorType != null){
            Integer covered = countCoveredRules(validatorType);
            pieChartModel.set("Covered", covered);
            Integer total = countActiveRules(validatorType);
            pieChartModel.set("Not covered", total - covered);
        }
        return pieChartModel;
    }

    private Integer countActiveRules(GazelleCrossValidatorType validator){
        RuleQuery query = new RuleQuery();
        query.gazelleCrossValidator().id().eq(validator.getId());
        query.status().eq(RuleStatus.ACTIVE);
        return query.getCount();
    }

    private Integer countCoveredRules(GazelleCrossValidatorType validatorType){
        RuleUnitTestQuery query = new RuleUnitTestQuery();
        query.testedRule().gazelleCrossValidator().id().eq(validatorType.getId());
        query.testedRule().status().eq(RuleStatus.ACTIVE);
        return query.testedRule().getCountDistinctOnPath();
    }
}
