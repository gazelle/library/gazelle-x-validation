package net.ihe.gazelle.xvalidation.validator.action;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.common.util.ZipUtility;
import net.ihe.gazelle.evsclient.interlay.gui.GuiMessage;
import net.ihe.gazelle.xvalidation.core.model.GazelleCrossValidatorType;
import net.ihe.gazelle.xvalidation.core.report.XSDValidation;
import net.ihe.gazelle.xvalidation.core.utils.GazelleCrossValidatorTransformer;
import net.ihe.gazelle.xvalidation.dao.DeprecatedValidatorVersionDAO;
import net.ihe.gazelle.xvalidation.dao.GazelleCrossValidatorTypeDAO;
import net.ihe.gazelle.xvalidation.menu.XValidationPages;
import net.ihe.gazelle.xvalidation.model.DeprecatedValidatorVersion;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.security.Identity;
import org.richfaces.event.FileUploadEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

@Stateful
@Scope(ScopeType.SESSION)
@Name("validatorImporter")
@GenerateInterface("ValidatorImporterLocal")
public class ValidatorImporter implements Serializable, ValidatorImporterLocal {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5837033300882941482L;

	private static Logger log = LoggerFactory.getLogger(ValidatorImporter.class);

	private GazelleCrossValidatorType validator;
	private GazelleCrossValidatorType oldValidator;
	private boolean update;
	private boolean archive = false;
	private boolean singleFile = true;
	private List<String> files;
	private int index;

	public String getXsdValidationErrors() {
		return xsdValidationErrors;
	}

	public void setXsdValidationErrors(String xsdValidationErrors) {
		this.xsdValidationErrors = xsdValidationErrors;
	}

	private String xsdValidationErrors;

	@Destroy
	@Remove
	public void remove() {

	}

	public void fileUploadListener(FileUploadEvent fileUploadEvent) throws IOException {
		files = null;
		xsdValidationErrors = null;
		File tmpFile = null;
		String filename = fileUploadEvent.getUploadedFile().getName();
		tmpFile = File.createTempFile("uploaded", null);
		FileOutputStream fos = null;
		try {
			if (tmpFile.exists()) {
				fos = new FileOutputStream(tmpFile);
				fos.write(fileUploadEvent.getUploadedFile().getData());
			}
		} catch (IOException e) {
			log.error(e.getMessage(), e);
			GuiMessage.logMessage(StatusMessage.Severity.ERROR, e.getMessage());
		} finally {
			if (fos != null) {
				fos.close();
			}
		}
		if (filename.endsWith(".xml")) {
			singleFile = true;
			processOneFile(tmpFile, filename);
		} else if (filename.endsWith(".zip")) {
			processZipFile(tmpFile, filename);
		} else {
			GuiMessage.logMessage(StatusMessage.Severity.ERROR, "Only XML and ZIP extensions are allowed");
		}
	}

	private void processZipFile(File tmpFile, String filename) {
		try {
			files = ZipUtility.getListDocumentPath(tmpFile, tmpFile.getParentFile());
			ZipUtility.unzip(tmpFile, tmpFile.getParentFile());
			tmpFile.delete();
			if (files == null || files.isEmpty()) {
				GuiMessage.logMessage(StatusMessage.Severity.ERROR, "No file found in the archive: " + filename);
				reset();
			} else {
				GuiMessage.logMessage(StatusMessage.Severity.INFO, "ZIP file contains " + files.size() + " files");
				index = 0;
				singleFile = (files.size() == 1);
				String path = files.get(0);
				processOneFile(new File(path), path);
			}
		} catch (IOException e) {
			GuiMessage.logMessage(StatusMessage.Severity.ERROR, e.getMessage());
			log.error(e.getMessage(), e);
		}

	}

	private void processOneFile(File tmpFile, String filename) {
		validator = null;
		try {
			log.info("Validating incoming file against schema");
			XSDValidation report = GazelleCrossValidatorType.validateXmlFile(tmpFile.getAbsolutePath());
			if (report.validationPassed()) {
				FileInputStream fis = new FileInputStream(tmpFile);
				validator = GazelleCrossValidatorTransformer.toObject(fis);
				oldValidator = GazelleCrossValidatorTypeDAO.instanceWithDefaultEntityManager()
						.getValidatorByUrlParameters(null, validator.getOid(), validator.getName(), validator.getAffinityDomain(), null);
				update = (oldValidator != null);
				fis.close();
				tmpFile.delete();
			} else {
				xsdValidationErrors = filename
						+ " is not a well-formed and/or valid XML file for the following reasons: <br/>";
				xsdValidationErrors = xsdValidationErrors + report.formatDetails();
				log.error(xsdValidationErrors);
			}
		} catch (JAXBException e) {
			xsdValidationErrors = e.getMessage();
			log.error(xsdValidationErrors, e);
		} catch (FileNotFoundException e) {
			xsdValidationErrors = e.getMessage();
			log.error(xsdValidationErrors, e);
		} catch (IOException e) {
			log.warn(e.getMessage(), e);
		}
		if (validator == null && !singleFile) {
			switchToNext();
		}
	}

	public GazelleCrossValidatorType getValidator() {
		return validator;
	}

	public void setValidator(GazelleCrossValidatorType validator) {
		this.validator = validator;
	}

	public String createValidator() {
		validator = GazelleCrossValidatorTypeDAO.instanceWithDefaultEntityManager().saveImportedEntity(validator);
		if (validator != null) {
			String redirectTo = XValidationPages.DOC_VALIDATOR.getSeamLink() + "?id=" + validator.getId();
			reset();
			return redirectTo;
		} else {
			return null;
		}

	}

	public String updateValidator() {
		if (oldValidator != null) {
			if (archive) {
				String filename = ValidatorIOManager.archiveValidator(oldValidator);
				DeprecatedValidatorVersion deprecated = new DeprecatedValidatorVersion(oldValidator, filename);
				deprecated.setComment("A new version (" + validator.getVersion() + ") has been imported by "
						+ Identity.instance().getCredentials().getUsername());
				DeprecatedValidatorVersionDAO.instanceWithDefaultEntityManager().saveEntity(deprecated);
			}
			GazelleCrossValidatorTypeDAO.instanceWithDefaultEntityManager().delete(oldValidator);
			return createValidator();
		} else {
			return null;
		}
	}

	public void createAndSwitchToNext() {
		createValidator();
		switchToNext();
	}

	public void updateAndSwitchToNext() {
		updateValidator();
		switchToNext();
	}

	private void switchToNext() {
		index++;
		if (index < files.size()) {
			String path = files.get(index);
			processOneFile(new File(path), path);
			singleFile = (index == files.size() - 1);
		}
	}

	public boolean isUpdate() {
		return update;
	}

	public GazelleCrossValidatorType getOldValidator() {
		return oldValidator;
	}

	public boolean isArchive() {
		return archive;
	}

	public void setArchive(boolean archive) {
		this.archive = archive;
	}

	public void reset() {
		validator = null;
		oldValidator = null;
		archive = false;
		update = false;
		if (!singleFile) {
			switchToNext();
		}
	}

	public boolean isSingleFile() {
		return singleFile;
	}

}
