package net.ihe.gazelle.xvalidation.dao;

import javax.persistence.EntityManager;

import net.ihe.gazelle.xvalidation.model.CrossValidationLog;
import net.ihe.gazelle.xvalidation.model.CrossValidationLogQuery;

import java.util.Date;

public class CrossValidationLogDAO extends DAO<CrossValidationLog> {

	public CrossValidationLogDAO() {
		super();
	}

	public CrossValidationLogDAO(EntityManager entityManager) {
		super(entityManager);
	}

	public static CrossValidationLogDAO instanceWithDefaultEntityManager() {
		return new CrossValidationLogDAO();
	}

	public void persistEntity(CrossValidationLog log) {
		entityManager.persist(log);
		entityManager.flush();
	}

	public void delete(CrossValidationLog validationLog) {
		if (validationLog.getId() != null) {
			CrossValidationLog todelete = find(validationLog.getId());
			entityManager.remove(todelete);
			entityManager.flush();
		}
	}

	@Override
	public Class<CrossValidationLog> getEntityClass() {
		return CrossValidationLog.class;
	}

	public CrossValidationLog getLogByOid(String oid) {
		CrossValidationLogQuery query = new CrossValidationLogQuery();
		query.oid().eq(oid);
		CrossValidationLog crossValidationLog = query.getUniqueResult();
		if (crossValidationLog != null) {
			return crossValidationLog;
		} else {
			return null;
		}
	}

	public String getStatusByOid(String oid) {
		CrossValidationLogQuery query = new CrossValidationLogQuery();
		query.oid().eq(oid);
		CrossValidationLog crossValidationLog = query.getUniqueResult();
		if (crossValidationLog != null) {
			return crossValidationLog.getResult().value();
		} else {
			return "NOT EXISTING";
		}
	}

	public Date getDateByOid(String oid) {
		CrossValidationLogQuery query = new CrossValidationLogQuery();
		query.oid().eq(oid);
		CrossValidationLog crossValidationLog = query.getUniqueResult();
		if (crossValidationLog != null) {
			return crossValidationLog.getTimestamp();
		} else {
			return null;
		}
	}

	public String getPermanentLinkByOid(String oid) {
		CrossValidationLogQuery query = new CrossValidationLogQuery();
		query.oid().eq(oid);
		CrossValidationLog crossValidationLog = query.getUniqueResult();
		if (crossValidationLog != null) {
			return crossValidationLog.getPermanentLink();
		} else {
			return "";
		}
	}



	public String getLastXValPermanentLinkFromExternalId(String externalId, String toolOid) {
		CrossValidationLogQuery query = new CrossValidationLogQuery();
		query.externalId().eq(externalId);
		query.callingToolOid().eq(toolOid);
		query.timestamp().isNotNull();
		// we might have several entries with the same externalID and we want to retrieve the latest
		query.timestamp().order(false);
		CrossValidationLog crossValidationLog = query.getUniqueResult();
		if (crossValidationLog != null) {
			return query.getUniqueResult().getPermanentLink();
		} else {
			return "";
		}
	}

	public String getLastXValStatusFromExternalId(String externalId, String toolOid) {
		CrossValidationLogQuery query = new CrossValidationLogQuery();
		query.externalId().eq(externalId);
		query.callingToolOid().eq(toolOid);
		query.timestamp().isNotNull();
		// we might have several entries with the same externalID and we want to retrieve the latest
		query.timestamp().order(false);
		CrossValidationLog crossValidationLog = query.getUniqueResult();
		if (crossValidationLog != null) {
			return query.getUniqueResult().getResult().value();
		} else {
			return "NOT EXISTING";
		}
	}

}
