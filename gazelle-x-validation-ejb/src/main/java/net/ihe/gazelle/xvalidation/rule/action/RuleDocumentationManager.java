package net.ihe.gazelle.xvalidation.rule.action;

import java.util.Map;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.xvalidation.core.model.Rule;
import net.ihe.gazelle.xvalidation.core.model.RuleQuery;
import net.ihe.gazelle.xvalidation.core.model.RuleStatus;
import net.ihe.gazelle.xvalidation.core.model.Status;
import net.ihe.gazelle.xvalidation.menu.XValidationPages;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Name("ruleDocumentationManager")
@Scope(ScopeType.PAGE)
public class RuleDocumentationManager extends AbstractRuleBrowser {

	/**
	 *
	 */
	private static final long serialVersionUID = -6587729915767857804L;

	@Override
	protected void addSpecificHQLCriterions(HQLCriterionsForFilter<Rule> criterions, RuleQuery query) {
		// no specific criterions in that case

	}

	@Override
	public void modifyQuery(HQLQueryBuilder<Rule> queryBuilder, Map<String, Object> filterValuesApplied) {
		queryBuilder.addEq("status", RuleStatus.ACTIVE);
		queryBuilder.addEq("gazelleCrossValidator.status", Status.AVAILABLE);
	}

	@Override
	public String listValidators() {
		return XValidationPages.DOC_HOME.getLink();
	}

	@Override
	public String listReferencedObjects() {
		return XValidationPages.DOC_REFERENCES.getLink();
	}

}
