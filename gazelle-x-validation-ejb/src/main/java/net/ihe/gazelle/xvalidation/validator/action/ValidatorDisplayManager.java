package net.ihe.gazelle.xvalidation.validator.action;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.xvalidation.core.model.Assertion;
import net.ihe.gazelle.xvalidation.core.model.GazelleCrossValidatorType;
import net.ihe.gazelle.xvalidation.core.model.Rule;
import net.ihe.gazelle.xvalidation.core.model.RuleQuery;
import net.ihe.gazelle.xvalidation.core.model.ValidatorInput;
import net.ihe.gazelle.xvalidation.core.model.ValidatorInputQuery;
import net.ihe.gazelle.xvalidation.dao.AssertionDAO;
import net.ihe.gazelle.xvalidation.dao.GazelleCrossValidatorTypeDAO;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Name("validatorDisplayManager")
@Scope(ScopeType.PAGE)
public class ValidatorDisplayManager implements Serializable, QueryModifier {

    /**
     *
     */
    private static final long serialVersionUID = -2675516042535721669L;

    private GazelleCrossValidatorType validator;

    private FilterDataModel<ValidatorInput> validatorInputs;
    private Filter<ValidatorInput> inputFilter;

    private FilterDataModel<Rule> activeRules;
    private Filter<Rule> ruleFilter;

    @Create
    public void getValidatorFromUrl() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        Integer id = null;
        String oid = null;
        if (params.containsKey("id")) {
            id = Integer.decode(params.get("id"));
        } else if (params.containsKey("oid")) {
            oid = params.get("oid");
        }
        validator = GazelleCrossValidatorTypeDAO.instanceWithDefaultEntityManager().getValidatorByUrlParameters(id, oid,
                params.get("name"), params.get("affinityDomain"), params.get("version"));
    }

    public GazelleCrossValidatorType getValidator() {
        return validator;
    }

    public void setValidator(GazelleCrossValidatorType validator) {
        this.validator = validator;
    }

    public List<Assertion> getAssertionsCoveredByRules() {
        return AssertionDAO.instanceWithDefaultEntityManager().getAssertionsCoveredByValidator(validator);
    }

    public FilterDataModel<ValidatorInput> getValidatorInputs() {
        return new FilterDataModel<ValidatorInput>(getInputFilter()) {
            @Override
            protected Object getId(ValidatorInput validatorInput) {
                return validatorInput.getId();
            }
        };
    }

    public Filter<ValidatorInput> getInputFilter() {
        if (inputFilter == null) {
            Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
            this.inputFilter = new Filter(this.getHqlCriterionsForInput(), params);
        }
        return this.inputFilter;
    }

    private HQLCriterionsForFilter getHqlCriterionsForInput() {
        ValidatorInputQuery query = new ValidatorInputQuery();
        HQLCriterionsForFilter criterion = query.getHQLCriterionsForFilter();
        criterion.addPath("objectType", query.referencedObject().objectType());
        criterion.addPath("keyword", query.referencedObject().keyword());
        criterion.addPath("description", query.referencedObject().description());
        criterion.addQueryModifier(this);
        return criterion;
    }

    public void resetInputFilter() {
        if (this.inputFilter != null) {
            this.inputFilter.clear();
        }
    }

    @Override
    public void modifyQuery(HQLQueryBuilder hqlQueryBuilder, Map map) {
        ValidatorInputQuery inputQuery = new ValidatorInputQuery();
        RuleQuery ruleQuery = new RuleQuery();
        if (validator != null) {
            hqlQueryBuilder.addRestriction(inputQuery.gazelleCrossValidator().eqRestriction(validator));
            hqlQueryBuilder.addRestriction(ruleQuery.gazelleCrossValidator().eqRestriction(validator));
        }
    }

    public FilterDataModel<Rule> getActiveRules() {
        return new FilterDataModel<Rule>(getRuleFilter()) {
            @Override
            protected Object getId(Rule rule) {
                return rule.getId();
            }
        };
    }

    public Filter<Rule> getRuleFilter() {
        if (ruleFilter == null) {
            Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
            this.ruleFilter = new Filter(this.getHqlCriterionsForRule(), params);
        }
        return this.ruleFilter;
    }

    private HQLCriterionsForFilter getHqlCriterionsForRule() {
        RuleQuery query = new RuleQuery();
        HQLCriterionsForFilter criterion = query.getHQLCriterionsForFilter();
        criterion.addPath("keyword", query.keyword());
        criterion.addPath("version", query.version());
        criterion.addQueryModifier(this);
        return criterion;
    }

    public void resetRuleFilter() {
        if (this.ruleFilter != null) {
            this.ruleFilter.clear();
        }
    }
}
