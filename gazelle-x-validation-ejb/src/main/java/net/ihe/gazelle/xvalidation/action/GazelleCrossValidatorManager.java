package net.ihe.gazelle.xvalidation.action;

import net.ihe.gazelle.evsclient.interlay.gui.GuiMessage;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.xvalidation.core.exception.GazelleXValidationException;
import net.ihe.gazelle.xvalidation.core.model.GazelleCrossValidatorType;
import net.ihe.gazelle.xvalidation.core.model.ValidatorInput;
import net.ihe.gazelle.xvalidation.core.utils.FileConverter;
import net.ihe.gazelle.xvalidation.dao.CrossValidationLogDAO;
import net.ihe.gazelle.xvalidation.dao.GazelleCrossValidatorTypeDAO;
import net.ihe.gazelle.xvalidation.dao.UploadedFileDAO;
import net.ihe.gazelle.xvalidation.dao.ValidatorInputDAO;
import net.ihe.gazelle.xvalidation.model.CrossValidationLog;
import net.ihe.gazelle.xvalidation.model.UploadedFile;
import net.ihe.gazelle.xvalidation.utils.ValidationEngineWithDB;
import org.apache.commons.io.IOUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.richfaces.event.FileUploadEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Name("gazelleCrossValidatorManager")
@Scope(ScopeType.PAGE)
public class GazelleCrossValidatorManager extends AbstractResultDisplay implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 393442715589691135L;
	private static Logger log = LoggerFactory.getLogger(GazelleCrossValidatorManager.class);
	private GazelleCrossValidatorType validator;
	private List<UploadedFile> files;
	private List<GazelleCrossValidatorType> availableValidators;
	private String affinityDomain;
	private transient File baseDir;
	private ValidatorInput selectedInput;
	private boolean displayResult = false;
	private UploadedFile currentFile;
	private boolean allInputsWithFiles = true;


	@Create
	public void init() {
		Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		affinityDomain = params.get("affinityDomain");
		availableValidators = GazelleCrossValidatorTypeDAO.instanceWithDefaultEntityManager()
				.getValidatorsAvailableForTesting(affinityDomain);
	}

	public GazelleCrossValidatorType getValidator() {
		return validator;
	}

	public void setValidator(GazelleCrossValidatorType inValidator) {
		this.validator = inValidator;
	}

	public void initializeValidator() {

		if (validator != null) {
			getValidationLog().setAffinityDomain(validator.getAffinityDomain());
			getValidationLog().setGazelleCrossValidator(validator.getName());
			setValidationLog(CrossValidationLogDAO.instanceWithDefaultEntityManager().saveEntity(getValidationLog()));
			createDirectory();
			List<ValidatorInput> inputs = getValidatorInputsForValidator();
			if ((inputs != null) && !inputs.isEmpty()) {
				files = new ArrayList<UploadedFile>();
				for (ValidatorInput input : inputs) {
					UploadedFile upload = new UploadedFile(input, baseDir.getAbsolutePath());
					upload.setLog(getValidationLog());
					files.add(UploadedFileDAO.instanceWithDefaultEntityManager().saveEntity(upload));
				}
			} else {
				log.error("no input defined");
			}
			testIfAllInputsWithFile();
		}
	}

	public void validatorSwitchListener(ValueChangeEvent event) {
		GazelleCrossValidatorType newVal = (GazelleCrossValidatorType) event.getNewValue();
		GazelleCrossValidatorType oldVal = (GazelleCrossValidatorType) event.getOldValue();
		reset();
		if (oldVal == null || !newVal.equals(oldVal)) {
			setValidator(newVal);
			setValidationLog(new CrossValidationLog());
			initializeValidator();
		}
	}

	public List<ValidatorInput> getValidatorInputsForValidator() {
		return ValidatorInputDAO.instanceWithDefaultEntityManager().getInputsForGazelleCrossValidator(validator);
	}

	public String validate() {
		if (allInputsWithFiles) {
			Map<String, Object> filesToValidate = new HashMap<String, Object>();
			for (UploadedFile file : files) {
				for (String filename : file.getFilesAbsolutePaths()) {
					// TODO handle multiple files by input
					try {
						filesToValidate
								.put(file.getInputKeyword(),
										FileConverter.file2Object(file.getInputObjectType(), filename,
												validator.getDicomLibrary()));
					} catch (GazelleXValidationException e) {
						GuiMessage.logMessage(StatusMessage.Severity.ERROR, e.getMessage());
						log.error(e.getMessage(), e);
					} catch (FileNotFoundException e) {
						GuiMessage.logMessage(StatusMessage.Severity.ERROR, "An error occurred when reading file " + file.getInputKeyword());
					}
				}
			}
			try {
				GazelleCrossValidatorType reloadValidator = GazelleCrossValidatorTypeDAO.instanceWithDefaultEntityManager()
						.getValidatorByUrlParameters(validator.getId(), null, null, null, null);
				ValidationEngineWithDB engine = new ValidationEngineWithDB(reloadValidator, filesToValidate);
				engine.validate();
				CrossValidationLogDAO dao = CrossValidationLogDAO.instanceWithDefaultEntityManager();
				setValidationLog(dao.find(getValidationLog().getId()));
				setDetailedResult(engine.getReportAsString());
				setValidationResult(engine.getStatus());
				dao.persistEntity(getValidationLog());
				CrossValidationLog.setOidFromRootOid(getValidationLog());
				getValidationLog().setValidatedFiles(files);
				dao.saveEntity(getValidationLog());
				displayResult = true;
				return getValidationLog().getPermanentLink();
			} catch (GazelleXValidationException e) {
				log.error(e.getMessage(), e);
				GuiMessage.logMessage(StatusMessage.Severity.ERROR, e.getMessage());
			} catch (IOException e) {
				log.error(e.getMessage(), e);
				GuiMessage.logMessage(StatusMessage.Severity.ERROR, e.getMessage());
			}
		} else {
			log.error("All inputs have not been filled");
			GuiMessage.logMessage(StatusMessage.Severity.ERROR, "All inputs have not been filled");
		}
		return null;
	}

	public void removeFile(String filename, UploadedFile uploadedFile) {
		uploadedFile.removeFile(filename);
		UploadedFileDAO.instanceWithDefaultEntityManager().saveEntity(uploadedFile);
		testIfAllInputsWithFile();
		if (displayResult) {
			setDisplayResult(false);
		}
	}

	private void createDirectory() {
		String baseDirectory = PreferenceService.getString("xvalidator_directory");
		StringBuilder dir = new StringBuilder(baseDirectory);
		dir.append('/');
		dir.append(UploadedFile.BASE_DIR);
		dir.append('/');
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		dir.append(sdf.format(new Date()));
		baseDir = new File(dir.toString());
		if (!baseDir.mkdirs()) {
			log.error("Unable to create " + baseDir.getAbsolutePath());
		}
	}

	public List<UploadedFile> getFiles() {
		files = UploadedFileDAO.instanceWithDefaultEntityManager().getFilesForValidation(getValidationLog());
		return files;
	}

	public List<GazelleCrossValidatorType> getAvailableValidators() {
		return availableValidators;
	}


	public String getAffinityDomain() {
		return affinityDomain;
	}

	@Destroy
	@Override
	public void reset() {
		log.info("reset method has been called");
		if (getValidationLog() != null && getValidationLog().getDetailedResult() == null) {
			deleteCurrentLog();
		}
		super.reset();
		validator = null;
		displayResult = false;
		files = null;
	}

	private void deleteCurrentLog(){
		if (files != null && !files.isEmpty()){
			for (UploadedFile upload: files){
				for (String path: upload.getFilesAbsolutePaths()){
					File file = new File(path);
					if (file.exists()){
						file.delete();
					}
				}
				File basedir = new File(upload.getDirectory());
				basedir.delete();
			}
		}
		CrossValidationLogDAO.instanceWithDefaultEntityManager().delete(getValidationLog());
	}

	public void fileUploadListener(FileUploadEvent event) throws IOException {
		String filename = event.getUploadedFile().getName();
		File tmpFile = File.createTempFile("uploaded", null);
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(tmpFile);
			fos.write(event.getUploadedFile().getData());
		} catch (Exception e){
			log.error("Error in fos gazelle cross manager : " + e.getMessage());
		} finally {
			if (fos != null) {
				fos.close();
			}
		}
		String newLocation = currentFile.getDirectory() + File.separatorChar + filename;
		saveFileUploaded(newLocation, tmpFile);
		currentFile.addFile(filename);
		UploadedFileDAO.instanceWithDefaultEntityManager().saveEntity(currentFile);
		//files.add(UploadedFileDAO.instanceWithDefaultEntityManager().saveEntity(currentFile));
		UploadedFileDAO.instanceWithDefaultEntityManager().setFiles(currentFile);
		testIfAllInputsWithFile();
	}

	public int getMaxInput() {
		if (this.currentFile == null) {
			return 0;
		}
		if (this.currentFile.getInputMax() < 0) {
			return 100000;
		}
		return this.currentFile.getInputMax();
	}

	public boolean isUploadNeeded() {
		if (this.currentFile == null) {
			return false;
		}
		if (this.currentFile.getInputMax() < 0) {
			return true;
		}
		return this.currentFile.getFiles().size() < this.currentFile.getInputMax();
	}

	public void saveFileUploaded(String newLocation, File tmpFile) throws IOException {
		InputStream inputStream = new FileInputStream(tmpFile);
		saveFileUploaded(newLocation, inputStream);
		tmpFile.delete();
	}

	public void saveFileUploaded(String newLocation, InputStream inputStream) {
		java.io.File fileToStore = new java.io.File(newLocation);
		fileToStore.getParentFile().mkdirs();
		FileOutputStream fos;
		try {
			if (fileToStore.exists()) {
				fileToStore.delete();
			}
			fos = new FileOutputStream(fileToStore);
			IOUtils.copy(inputStream, fos);
			inputStream.close();
			fos.close();
		} catch (FileNotFoundException e) {
			log.error(e.getMessage(), e);
			GuiMessage.logMessage(StatusMessage.Severity.ERROR, e.getMessage());
		} catch (IOException e) {
			log.error(e.getMessage(), e);
			GuiMessage.logMessage(StatusMessage.Severity.ERROR, e.getMessage());
		}
	}

	public void testIfAllInputsWithFile() {

		int nbFilesNeededByInputs = getValidator().getValidatedObjects().size();

		int nbFilesUploaded = 0;

		List<UploadedFile> optionalFilesList = UploadedFileDAO.instanceWithDefaultEntityManager().getOptionalFilesFromList(getValidationLog());

		for (int index = 0;index<nbFilesNeededByInputs;index++) {
			//test if there is an uploaded file for each input (different from null)
			//ValidatorInput input = validator.getValidatedObjects().get(index);
			List<String> fileContent = this.files.get(index).getFiles();
			if (fileContent.size() != 0) {
				nbFilesUploaded++;
			}
		}

		if (optionalFilesList != null) {
			for (UploadedFile uploadedFile : optionalFilesList) {
				if (uploadedFile.getFiles().size() == 0) {
					nbFilesNeededByInputs--;
				}
			}
		}

		if (nbFilesUploaded<nbFilesNeededByInputs) {
			setAllInputsWithFiles(false);
		} else {
			setAllInputsWithFiles(true);
		}
	}
	
	
	public ValidatorInput getSelectedInput() {
		return selectedInput;
	}

	public void setSelectedInput(ValidatorInput selectedInput) {
		this.selectedInput = selectedInput;
	}

	public boolean isDisplayResult() {
		return displayResult;
	}

	public void setDisplayResult(boolean displayResult) {
		this.displayResult = displayResult;
	}

	public UploadedFile getCurrentFile() {
		return currentFile;
	}

	public void setCurrentFile(UploadedFile currentFile) {
		this.currentFile = currentFile;
	}

	public boolean areAllInputsWithFiles() {
		return allInputsWithFiles;
	}

	public void setAllInputsWithFiles(boolean allInputsWithFiles) {
		this.allInputsWithFiles = allInputsWithFiles;
	}

}
