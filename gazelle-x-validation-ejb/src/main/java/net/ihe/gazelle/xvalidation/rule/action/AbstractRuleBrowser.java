package net.ihe.gazelle.xvalidation.rule.action;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.xvalidation.core.model.Rule;
import net.ihe.gazelle.xvalidation.core.model.RuleQuery;
import net.ihe.gazelle.xvalidation.dao.RuleDAO;

public abstract class AbstractRuleBrowser implements QueryModifier<Rule> {

	/**
	 *
	 */
	private static final long serialVersionUID = 3776851040218894588L;
	public static final String ALL = "All";

	private Filter<Rule> filter;

	private boolean emptyRuleList = true;

	public Filter<Rule> getFilter() {
		if (filter == null) {
			filter = new Filter<Rule>(buildHQLCriterions());
		}
		return filter;
	}

	public FilterDataModel<Rule> getRules() {
		return new FilterDataModel<Rule>(getFilter()){
                    @Override
        protected Object getId(Rule t) {
        // TODO Auto-generated method stub
        return t.getId();
        }
        };
	}

	private HQLCriterionsForFilter<Rule> buildHQLCriterions() {
		RuleQuery query = new RuleQuery();
		HQLCriterionsForFilter<Rule> criteria = query.getHQLCriterionsForFilter();
		criteria.addPath("validator", query.gazelleCrossValidator());
		criteria.addPath("assertion", query.assertions());
		criteria.addPath("level", query.level());
		addSpecificHQLCriterions(criteria, query);
		criteria.addQueryModifier(this);
		return criteria;
	}

	public void resetFilter() {
		getFilter().clear();
	}

	protected abstract void addSpecificHQLCriterions(HQLCriterionsForFilter<Rule> criterions, RuleQuery query);

	public abstract String listValidators();

	public abstract String listReferencedObjects();

	public boolean isEmptyRuleList() {
		if (emptyRuleList) {
			emptyRuleList = (RuleDAO.instanceWithDefaultEntityManager().countEntries() == 0);
		}
		return emptyRuleList;
	}
}
