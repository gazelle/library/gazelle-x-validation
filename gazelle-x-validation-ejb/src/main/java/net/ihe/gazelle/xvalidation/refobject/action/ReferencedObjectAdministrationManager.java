package net.ihe.gazelle.xvalidation.refobject.action;

import net.ihe.gazelle.xvalidation.core.model.ReferencedObject;
import net.ihe.gazelle.xvalidation.menu.XValidationPages;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Name("referencedObjectAdministrationManager")
@Scope(ScopeType.PAGE)
public class ReferencedObjectAdministrationManager extends AbstractReferencedObjectBrowser {

	/**
	 *
	 */
	private static final long serialVersionUID = 9151605337298899652L;
	
	private static Logger log = LoggerFactory.getLogger(ReferencedObjectAdministrationManager.class);

	@Override
	public String listValidators() {
		return XValidationPages.ADMIN_VALIDATORS.getLink();
	}

	@Override
	public String listRules() {
		return XValidationPages.ADMIN_RULES.getLink();
	}

	public String edit(ReferencedObject ref) {
		return XValidationPages.ADMIN_EDIT_REFOBJECT.getSeamLink() + "?keyword=" + ref.getKeyword();
	}
	
	public String duplicate(ReferencedObject ref){
		String link = edit(ref);
		log.info(link);
		return link + "&duplicate=true";
	}
}
