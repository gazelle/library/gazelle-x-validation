package net.ihe.gazelle.xvalidation.input.action;

import net.ihe.gazelle.evsclient.interlay.gui.GuiMessage;
import net.ihe.gazelle.xvalidation.core.model.GazelleCrossValidatorType;
import net.ihe.gazelle.xvalidation.core.model.Status;
import net.ihe.gazelle.xvalidation.core.model.ValidatorInput;
import net.ihe.gazelle.xvalidation.dao.DeprecatedValidatorVersionDAO;
import net.ihe.gazelle.xvalidation.dao.GazelleCrossValidatorTypeDAO;
import net.ihe.gazelle.xvalidation.menu.XValidationPages;
import net.ihe.gazelle.xvalidation.model.DeprecatedValidatorVersion;
import net.ihe.gazelle.xvalidation.validator.action.ValidatorIOManager;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

import java.io.Serializable;

@Name("validatorInputUpdateManager")
@Scope(ScopeType.PAGE)
public class ValidatorInputUpdateManager implements Serializable {

    private ValidatorInput input;
    private GazelleCrossValidatorType validator;
    private boolean putValidatorInMaintenance = false;

    private String oldVersion;
    private String newVersion;

    public String editInput(ValidatorInput input) {
        return XValidationPages.ADMIN_EDIT_INPUT.getSeamLink() + "?id=" + input.getId();
    }

    private String editValidator() {
        return XValidationPages.ADMIN_EDIT_VALIDATOR.getSeamLink() + "?id=" + validator.getId();
    }

    public String updateInput() {
        if (putValidatorInMaintenance) {
            putValidatorInMaintenanceMode();
        }
        return editInput(input);
    }

    public ValidatorInput getInput() {
        return input;
    }

    public void setInput(ValidatorInput input) {
        this.input = input;
        this.validator = input.getGazelleCrossValidator();
        this.putValidatorInMaintenance = validator.isAvailable();
        if (putValidatorInMaintenance) {
            if (oldVersion.contains(GazelleCrossValidatorType.DEV_SUFFIX) && !oldVersion.contains(GazelleCrossValidatorType.COPY_SUFFIX)) {
                String oldVersionModified = oldVersion.replace(GazelleCrossValidatorType.DEV_SUFFIX, "");
            }
//                newVersion = String.valueOf(Integer.decode(oldVersionModified) + 1);
//            } else if (oldVersion.contains(GazelleCrossValidatorType.COPYDEV_SUFFIX)) {
//                String oldVersionModified = oldVersion.replace(GazelleCrossValidatorType.COPYDEV_SUFFIX, "0");
//                newVersion = String.valueOf(Integer.decode(oldVersionModified) + 1);
//
//            } else {
//                newVersion = String.valueOf(Integer.decode(oldVersion) + 1);
//            }
        }
    }

    public GazelleCrossValidatorType getValidator() {
        return validator;
    }

    public boolean isPutValidatorInMaintenance() {
        return putValidatorInMaintenance;
    }

    public String getNewVersion() {
        return newVersion;
    }

    public void setNewVersion(String newVersion) {
        this.newVersion = newVersion;
    }

    public String getOldVersion() {
        return oldVersion;
    }

    public void setOldVersion(String oldVersion) {
        this.oldVersion = oldVersion;
    }

    private boolean putValidatorInMaintenanceMode() {
        String filename = ValidatorIOManager.archiveValidator(validator);
        if (filename != null) {
            DeprecatedValidatorVersion deprecated = new DeprecatedValidatorVersion(validator, filename);
            DeprecatedValidatorVersionDAO.instanceWithDefaultEntityManager().saveEntity(deprecated);
            oldVersion = validator.getVersion();
            validator.setVersion(oldVersion + GazelleCrossValidatorType.DEV_SUFFIX);
            validator.setStatus(Status.MAINTENANCE);
            GazelleCrossValidatorTypeDAO.instanceWithDefaultEntityManager().saveEntity(validator);
            return true;
        } else {
            GuiMessage.logMessage(StatusMessage.Severity.ERROR, "Back up of the current version did not work");
            GuiMessage.logMessage(StatusMessage.Severity.ERROR, "Back up of the current version did not work");
            return false;
        }
    }

    public void cancelUpdate() {
        input = null;
        validator = null;
    }

}
