/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.xvalidation.servlet;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;


import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class Upload extends HttpServlet {

    private static final long serialVersionUID = 1434693061336395803L;
    private static final Logger LOGGER = LoggerFactory.getLogger(Upload.class);

    private static long fileSize;//octets

    private static String key;

    public static long getFileSize() {
        return Upload.fileSize;
    }

    private void setFileSize(long fs) {
        fileSize = fs;
    }

    public static String getKey() {
        return key;
    }

    public static void setKey(String key) {
        Upload.key = key;
    }


    private static final Map<String, String> UPLOADED_FILES = Collections.synchronizedMap(new ConcurrentHashMap<String, String>());

    public static String putFile(final String filePath) {
        key = Integer.toHexString(filePath.hashCode());
        Upload.UPLOADED_FILES.put(key, filePath);
        return key;
    }

    public static String getFile(final String key) {
        String filePath = Upload.UPLOADED_FILES.get(key);
        deleteFile(key);
        return filePath;
    }

    public static Map<String, String> getMap() { return Upload.UPLOADED_FILES; }

    private static String deleteFile(final String key) { return Upload.UPLOADED_FILES.remove(key); }

    @Override
    protected void doPost(final HttpServletRequest req, final HttpServletResponse resp) throws IOException {
        String result = "";
        InputStream stream = null;
        try {
            Writer writer = resp.getWriter();
            if (ServletFileUpload.isMultipartContent(req)) {
                final ServletFileUpload upload = new ServletFileUpload();
                final FileItemIterator iter = upload.getItemIterator(req);
                while (iter.hasNext()) {
                    FileItemStream item = iter.next();
                    stream = item.openStream();
                    String messageType = item.getName();
                    String xvalType = item.getFieldName();
                    final File tmpFile = File.createTempFile("xval_" + xvalType + "_", '.' + messageType);
                    final OutputStream outputStream = new FileOutputStream(tmpFile);
                    Streams.copy(stream, outputStream, true);
                    result += Upload.putFile(tmpFile.getAbsolutePath()) + " ";
                    this.setFileSize(tmpFile.length());
                }
            }
            writer.write(result);
        } catch (final FileUploadException|IOException e) {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, e.getMessage());
        } finally {
            IOUtils.closeQuietly(stream);
        }
    }
}
