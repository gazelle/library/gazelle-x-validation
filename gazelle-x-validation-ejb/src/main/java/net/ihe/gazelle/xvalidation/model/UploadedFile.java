package net.ihe.gazelle.xvalidation.model;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import net.ihe.gazelle.xvalidation.core.model.Kind;
import net.ihe.gazelle.xvalidation.core.model.ValidatorInput;

import javax.persistence.ElementCollection;
import org.hibernate.annotations.IndexColumn;

@Entity
@Table(name = "xval_uploaded_file", schema = "public")
@SequenceGenerator(name = "xval_uploaded_file_sequence", sequenceName = "xval_uploaded_file_id_seq", allocationSize = 1)
public class UploadedFile implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1031522628575005730L;
	public static final String BASE_DIR = "validated_files";

	@Id
	@GeneratedValue(generator = "xval_uploaded_file_sequence", strategy = GenerationType.SEQUENCE)
	private Integer id;

	@Column(name = "keyword")
	private String inputKeyword;

	@Column(name = "input_description")
	private String inputDescription;

	@Column(name = "input_object_type")
	private Kind inputObjectType;

	@Column(name = "input_min")
	private Integer inputMin;

	@Column(name = "input_max")
	private Integer inputMax;

	@Transient
	private boolean complete;

	@Column(name = "directory")
	private String directory;

	@ElementCollection(targetClass = String.class)
	@CollectionTable(name = "xval_uploaded_file_files" , joinColumns = @JoinColumn(name = "xval_uploaded_file_id"))
	@Column(name = "element")
	private List<String> files;

	@ManyToOne(targetEntity = CrossValidationLog.class)
	@JoinTable(name = "xval_log_id")
	private CrossValidationLog log;

	public UploadedFile() {

	}

	public UploadedFile(ValidatorInput input, String directory) {
		if (input != null) {
			this.inputKeyword = input.getReferencedObject().getKeyword();
			this.inputDescription = input.getReferencedObject().getDescription();
			this.inputObjectType = input.getReferencedObject().getObjectType();
			this.inputMin = input.getMinQuantity();
			this.inputMax = input.getMaxQuantity();
		}
		this.complete = false;
		this.directory = directory;
	}

	public String getInputKeyword() {
		return inputKeyword;
	}

	public void setInputKeyword(String inputKeyword) {
		this.inputKeyword = inputKeyword;
	}

	public String getInputDescription() {
		return inputDescription;
	}

	public void setInputDescription(String inputDescription) {
		this.inputDescription = inputDescription;
	}

	public Kind getInputObjectType() {
		return inputObjectType;
	}

	public void setInputObjectType(Kind inputObjectType) {
		this.inputObjectType = inputObjectType;
	}

	public Integer getInputMin() {
		return inputMin;
	}

	public void setInputMin(Integer inputMin) {
		this.inputMin = inputMin;
	}

	public Integer getInputMax() {
		return inputMax;
	}

	public void setInputMax(Integer inputMax) {
		this.inputMax = inputMax;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<String> getFiles() {
		if (this.files == null) {
			this.files = new ArrayList<String>();
		}
		return files;
	}

	public void setFiles(List<String> fileLocation) {
		this.files = fileLocation;
	}

	public void addFile(String filename) {
		this.getFiles().add(filename);
		if (this.getFiles().size() >= inputMin) {
			this.complete = true;
		}
	}

	public void removeFile(String filename) {
		this.getFiles().remove(filename);
		if (this.getFiles().size() < inputMin) {
			this.complete = false;
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (prime * result) + ((inputKeyword == null) ? 0 : inputKeyword.hashCode());
		result = (prime * result) + ((log == null) ? 0 : log.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		UploadedFile other = (UploadedFile) obj;
		if (inputKeyword == null) {
			if (other.inputKeyword != null) {
				return false;
			}
		} else if (!inputKeyword.equals(other.inputKeyword)) {
			return false;
		}
		if (log == null) {
			if (other.log != null) {
				return false;
			}
		} else if (!log.equals(other.log)) {
			return false;
		}
		return true;
	}

	public void saveUploadedFile(File tmpFile, String filename, File baseDir) {
		if (tmpFile.exists() && baseDir.exists() && baseDir.isDirectory()) {
			File newFile = new File(baseDir, filename);
			tmpFile.renameTo(newFile);
			addFile(filename);
		}
	}

	public boolean isComplete() {
		return complete;
	}

	public void setComplete(boolean complete) {
		this.complete = complete;
	}

	public String getDirectory() {
		return directory;
	}

	public void setDirectory(String directory) {
		this.directory = directory;
	}

	public List<String> getFilesAbsolutePaths() {
		List<String> paths = new ArrayList<String>();
		for (String filename : files) {
			paths.add(directory + File.separatorChar + filename);
		}
		return paths;
	}

	public CrossValidationLog getLog() {
		return log;
	}

	public void setLog(CrossValidationLog log) {
		this.log = log;
	}
}
