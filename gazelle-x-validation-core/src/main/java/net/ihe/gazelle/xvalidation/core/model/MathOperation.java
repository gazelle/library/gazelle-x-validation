/*
 * Copyright 2016 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package net.ihe.gazelle.xvalidation.core.model;

import junit.framework.Assert;
import junit.framework.AssertionFailedError;
import net.ihe.gazelle.xvalidation.core.exception.GazelleXValidationException;
import net.ihe.gazelle.xvalidation.core.xpath.XpathUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.Map;

/**
 * <b>Class Description : </b>MathOperation<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 24/02/16
 * @class MathOperation
 * @package net.ihe.gazelle.xvalidation.core.model
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"member1", "operator", "member2"})
@XmlRootElement(name = "MathOperation")
@Entity
@DiscriminatorValue("mathOperation")
public class MathOperation extends MathMember {

    @XmlElements({@XmlElement(name = "MathOperationLeft", type = MathOperation.class),
            @XmlElement(name = "SingleMemberLeft", type = SingleMember.class),
            @XmlElement(name = "ConstantMemberLeft", type = ConstantMember.class)})
    @OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "member1_id")
    private MathMember member1;

    @XmlElements({@XmlElement(name = "MathOperationRight", type = MathOperation.class),
            @XmlElement(name = "SingleMemberRight", type = SingleMember.class),
            @XmlElement(name = "ConstantMemberRight", type = ConstantMember.class)})
    @OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "member2_id")
    private MathMember member2;

    @XmlElement(name = "MathOperator", required = true)
    @Column(name = "math_operator")
    private MathOperatorType operator;

    @XmlTransient
    @Transient
    private org.w3c.dom.Node xmlNodePresentation;

    public MathOperation() {
        super();
    }

    public MathOperation(MathOperatorType operator) {
        super();
        this.operator = operator;
    }

    public MathOperation(MathOperation operation) {
        super();
        this.operator = operation.getOperator();
        this.member1 = operation.getMember1().createCopy();
        this.member2 = operation.getMember2().createCopy();
    }

    public MathMember getMember1() {
        return member1;
    }

    public void setMember1(MathMember member1) {
        this.member1 = member1;
    }

    public MathMember getMember2() {
        return member2;
    }

    public void setMember2(MathMember member2) {
        this.member2 = member2;
    }

    public MathOperatorType getOperator() {
        return operator;
    }

    public void setOperator(MathOperatorType operator) {
        this.operator = operator;
    }

    @Override
    public Double evaluate(Map<String, Node> files, ValidatorConfiguration configuration, StringBuilder xpathReport) throws GazelleXValidationException {
        StringBuilder xpathReport1 = new StringBuilder();
        StringBuilder xpathReport2 = new StringBuilder();
        Double value1 = member1.evaluate(files, configuration, xpathReport1);
        Double value2 = member2.evaluate(files, configuration, xpathReport2);
        switch (operator) {
            case PLUS:
                xpathReport.append("( " + xpathReport1.toString() + " + " + xpathReport2.toString() + " )");
                return (value1 + value2);
            case MINUS:
                xpathReport.append("( " + xpathReport1.toString() + " - " + xpathReport2.toString() + " )");
                return (value1 - value2);
            case TIMES:
                xpathReport.append("( " + xpathReport1.toString() + " * " + xpathReport2.toString() + " )");
                return (value1 * value2);
            case DIVIDE:
                xpathReport.append("( " + xpathReport1.toString() + " / " + xpathReport2.toString() + " )");
                return (value1 / value2);
            case POWER:
                xpathReport.append("( " + xpathReport1.toString() + " ^ " + xpathReport2.toString() + " )");
                return Math.pow(value1, value2);
            default:
                return null;
        }
    }

    @Override
    public String getDisplayName() {
        return operator.getValue();
    }

    @Override
    public String getEditLink() {
        return "/xvalidation/expressions/mathOperation.xhtml";
    }

    @Override
    public void testCorrectness() throws AssertionFailedError {
        Assert.assertNotNull("[Expression #" + getFakeId() + "] first member is null", member1);
        member1.testCorrectness();
        Assert.assertNotNull("[Expression #" + getFakeId() + "] second member is null", member2);
        member2.testCorrectness();
    }

    @Override
    public ArrayList<String> appliesTo() throws AssertionFailedError {
        testCorrectness();
        ArrayList<String> keywords = new ArrayList<String>();
        keywords.addAll(member1.appliesTo());
        for (String key : member2.appliesTo()) {
            if (!keywords.contains(key)) {
                keywords.add(key);
            }
        }
        return keywords;
    }

    @Override
    public String toString() {
        if (operator == null) {
            return member1.toString();
        } else {
            StringBuilder builder = new StringBuilder();
            builder.append('(');
            builder.append(member1.toString());
            builder.append(") ");
            builder.append(operator.getFriendlyName());
            builder.append(" (");
            builder.append(member2.toString());
            builder.append(')');
            return builder.toString();
        }
    }

    @Override
    public MathMember createCopy() {
        return new MathOperation(this);
    }

    @Override
    public Node getXmlNodePresentation() {
        if (xmlNodePresentation == null) {
            xmlNodePresentation = ObjectFactory.getXmlNodePresentation("", "MathOperation", this);
        }
        return xmlNodePresentation;
    }

    @Override
    public void setXmlNodePresentation(Node xmlNodePresentation) {
        this.xmlNodePresentation = xmlNodePresentation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MathOperation)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        MathOperation that = (MathOperation) o;

        if (member1 != null ? !member1.equals(that.member1) : that.member1 != null) {
            return false;
        }
        if (member2 != null ? !member2.equals(that.member2) : that.member2 != null) {
            return false;
        }
        return operator == that.operator;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (member1 != null ? member1.hashCode() : 0);
        result = 31 * result + (member2 != null ? member2.hashCode() : 0);
        result = 31 * result + (operator != null ? operator.hashCode() : 0);
        return result;
    }
}
