package net.ihe.gazelle.xvalidation.core.report;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.w3c.dom.Node;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Aborted")
@XmlType(name = "Aborted", propOrder = {"reason" })
public class Aborted extends Notification {

	/**
	 *
	 */
	private static final long serialVersionUID = -7650088912920460697L;

	@XmlTransient
	private Node xmlNodePresentation;
	
	@XmlElement(name="reason")
	private String reason;

	@Override
	public Node getXmlNodePresentation() {
		if (xmlNodePresentation == null) {
			xmlNodePresentation = ObjectFactory.getXmlNodePresentation("", "Aborted", this);
		}
		return xmlNodePresentation;
	}

	@Override
	public void setXmlNodePresentation(Node xmlNodePresentation) {
		this.xmlNodePresentation = xmlNodePresentation;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

}
