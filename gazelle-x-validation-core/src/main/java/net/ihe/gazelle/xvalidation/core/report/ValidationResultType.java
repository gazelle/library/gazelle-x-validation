/**
 * ValidationResultType.java
 *
 * File generated from the XValidationReport1::ValidationResultType uml Enumeration
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.xvalidation.core.report;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import net.ihe.gazelle.hql.FilterLabel;

/**
 * Description of the enumeration ValidationResultType.
 *
 */

@XmlType(name = "ValidationResultType")
@XmlEnum
@XmlRootElement(name = "ValidationResultType")
public enum ValidationResultType {
	@XmlEnumValue("PASSED")
	PASSED("PASSED"),
	@XmlEnumValue("FAILED")
	FAILED("FAILED"),
	@XmlEnumValue("ABORTED")
	ABORTED("ABORTED"),
	@XmlEnumValue("DONE_UNDEFINED")
	DONE_UNDEFINED("DONE_UNDEFINED");

	private final String value;

	ValidationResultType(String v) {
		value = v;
	}

	@FilterLabel
	public String value() {
		return value;
	}

	public String getValue() {
		return value;
	}

	public static ValidationResultType fromValue(String v) {
		for (ValidationResultType c : ValidationResultType.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}

}