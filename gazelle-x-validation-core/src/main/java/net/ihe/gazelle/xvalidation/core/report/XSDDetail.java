package net.ihe.gazelle.xvalidation.core.report;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.w3c.dom.Node;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "XSDDetail", propOrder = { "message", "severity", "lineNumber", "columnNumber" })
@XmlRootElement(name = "XSDDetail")
public class XSDDetail implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7740838620854581166L;

	@XmlElement(name="Message")
	private String message;
	
	@XmlAttribute(name="severity")
	private String severity;
	
	@XmlAttribute(name="lineNumber")
	private Integer lineNumber;
	
	@XmlAttribute(name="columnNumber")
	private Integer columnNumber;
	
	@XmlTransient
	private Node xmlNodeRepresentation;

	public XSDDetail(){
		
	}
	
	public XSDDetail(String message){
		this.message = message;
	}
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getSeverity() {
		return severity;
	}

	public void setSeverity(String severity) {
		this.severity = severity;
	}

	public Integer getLineNumber() {
		return lineNumber;
	}

	public void setLineNumber(Integer lineNumber) {
		this.lineNumber = lineNumber;
	}

	public Integer getColumnNumber() {
		return columnNumber;
	}

	public void setColumnNumber(Integer columnNumber) {
		this.columnNumber = columnNumber;
	}

	public Node getXmlNodeRepresentation() {
		return xmlNodeRepresentation;
	}

	public void setXmlNodeRepresentation(Node xmlNodeRepresentation) {
		this.xmlNodeRepresentation = xmlNodeRepresentation;
	}
	
}
