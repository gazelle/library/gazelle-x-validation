package net.ihe.gazelle.xvalidation.core.exception;

import net.ihe.gazelle.xvalidation.core.model.Locator;

public class NoNodeSelectedException extends GazelleXValidationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3363349875803423994L;

	public NoNodeSelectedException(Locator locator){
		super(locator, "This XPath has not returned any element");
		setNotifyAdministrator(false);
	}
}
