package net.ihe.gazelle.xvalidation.core.utils;

import net.sf.saxon.dom.DOMNodeList;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>IgnoreWhitespaceNodeList class. Because DOM interprates \n and \t characters as text node, we need to create a copy of the NodeList which does not contain
 * this kind of nodes, in this way we can compare the nodes coming from two documents which are not indented the same way</p>
 *
 * @author abe
 * @version 1.0: 05/07/18
 */

public class IgnoreWhitespaceNodeList implements NodeList {

    private List<Node> nodes;

    public IgnoreWhitespaceNodeList(DOMNodeList list) {
        nodes = new ArrayList<Node>();
        for (int index = 0; index < list.getLength(); index++) {
            Node node = list.item(index);
            if (!isWhitespaceNode(node) && !isCommentNode(node)) {
                if (node.hasChildNodes()) {
                    removeWhitespacesAndComments(node);
                }
                nodes.add(node);
            }
        }
    }

    private void removeWhitespacesAndComments(Node node) {
        List<Node> toRemove = new ArrayList<Node>();
        for (Node child = node.getFirstChild(); child != null;
             child = child.getNextSibling()) {
            if (isWhitespaceNode(child) || isCommentNode(child)) {
                toRemove.add(child);
            } else if (child.hasChildNodes()) {
                removeWhitespacesAndComments(child);
            }
        }
        if (!toRemove.isEmpty()){
            for (Node entry: toRemove){
                node.removeChild(entry);
            }
        }
    }

    private static boolean isCommentNode(Node node) {
        return node.getNodeType() == Node.COMMENT_NODE;
    }

    /**
     * @param node
     *
     * @return true if trimmed content is an empty string or line feed, tab, or line break
     */
    private static boolean isWhitespaceNode(Node node) {
        if (node.getNodeType() == Node.TEXT_NODE) {
            String val = node.getNodeValue();
            if (val.trim().length() == 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    @Override
    public Node item(int index) {
        return nodes.get(index);
    }

    @Override
    public int getLength() {
        return nodes.size();
    }
}
