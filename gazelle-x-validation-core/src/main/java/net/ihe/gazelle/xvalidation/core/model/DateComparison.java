/*
 * Copyright 2016 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package net.ihe.gazelle.xvalidation.core.model;

import junit.framework.Assert;
import junit.framework.AssertionFailedError;
import net.ihe.gazelle.xvalidation.core.exception.GazelleXValidationException;
import net.ihe.gazelle.xvalidation.core.report.Notification;
import net.ihe.gazelle.xvalidation.core.xpath.XpathUtils;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DateComparison", propOrder = {"locator1", "dateFormats1", "locator2", "dateFormats2", "dateOperator"})
@XmlRootElement(name = "DateComparison")
@Entity
@DiscriminatorValue("dateComparison")
public class DateComparison extends AbstractExpression {

    private static Logger log = LoggerFactory.getLogger(BasicOperation.class);

    @XmlElement(name = "Locator1", required = true)
    @OneToOne(cascade = { CascadeType.ALL })
    @JoinColumn(name = "locator_1_id")
    private Locator locator1;

    @XmlElementWrapper(name = "DateFormats1", required = true)
    @XmlElementRefs({@XmlElementRef(name = "DateFormatType")})
    @ManyToMany(fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    @JoinTable(name = "xval_date1_format_type",
            joinColumns = @JoinColumn(name = "date_comparison_id"),
            inverseJoinColumns = @JoinColumn(name = "date_format_id"),
            uniqueConstraints = @UniqueConstraint(columnNames = { "date_comparison_id", "date_format_id" }))
    private List<DateFormatType> dateFormats1;

    @OneToOne(cascade = { CascadeType.ALL })
    @JoinColumn(name = "locator_2_id")
    @XmlElement(name = "Locator2", required = true)
    private Locator locator2;

    @XmlElementWrapper(name = "DateFormats2", required = true)
    @XmlElementRefs({@XmlElementRef(name = "DateFormatType")})
    @ManyToMany(fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    @JoinTable(name = "xval_date2_format_type",
            joinColumns = @JoinColumn(name = "date_comparison_id"),
            inverseJoinColumns = @JoinColumn(name = "date_format_id"),
            uniqueConstraints = @UniqueConstraint(columnNames = { "date_comparison_id", "date_format_id" }))
    private List<DateFormatType> dateFormats2;

    @XmlAttribute(name = "dateOperator", required = true)
    @Column(name = "date_operator")
    private DateOperatorType dateOperator;

    @XmlTransient
    @Transient
    private Node xmlNodePresentation;

    public DateComparison() {
        super();
    }

    public DateComparison(DateOperatorType operator) {
        super();
        this.dateOperator = operator;
        this.locator1 = new Locator();
        this.locator2 = new Locator();
    }

    public DateComparison(DateComparison operation){
        super();
        this.dateOperator = operation.getDateOperator();
        this.locator1 = new Locator(operation.getLocator1());
        this.locator2 = new Locator(operation.getLocator2());
    }

    public Locator getLocator1() {
        return locator1;
    }

    public void setLocator1(Locator locator1) {
        this.locator1 = locator1;
    }

    public List<DateFormatType> getDateFormats1() {
        return dateFormats1;
    }

    public void setDateFormats1(List<DateFormatType> dateFormats1) {
        if (dateFormats1 != null){
            this.dateFormats1 = new ArrayList<>(dateFormats1);
        } else {
            this.dateFormats1 = null;
        }
    }

    public Locator getLocator2() {
        return locator2;
    }

    public void setLocator2(Locator locator2) {
        this.locator2 = locator2;
    }

    public List<DateFormatType> getDateFormats2() {
        return dateFormats2;
    }

    public void setDateFormats2(List<DateFormatType> dateFormats2) {
        if (dateFormats2 != null){
            this.dateFormats2 = new ArrayList<>(dateFormats2);
        } else {
            this.dateFormats2 = null;
        }
    }

    public DateOperatorType getDateOperator() {
        return dateOperator;
    }

    public void setDateOperator(DateOperatorType dateOperator) {
        this.dateOperator = dateOperator;
    }

    @Override
    public Node getXmlNodePresentation() {
        if (xmlNodePresentation == null) {
            xmlNodePresentation = ObjectFactory.getXmlNodePresentation("", "DateComparison", this);
        }
        return xmlNodePresentation;
    }

    @Override
    public void setXmlNodePresentation(Node xmlNodePresentation) {
        this.xmlNodePresentation = xmlNodePresentation;
    }

    @Override
    public Notification evaluate(Map<String, Node> files, ValidatorConfiguration configuration, Level level){

        String stringDate1;
        String stringDate2;
        String xpathReport;
        try {
            stringDate1 = XpathUtils.getStringValue(files.get(locator1.getAppliesOn()), locator1, configuration.getNamespaceContext());
            stringDate2 = XpathUtils.getStringValue(files.get(locator2.getAppliesOn()), locator2, configuration.getNamespaceContext());
        } catch (GazelleXValidationException e){
            return abort("Error evaluating XPath : " + e.getMessage(), e.getLocator(), "Error evaluating XPath " + e.getLocator().toString() + " : " + e.getMessage());
        }

        DateFormatType format1 = getAppropriateFormat(dateFormats1, stringDate1);
        DateFormatType format2 = getAppropriateFormat(dateFormats2, stringDate2);
        if (format1 == null){
            return abort("Cannot match date from locator 1 to one of given date format.", locator1, "Cannot match date from locator 1 to one of given date format : " + XpathUtils.getXPathReport(locator1, stringDate1));
        }
        if (format2 == null){
            return abort("Cannot match date from locator 2 to one of given date format.", locator2, "Cannot match date from locator 2 to one of given date format : " + XpathUtils.getXPathReport(locator2, stringDate2));
        }

        long longDate1;
        long longDate2;
        try {
            if (format1.getPrecision() <= format2.getPrecision()) {
                longDate1 = new SimpleDateFormat(format1.getValue()).parse(stringDate1).getTime();
                String date2ToCompare = getStringDateToCompare(format2, format1, stringDate2);
                longDate2 = new SimpleDateFormat(format1.getValue()).parse(date2ToCompare).getTime();
            } else {
                String date1ToCompare = getStringDateToCompare(format1, format2, stringDate1);
                longDate1 = new SimpleDateFormat(format2.getValue()).parse(date1ToCompare).getTime();
                longDate2 = new SimpleDateFormat(format2.getValue()).parse(stringDate2).getTime();
            }
        } catch (ParseException e) {
            return abort("Error parsing dates with less restrictive format : " + e.getMessage(), null, "Error parsing dates with less restrictive format");
        }

        boolean result;
        switch (dateOperator) {
            case EQUAL:
                result = (longDate1 == longDate2);
                xpathReport = XpathUtils.getXPathReport(locator1, stringDate1) + "= " + XpathUtils.getXPathReport(locator2, stringDate2);
                break;
            case AFTER:
                result = (longDate1 > longDate2);
                xpathReport = XpathUtils.getXPathReport(locator1, stringDate1) + "> " + XpathUtils.getXPathReport(locator2, stringDate2);
                break;
            case BEFORE:
                result = (longDate1 < longDate2);
                xpathReport = XpathUtils.getXPathReport(locator1, stringDate1) + "< " + XpathUtils.getXPathReport(locator2, stringDate2);
                break;
            default:
                String message = this.dateOperator.getValue() + " is not a supported operation";
                log.error(message);
                return abort(message, null, "Unsupported operation : " + dateOperator.getValue());
        }

        Notification resultNotification;
        if (!result){
            resultNotification = newNotificationForLevel(level);
        } else {
            resultNotification = newNotificationForLevel(null);
        }
        resultNotification.setXpathReport(xpathReport);
        return resultNotification;
    }

    private DateFormatType getAppropriateFormat(List<DateFormatType> dateFormats, String dateAsString){
        for (DateFormatType dft : dateFormats){
            SimpleDateFormat sdf = new SimpleDateFormat(dft.getValue());
            try {
                String dateString = sdf.format(sdf.parse(dateAsString));
                if (dateAsString.length() == dateString.length()) {
                    return dft;
                }
            } catch (ParseException e) {
                if (log.isDebugEnabled()){
                    log.debug("Error parsing date " + dateAsString + " with format " + dft.getValue());
                }
            }
        }
        return null;
    }

    private String getStringDateToCompare(DateFormatType baseDateFormat, DateFormatType targetDateFormat, String dateAsString) throws ParseException{
        SimpleDateFormat baseSdf = new SimpleDateFormat(baseDateFormat.getValue());
        baseSdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        SimpleDateFormat targetSdf = new SimpleDateFormat(targetDateFormat.getValue());
        targetSdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        return targetSdf.format(baseSdf.parse(dateAsString));
    }

    @Override
    public String getEditLink() {
        return "/xvalidation/expressions/dateComparison.xhtml";
    }

    @Override
    public void testCorrectness() throws AssertionFailedError {
        Assert.assertNotNull("[Expression #" + getFakeId() + "] first member is null", locator1);
        Assert.assertTrue("[Expression #" + getFakeId() + "] first member is not correctly filled out",
                locator1.isValued());
        Assert.assertNotNull("[Expression #" + getFakeId() + "] second member is null", locator2);
        Assert.assertTrue("[Expression #" + getFakeId() + "] second member is not correctly filled out",
                locator2.isValued());
        Assert.assertNotNull("[Expression #" + getFakeId() + "] no operator defined" + dateOperator);
    }

    @Override
    public ArrayList<String> appliesTo() throws AssertionFailedError {
        testCorrectness();
        ArrayList<String> keywords = new ArrayList<String>();
        keywords.add(locator1.getAppliesOn());
        keywords.add(locator2.getAppliesOn());
        return keywords;
    }

    @Override
    public AbstractExpression createCopy() {
        return new DateComparison(this);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.locator1.toString());
        builder.append(this.dateOperator.getNotation());
        builder.append(this.locator2.toString());
        return builder.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        DateComparison other = (DateComparison) obj;
        if (dateOperator != other.dateOperator) {
            return false;
        }
        if (locator1 == null) {
            if (other.locator1 != null) {
                return false;
            }
        } else if (!locator1.equals(other.locator1)) {
            return false;
        }
        if (locator2 == null) {
            if (other.locator2 != null) {
                return false;
            }
        } else if (!locator2.equals(other.locator2)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = (prime * result) + ((dateOperator == null) ? 0 : dateOperator.hashCode());
        result = (prime * result) + ((locator1 == null) ? 0 : locator1.hashCode());
        result = (prime * result) + ((locator2 == null) ? 0 : locator2.hashCode());
        return result;
    }
}
