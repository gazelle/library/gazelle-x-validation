package net.ihe.gazelle.xvalidation.core.utils;

import net.ihe.gazelle.xvalidation.core.exception.GazelleXValidationException;
import net.ihe.gazelle.xvalidation.core.xpath.XpathUtils;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.jboss.resteasy.client.ClientExecutor;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;
import org.jboss.resteasy.client.core.executors.ApacheHttpClientExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SVSConsumer implements ValueSetProvider {

	private static Logger log = LoggerFactory.getLogger(SVSConsumer.class);

	private static Map<String, List<Concept>> listConcept = new HashMap<String, List<Concept>>();

	private static final ClientExecutor CLIENT_EXECUTOR = new ApacheHttpClientExecutor(new HttpClient(
			new MultiThreadedHttpConnectionManager()));
	private static final String SVS_METHOD = "/RetrieveValueSetForSimulator";
	private static final String ID = "id";
	private static final String CODE = "code";
	private static final String DISPLAY_NAME = "displayName";
	private static final String LANG = "lang";
	private static final String CODE_SYSTEM_NAME = "codeSystemName";
	private static final String CODE_SYSTEM = "codeSystem";
	private static final String CONCEPT = "Concept";
	public static final String DEFAULT_SVS = "https://gazelle.ihe.net";
	private static final int STATUS_OK = 200;

	/**
	 *
	 * @param valueSetId
	 * @return
	 */
	@Override
	public List<Concept> getConceptsListFromValueSet(String valueSetId, String lang) throws GazelleXValidationException{
		if (listConcept.get(valueSetId) != null) {
			return listConcept.get(valueSetId);
		}
		String svsRepository = getSVSRepositoryUrl();
		ClientRequest request = new ClientRequest(svsRepository.concat(SVS_METHOD), CLIENT_EXECUTOR);
		request.followRedirects(true);
		request.queryParameter(ID, valueSetId);
		if ((lang != null) && !lang.isEmpty()) {
			request.queryParameter(LANG, lang);
		}
		try {
			ClientResponse<String> response = request.get(String.class);
			if (response.getStatus() == STATUS_OK) {
				String xmlContent = response.getEntity();
				List<Concept> conceptsList = readConceptListFromResponse(xmlContent);
				if ((conceptsList != null) && !conceptsList.isEmpty()) {
					listConcept.put(valueSetId, conceptsList);
					return conceptsList;
				} else {
					return null;
				}
			} else {
				String message = "SVSSimulator returned code " + response.getStatus();
				log.error(message);
				throw new GazelleXValidationException(message);
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new GazelleXValidationException(e.getMessage(), e);
		}
	}

	protected String getSVSRepositoryUrl() {
		return DEFAULT_SVS;
	}

	private Concept readConceptFromNode(Node conceptNode) {
		NamedNodeMap attributes = conceptNode.getAttributes();
		Concept concept = new Concept();
		if (attributes.getNamedItem(CODE) != null) {
			concept.setCode(attributes.getNamedItem(CODE).getTextContent());
		}
		if (attributes.getNamedItem(DISPLAY_NAME) != null) {
			concept.setDisplayName(attributes.getNamedItem(DISPLAY_NAME).getTextContent());
		}
		if (attributes.getNamedItem(CODE_SYSTEM) != null) {
			concept.setCodeSystem(attributes.getNamedItem(CODE_SYSTEM).getTextContent());
		}
		if (attributes.getNamedItem(CODE_SYSTEM_NAME) != null) {
			concept.setCodeSystemName(attributes.getNamedItem(CODE_SYSTEM_NAME).getTextContent());
		}
		return concept;
	}

	private List<Concept> readConceptListFromResponse(String xmlContent) throws GazelleXValidationException {
		Document document;
		try {
			document = XpathUtils.parse(xmlContent);
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new GazelleXValidationException(e.getMessage(), e);
		}
		NodeList concepts = document.getElementsByTagName(CONCEPT);
		if (concepts.getLength() > 0) {
			List<Concept> conceptsList = new ArrayList<Concept>();
			for (int index = 0; index < concepts.getLength(); index++) {
				Node conceptNode = concepts.item(index);
				conceptsList.add(readConceptFromNode(conceptNode));
			}
			return conceptsList;
		} else {
			return null;
		}
	}
}
