/**
 * ReferencedObject.java
 *
 * File generated from the XValidator4::ReferencedObject uml Class
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.xvalidation.core.model;

// End of user code
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import net.ihe.gazelle.hql.FilterLabel;

import org.hibernate.annotations.Type;
import org.w3c.dom.Node;

/**
 * Description of the class ReferencedObject.
 *
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReferencedObject", propOrder = { "xsdLocation", "description", "keyword", "objectType" })
@XmlRootElement(name = "ReferencedObject")
@Entity
@Table(name = "xval_referenced_object", schema = "public")
@SequenceGenerator(name = "xval_referenced_object_sequence", sequenceName = "xval_referenced_object_id_seq", allocationSize = 1)
public class ReferencedObject implements java.io.Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@XmlTransient
	@Column(name = "id", unique = true, nullable = false)
	@GeneratedValue(generator = "xval_referenced_object_sequence", strategy = GenerationType.SEQUENCE)
	private Integer id;

	@XmlElement(name = "xsdLocation")
	private String xsdLocation;

	@XmlAttribute(name = "description", required = true)
	@Type(type = "text")
	@Lob
	private String description;

	@XmlAttribute(name = "keyword", required = true)
	@Column(name = "keyword", unique = true)
	private String keyword;

	@XmlAttribute(name = "objectType", required = true)
	@Column(name = "object_type")
	private Kind objectType;

	/**
	 * An attribute containing marshalled element node
	 */
	@XmlTransient
	@Transient
	private org.w3c.dom.Node xmlNodePresentation;
	
	public ReferencedObject(){
		super();
	}
	
	public ReferencedObject(ReferencedObject ref){
		this.keyword = ref.getKeyword() + "_COPY";
		this.objectType = ref.getObjectType();
		this.description = ref.getDescription();
		this.xsdLocation = ref.getXsdLocation();
	}

	/**
	 * Return xsdLocation.
	 * 
	 * @return xsdLocation
	 */
	public String getXsdLocation() {
		return xsdLocation;
	}

	/**
	 * Set a value to attribute xsdLocation.
	 * 
	 * @param xsdLocation
	 *            .
	 */
	public void setXsdLocation(String xsdLocation) {
		this.xsdLocation = xsdLocation;
	}

	/**
	 * Return description.
	 * 
	 * @return description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Set a value to attribute description.
	 * 
	 * @param description
	 *            .
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Return keyword.
	 * 
	 * @return keyword
	 */
	public String getKeyword() {
		return keyword;
	}

	/**
	 * Set a value to attribute keyword.
	 * 
	 * @param keyword
	 *            .
	 */
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	/**
	 * Return objectType.
	 * 
	 * @return objectType
	 */
	public Kind getObjectType() {
		return objectType;
	}

	/**
	 * Set a value to attribute objectType.
	 * 
	 * @param objectType
	 *            .
	 */
	public void setObjectType(Kind objectType) {
		this.objectType = objectType;
	}

	@FilterLabel
	public String getLabelForFilter() {
		return this.objectType.getValue() + " - " + this.keyword;
	}

	public Node getXmlNodePresentation() {
		if (xmlNodePresentation == null) {
			xmlNodePresentation = ObjectFactory.getXmlNodePresentation("", "ReferencedObject", this);
		}
		return xmlNodePresentation;
	}

	public void setXmlNodePresentation(Node xmlNodePresentation) {
		this.xmlNodePresentation = xmlNodePresentation;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (prime * result) + ((keyword == null) ? 0 : keyword.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ReferencedObject other = (ReferencedObject) obj;
		if (keyword == null) {
			if (other.keyword != null) {
				return false;
			}
		} else if (!keyword.equals(other.keyword)) {
			return false;
		}
		return true;
	}

	public boolean requiresXSDValidation() {
		return (xsdLocation != null && !xsdLocation.isEmpty());
	}

	public boolean isXml() {
		return (Kind.CDA.equals(objectType) || Kind.XML.equals(objectType));
	}
}