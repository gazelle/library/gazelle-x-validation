package net.ihe.gazelle.xvalidation.core.utils;

import java.io.*;
import java.util.ResourceBundle;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import net.ihe.gazelle.xvalidation.core.model.GazelleCrossValidatorType;

import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class GazelleCrossValidatorTransformer {

	private static Logger log = LoggerFactory.getLogger(GazelleCrossValidatorTransformer.class);

	private GazelleCrossValidatorTransformer() {

	}

	public static GazelleCrossValidatorType toObject(InputStream is) throws JAXBException {
		JAXBContext jc = JAXBContext.newInstance(GazelleCrossValidatorType.class);
		Unmarshaller u = jc.createUnmarshaller();
		return (GazelleCrossValidatorType) u.unmarshal(is);
	}

	public static void toXml(OutputStream out, GazelleCrossValidatorType xValidator) throws JAXBException {
		JAXBContext jc = JAXBContext.newInstance(GazelleCrossValidatorType.class);
		Marshaller m = jc.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		m.setProperty(Marshaller.JAXB_ENCODING, "UTF8");
		m.marshal(xValidator, out);
	}

	public static void main(String[] args) {
		try {
			FileInputStream fis = null;
			try {
				ResourceBundle bundle = ResourceBundle.getBundle("FilePath");
				String filePath = bundle.getString("filepathTransformer");
				fis = new FileInputStream(filePath);
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				GazelleCrossValidatorType xValidator = GazelleCrossValidatorTransformer.toObject(fis);
				GazelleCrossValidatorTransformer.toXml(baos, xValidator);
				log.info(baos.toString());
			} catch (FileNotFoundException e) {
				log.error(e.getMessage(), e);
			} catch (IOException e) {
				log.error(e.getMessage(), e);
			} finally {
				if (fis != null) {
					try {
						fis.close();
					} catch (IOException e) {
						log.error(e.getMessage(), e);
					}
				}
			}
		} catch (JAXBException e) {
			log.error(e.getMessage(), e);
		}
	}
}
