package net.ihe.gazelle.xvalidation.core.xpath;

import net.ihe.gazelle.evsclient.interlay.factory.XmlDocumentBuilderFactory;
import net.ihe.gazelle.xvalidation.core.exception.FileMissingException;
import net.ihe.gazelle.xvalidation.core.exception.GazelleXValidationException;
import net.ihe.gazelle.xvalidation.core.exception.NoNodeSelectedException;
import net.ihe.gazelle.xvalidation.core.model.Locator;
import net.sf.saxon.dom.DOMNodeList;
import net.sf.saxon.lib.NamespaceConstant;
import net.sf.saxon.s9api.*;
import net.sf.saxon.trans.XPathException;
import net.sf.saxon.xpath.XPathEvaluator;
import net.sf.saxon.xpath.XPathFactoryImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.namespace.NamespaceContext;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import javax.xml.xpath.XPathFactoryConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

public final class XpathUtils {

	private static final String UTF8 = "UTF-8";

	private XpathUtils() {

	}

	private static final XPathFactoryImpl FACTORY = (XPathFactoryImpl) buildFactory();
	private static final Logger LOG = LoggerFactory.getLogger(XpathUtils.class);


	public static XPathFactoryImpl getFactory() {
		return FACTORY;
	}


	private static XPathFactory buildFactory() {
		try {
			System.setProperty("javax.xml.xpath.XPathFactory:"
							+ NamespaceConstant.OBJECT_MODEL_SAXON,
					                           "net.sf.saxon.xpath.XPathFactoryImpl");
			return XPathFactory.newInstance(NamespaceConstant.OBJECT_MODEL_SAXON);

		} catch (XPathFactoryConfigurationException e) {
			LOG.error("Error in build Factory : " + e.getMessage());
			return null;
		}
	}

	public static Object evaluateXPath(Node node, Locator locator, NamespaceContext namespaceContext)
			throws GazelleXValidationException {
		if (node == null) {
			throw new FileMissingException(locator);
		} else {
			XPathEvaluator xpath = getXpathEvaluator(namespaceContext);
			if (xpath == null) {
                throw new NullPointerException();
            } else {
                try {
                    DOMNodeList resultAsNodeList = (DOMNodeList) xpath.evaluate(locator.getPath(), node,
                            XPathConstants.NODESET);
                    if (resultAsNodeList == null) {
                        throw new NoNodeSelectedException(locator);
                    } else {
                        return resultAsNodeList;
                    }
                } catch (XPathExpressionException e) {
                    if (e.getCause() instanceof XPathException) {
                        try {
                            String resultAsString = (String) xpath.evaluate(locator.getPath(), node, XPathConstants.STRING);

                            if (resultAsString == null) {
                                throw new NoNodeSelectedException(locator);
                            } else {
                                return resultAsString;
                            }
                        } catch (XPathExpressionException e1) {
                            throw new GazelleXValidationException(locator, e1);
                        }
                    } else {
                        throw new GazelleXValidationException(locator, e);
                    }
                }
            }
		}
	}

	public static DOMNodeList getNodesFromDOM(Node node, Locator locator, NamespaceContext namespaceContext)
			throws GazelleXValidationException {
		if (node == null) {
			throw new FileMissingException(locator);
		} else {
			XPathEvaluator xpath = getXpathEvaluator(namespaceContext);
            if (xpath == null) {
                throw new NullPointerException();
            } else {
                try {
                    DOMNodeList list = (DOMNodeList) xpath.evaluate(locator.getPath(), node, XPathConstants.NODESET);
                    if (list.getLength() == 0) {
                        throw new NoNodeSelectedException(locator);
                    } else {
                        return list;
                    }
                } catch (XPathExpressionException e) {
                    throw new GazelleXValidationException(locator, e);
                }
            }
		}
	}

	public static String getStringValue(Node node, Locator locator, NamespaceContext namespaceContext)
			throws GazelleXValidationException {
		if (node == null) {
			throw new FileMissingException(locator);
		} else {
			XPathEvaluator xpath = getXpathEvaluator(namespaceContext);
            if (xpath == null) {
                throw new NullPointerException();
            } else {
                try {
                    String result = (String) xpath.evaluate(locator.getPath(), node, XPathConstants.STRING);
                    if (result.isEmpty()) {
                        throw new NoNodeSelectedException(locator);
                    } else {
                        return result;
                    }
                } catch (XPathExpressionException e) {
                    throw new GazelleXValidationException(locator, e);
                }
            }
		}
	}

    public static Double getDoubleValue(Node node, Locator locator, NamespaceContext namespaceContext)
            throws GazelleXValidationException {
        if (node == null) {
            throw new FileMissingException(locator);
        } else {
            XPathEvaluator xpath = getXpathEvaluator(namespaceContext);
            if (xpath == null) {
                throw new NullPointerException();
            } else {
                try {
                    Double result = (Double) xpath.evaluate(locator.getPath(), node, XPathConstants.NUMBER);
                    if (result == null || result.isNaN()) {
                        throw new NoNodeSelectedException(locator);
                    } else {
                        return result;
                    }
                } catch (XPathExpressionException e) {
                    throw new GazelleXValidationException(locator, e);
                }
            }
        }
    }

	public static Boolean getBooleanValue(Node node, Locator locator, NamespaceContext namespaceContext)
			throws GazelleXValidationException {
		if (node == null) {
			throw new FileMissingException(locator);
		} else {
			XPathEvaluator xpath = getXpathEvaluator(namespaceContext);
            if (xpath == null) {
                throw new NullPointerException();
            } else {
                try {
                    return (Boolean) xpath.evaluate(locator.getPath(), node, XPathConstants.BOOLEAN);
                } catch (XPathExpressionException e) {
                    throw new GazelleXValidationException(locator, e);
                }
            }
		}
	}

	public static Document parse(InputStream document) throws GazelleXValidationException {
		try {
			return new XmlDocumentBuilderFactory()
					.setIgnoringComments(true)
					.setNamespaceAware(true)
					.getBuilder()
					.parse(document);
		} catch (Exception e) {
			throw new GazelleXValidationException(e.getMessage(), e);
		}

	}

	public static Document parse(String xmlContent) throws GazelleXValidationException {
		if (xmlContent != null) {
			ByteArrayInputStream bais = new ByteArrayInputStream(xmlContent.getBytes(Charset.forName(UTF8)));
			return parse(bais);
		} else {
			return null;
		}
	}

	private static XPathEvaluator getXpathEvaluator(NamespaceContext nsContext) {
		XPathEvaluator xpath = null;
		synchronized (FACTORY) {
            xpath = (XPathEvaluator) FACTORY.newXPath();
            xpath.setNamespaceContext(nsContext);
		}
		return xpath;
	}

	public static Integer getObjectLength(Object object) {
		if (object instanceof String) {
			return 1;
		} else if (object instanceof Boolean) {
			return 0;
		} else if (object instanceof DOMNodeList) {
			return ((DOMNodeList) object).getLength();
		} else {
			return null;
		}
	}

	public static String getXPathReport(Locator xpath, Object xpathEvaluationResult){
		StringBuilder xpathReport = new StringBuilder("[");
		xpathReport.append(xpath.toString());
		if (xpathEvaluationResult == null){
			xpathReport.append(" matched nothing");
		} else if (xpathEvaluationResult instanceof String){
			xpathReport.append(" matched String with value \"" + xpathEvaluationResult + "\"");
		} else if (xpathEvaluationResult instanceof DOMNodeList){
			xpathReport.append(" matched DOMNodeList of size " + ((DOMNodeList) xpathEvaluationResult).getLength());
			if (((DOMNodeList) xpathEvaluationResult).getLength() != 0){
				xpathReport.append(" containing following nodes : ");
				for (int i = 0; i<((DOMNodeList) xpathEvaluationResult).getLength(); i++){
					Node node = ((DOMNodeList) xpathEvaluationResult).item(i);
					xpathReport.append("\n - Node of type " + node.getNodeType() + " with name " + getNodeDisplayName(node) + " from path " + getXPath(node));
					if (node.getNodeValue() != null){
						xpathReport.append(" with value : " + node.getNodeValue());
					}
				}
			}
		} else if (xpathEvaluationResult instanceof Boolean){
			xpathReport.append(" matched boolean with value " + xpathEvaluationResult.toString());
		} else if (xpathEvaluationResult instanceof Double) {
			xpathReport.append(" matched Double with value " + xpathEvaluationResult.toString());
		} else {
			xpathReport.append(" matched an unknown element of class : " + xpathEvaluationResult.getClass());
		}
		xpathReport.append("]\n");
		return xpathReport.toString();
	}

	private static String getNodeDisplayName(Node node) {
		if (node.getPrefix()!=null && !node.getNodeName().contains(":")){
			return node.getPrefix() + ":" + node.getNodeName();
		}
		return node.getNodeName();
	}
	private static String getXPath(Node node) {
		Node parent = node.getParentNode();
		StringBuilder path = new StringBuilder();
		if (parent == null) {
			path.append("/");
			path.append(getNodeDisplayName(node));
		} else {
			path.append(getXPath(parent) + "/" + getNodeDisplayName(node));
			if (isArrayNode(node)){
				path.append("[" + getIndexOfArrayNode(node) + "]");
			}
		}
		return path.toString();
	}

	private static boolean isArrayNode(Node node) {
		if (node.getNextSibling() == null && node.getPreviousSibling() == null) {
			return false;
		} else {
			Node currentNode = node.getPreviousSibling();
			while(currentNode!=null){
				if (getNodeDisplayName(currentNode).equals(getNodeDisplayName(node))){
					return true;
				}
				currentNode = currentNode.getPreviousSibling();
			}
			currentNode = node.getNextSibling();
			while(currentNode!=null){
				if (getNodeDisplayName(currentNode).equals(getNodeDisplayName(node))){
					return true;
				}
				currentNode = currentNode.getNextSibling();
			}
			return false;
		}
	}

	private static Integer getIndexOfArrayNode(Node node) {
		if(isArrayNode(node)) {
			int leftCount = 0;
			Node currentNode = node.getPreviousSibling();
			while(currentNode != null) {
				if (getNodeDisplayName(currentNode).equals(getNodeDisplayName(node))){
					leftCount++;
				}
				currentNode = currentNode.getPreviousSibling();
			}
			return leftCount;
		} else {
			return null;
		}
	}
}
