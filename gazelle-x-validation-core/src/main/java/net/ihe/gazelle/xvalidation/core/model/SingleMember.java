/*
 * Copyright 2016 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package net.ihe.gazelle.xvalidation.core.model;

import junit.framework.Assert;
import junit.framework.AssertionFailedError;
import net.ihe.gazelle.xvalidation.core.exception.GazelleXValidationException;
import net.ihe.gazelle.xvalidation.core.xpath.XpathUtils;
import org.w3c.dom.Node;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.Map;

/**
 * <b>Class Description : </b>SingleMember<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 25/02/16
 * @class SingleMember
 * @package net.ihe.gazelle.xvalidation.core.model
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"locator"})
@XmlRootElement(name = "SingleMember")
@Entity
@DiscriminatorValue("singleMember")
public class SingleMember extends MathMember {

    @XmlElement(name = "locator")
    @OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "locator_id")
    private Locator locator;

    public SingleMember(){
        super();
        locator = new Locator();
    }

    public SingleMember(SingleMember member){
        super();
        this.locator = new Locator(member.getLocator());
    }

    public SingleMember(Locator inLocator){
        super();
        this.locator = inLocator;
    }

    public SingleMember(String appliesTo, String xpath){
        super();
        this.locator = new Locator(appliesTo, xpath);
    }

    @XmlTransient
    @Transient
    private org.w3c.dom.Node xmlNodePresentation;

    public Locator getLocator() {
        return locator;
    }

    public void setLocator(Locator locator) {
        this.locator = locator;
    }

    public Node getXmlNodePresentation() {
        if (xmlNodePresentation == null) {
            xmlNodePresentation = ObjectFactory.getXmlNodePresentation("", "SingleMember", this);
        }
        return xmlNodePresentation;
    }

    public void setXmlNodePresentation(Node xmlNodePresentation) {
        this.xmlNodePresentation = xmlNodePresentation;
    }

    @Override
    public MathMember createCopy() {
        return new SingleMember(this);
    }

    @Override
    public Double evaluate(Map<String, Node> files, ValidatorConfiguration configuration, StringBuilder xpathReport) throws GazelleXValidationException {
        Double value = XpathUtils.getDoubleValue(files.get(locator.getAppliesOn()), locator, configuration.getNamespaceContext());
        if (value == null){
            throw new GazelleXValidationException(locator, "XPath did not select anything");
        } else {
            xpathReport.append(XpathUtils.getXPathReport(locator, value));
            return value;
        }
    }

    @Override
    public String getDisplayName() {
        return "single";
    }

    @Override
    public String getEditLink() {
        return "/xvalidation/expressions/locator.xhtml";
    }

    @Override
    public void testCorrectness() throws AssertionFailedError {
        Assert.assertTrue("[Member #" + getFakeId() + "] is null", locator.isValued());
    }

    @Override
    public String toString() {
        return locator.toString();
    }

    @Override
    public ArrayList<String> appliesTo() throws AssertionFailedError {
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SingleMember)) return false;

        SingleMember that = (SingleMember) o;

        return locator != null ? locator.equals(that.locator) : that.locator == null;

    }

    @Override
    public int hashCode() {
        return locator != null ? locator.hashCode() : 0;
    }
}
