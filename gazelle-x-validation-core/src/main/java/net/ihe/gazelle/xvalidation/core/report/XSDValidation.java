package net.ihe.gazelle.xvalidation.core.report;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.slf4j.Logger;
import org.w3c.dom.Node;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "XSDValidation", propOrder = { "keyword", "xsdLocation", "file", "errors", "warnings", "details",
		"result" })
@XmlRootElement(name = "XSDValidation")
public class XSDValidation implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1096036621860182076L;

	@XmlAttribute(name = "keyword", required = true)
	private String keyword;

	@XmlAttribute(name = "xsdLocation", required = true)
	private String xsdLocation;

	@XmlAttribute(name = "file", required = false)
	private String file;

	@XmlAttribute(name = "errors")
	private Integer errors;

	@XmlAttribute(name = "warnings")
	private Integer warnings;

	@XmlElement(name = "Detail", required = false)
	private List<XSDDetail> details;

	@XmlElement(name = "Result")
	private ValidationResultType result;

	@XmlTransient
	private Node xmlNodePresentation;

	public XSDValidation() {
		this.errors = 0;
		this.warnings = 0;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getXsdLocation() {
		return xsdLocation;
	}

	public void setXsdLocation(String xsdLocation) {
		this.xsdLocation = xsdLocation;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public Integer getErrors() {
		if (errors == null) {
			errors = 0;
		}
		return errors;
	}

	public void setErrors(Integer errors) {
		this.errors = errors;
	}

	public Integer getWarnings() {
		if (warnings == null) {
			warnings = 0;
		}
		return warnings;
	}

	public void setWarnings(Integer warnings) {
		this.warnings = warnings;
	}

	public List<XSDDetail> getDetails() {
		if (details == null) {
			details = new ArrayList<XSDDetail>();
		}
		return details;
	}

	public void setDetails(List<XSDDetail> messages) {
		this.details = messages;
	}

	public void addDetail(XSDDetail message) {
		this.getDetails().add(message);
	}

	public ValidationResultType getResult() {
		return result;
	}

	public void setResult(ValidationResultType result) {
		this.result = result;
	}

	public Node getXmlNodePresentation() {
		if (xmlNodePresentation == null) {
			this.xmlNodePresentation = ObjectFactory.getXmlNodePresentation("", "XSDValidation", this);
		}
		return xmlNodePresentation;
	}

	public void setXmlNodePresentation(Node xmlNodePresentation) {
		this.xmlNodePresentation = xmlNodePresentation;
	}

	public boolean validationPassed() {
		return ValidationResultType.PASSED.equals(result);
	}

	public String formatDetails() {
		StringBuilder message = new StringBuilder();
		for (XSDDetail detail : details) {
			message.append(detail.getSeverity());
			message.append(": ");
			message.append(detail.getMessage());
			message.append("[col: ");
			message.append(detail.getColumnNumber());
			message.append(", line: ");
			message.append(detail.getLineNumber());
			message.append(']');
			message.append('\n');
		}
		return message.toString();
	}

	public void print(Logger log) {
		if (!validationPassed()) {
			log.error(formatDetails());
		}
	}
}
