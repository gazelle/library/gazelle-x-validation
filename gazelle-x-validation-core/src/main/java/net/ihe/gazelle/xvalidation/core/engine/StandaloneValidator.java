package net.ihe.gazelle.xvalidation.core.engine;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import javax.xml.bind.JAXBException;

import net.ihe.gazelle.xvalidation.core.exception.GazelleXValidationException;
import net.ihe.gazelle.xvalidation.core.model.DICOMLibrary;
import net.ihe.gazelle.xvalidation.core.model.GazelleCrossValidatorType;
import net.ihe.gazelle.xvalidation.core.model.ValidatorInput;
import net.ihe.gazelle.xvalidation.core.report.XSDValidation;
import net.ihe.gazelle.xvalidation.core.utils.FileConverter;
import net.ihe.gazelle.xvalidation.core.utils.GazelleCrossValidatorTransformer;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class StandaloneValidator {

	private static Logger log = LoggerFactory.getLogger(StandaloneValidator.class);

	// -validator declares the path to the XML file which represents the validator
	private static final String OPT_VALIDATOR = "validator";
	// -output declares the file path for saving the XML report
	private static final String OPT_OUTPUT = "output";
	// -help print the help
	private static final String OPT_HELP = "help";
	// -file keyword=path,keyword2=path2
	private static final String OPT_FILES = "files";

	private static boolean helpPrinted = false;
	private static Map<String, String> options;

	private StandaloneValidator() {
		options = new HashMap<String, String>();
	}

	public static void main(String args[]) {
		StandaloneValidator standaloneValidator = new StandaloneValidator();
		Options optionsFromCommandLine = standaloneValidator.getOptions();
		boolean requiredParametersPresent = false;
		// Get parameters from the command line
		try {
			requiredParametersPresent = standaloneValidator.getParameters(args, optionsFromCommandLine);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			standaloneValidator.printHelp(optionsFromCommandLine, e);
		}
		// When parameters are OK
		if (requiredParametersPresent && options.containsKey(OPT_VALIDATOR) && (options.get(OPT_VALIDATOR) != null)) {
			try {
				String validatorLocation = options.get(OPT_VALIDATOR);
				if (validateValidatorFile(validatorLocation)) {
					log.info("... OK");
					FileInputStream fis = new FileInputStream(options.get(OPT_VALIDATOR));
					GazelleCrossValidatorType validator = GazelleCrossValidatorTransformer.toObject(fis);
					fis.close();
					Scanner scanner = new Scanner(System.in);
					Map<String, Object> files = processFilesToValidate(scanner, validator.getValidatedObjects(), validator.getConfiguration().getDicomLibrary());
					String reportLocation = options.get(OPT_OUTPUT);
					validate(reportLocation, validator, files, scanner);
				} else {
					log.info(validatorLocation + " is not well-formed or valid, see details above. Please fix it before trying again");
//					System.exit(0);
				}
			} catch (FileNotFoundException e) {
				log.error(e.getMessage(), e);
			} catch (JAXBException e) {
				log.error(options.get(OPT_VALIDATOR) + " is not formatted as a GazelleCrossValidator", e);
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			}
		} else {
			log.info("exiting");
		}
	}

	private static void validate(String reportPath, GazelleCrossValidatorType validator, Map<String, Object> files,
			Scanner scanner) {
		ValidationEngine engine = null;
		try {
			engine = new ValidationEngine(validator, files);
			engine.validate();
		} catch (GazelleXValidationException e) {
			log.error(e.getMessage(), e);
//			System.exit(0);
		} catch (IOException e) {
			log.error(e.getMessage(), e);
//			System.exit(0);
		}
		if (engine != null) {
			if (reportPath != null) {
				boolean saved = false;
				do {
					File report = new File(reportPath);
					if (report.isFile() && report.exists() && (report.length() > 0)) {
						log.warn(reportPath + " already exists, override ? [Y/n] :");
						String override = scanner.next();
						if (override.equalsIgnoreCase("n")) {
							log.info("New path for the report: ");
							reportPath = scanner.next();
						} else {
							engine.saveReport(reportPath);
							saved = true;
						}
					} else {
						engine.saveReport(reportPath);
						saved = true;
					}
				} while (!saved);
				log.info("The report has been saved at " + reportPath);
			} else {
				log.info(engine.getReportAsString());
			}
		}
	}

	private static Map<String, Object> processFilesToValidate(Scanner scanner, List<ValidatorInput> expectedInputs, DICOMLibrary dicomLibrary) {
		Map<String, Object> files = new HashMap<String, Object>();
		for (ValidatorInput input : expectedInputs) {
			// TODO : how to handle minQuantity/maxQuantity
			String path = options.get(input.getReferencedObject().getKeyword());
			if ((path == null) || path.isEmpty()) {
				log.info("Enter the path to this file : '" + input.getReferencedObject().getDescription() + "': ");
				path = scanner.next();
			}
			// TODO check the file extension
			File currentFile = new File(path);
			if (!currentFile.exists() || !currentFile.isFile()) {
				log.error(path + " does not reference an existing file");
//				System.exit(0);
			} else {
				Object value = null;
				switch (input.getReferencedObject().getObjectType()) {
				case DICOM:
					value = FileConverter.dumpDICOMObject(path, dicomLibrary);
					if (value == null) {
						log.error(path + " does not reference a valid DICOM object");
//						System.exit(0);
					}
					break;
				case HL7V2:
					value = FileConverter.dumpER7Message(path);
					if (value == null) {
						log.error(path + " does not reference a valid ER7-encoded HL7v2 message");
//						System.exit(0);
					}
					break;
				case REST:
					value = FileConverter.dumpRESTRequest(path);
					if (value == null) {
						log.error(path + "does not reference a valid REST request");
//						System.exit(0);
					}
				default:
					value = path;
					break;
				}
				files.put(input.getReferencedObject().getKeyword(), value);
			}
		}
		return files;
	}

	private static boolean validateValidatorFile(String fileLocation) {
		log.info("validating " + fileLocation + " against XValidator.xsd");
		try {
			XSDValidation report = GazelleCrossValidatorType.validateXmlFile(fileLocation);
			report.print(log);
			return report.validationPassed();
		} catch (FileNotFoundException e) {
			log.error(e.getMessage(), e);
			return false;
		} catch (IOException e) {
			log.warn(e.getMessage());
			return true;
		}
	}

	private void printHelp(Options options, Exception e) {
		// automatically generate the help statement
		HelpFormatter formatter = new HelpFormatter();
		if (e != null) {
			log.error(e.getMessage());
		}
		if (!helpPrinted) {
			helpPrinted = true;
			formatter.printHelp("gazelle-x-validation-service -validator yourValidator "
					+ "[-files filesToValidate (keyword=path)] [-output reportPath]", options);
		}
	}

	private boolean getParameters(String[] args, Options commandLineOptions) throws Exception {
		CommandLineParser parser = new PosixParser();

		// parse the command line arguments
		CommandLine line = null;
		try {
			line = parser.parse(commandLineOptions, args);
		} catch (ParseException e) {
			printHelp(commandLineOptions, e);
			return false;
		}
		if (line.hasOption(OPT_HELP)) {
			printHelp(commandLineOptions, null);
			return false;
		} else {
			options.put(OPT_VALIDATOR, line.getOptionValue(OPT_VALIDATOR));
			options.put(OPT_OUTPUT, line.getOptionValue(OPT_OUTPUT));
			if (line.hasOption(OPT_FILES)) {
				String files = line.getOptionValue(OPT_FILES);
				String[] entries = files.split(",");
				if (entries.length > 0) {
					for (String entry : entries) {
						String[] values = entry.split("=");
						if (values.length != 2) {
							printHelp(commandLineOptions, new Exception(
									"list of files to validate shall be formatted as keyword1=path1,keyword2=path2,..."));
							return false;
						} else {
							options.put(values[0].trim(), values[1].trim());
						}
					}
				}
			}
		}
		return true;
	}

	private Options getOptions() {
		Options commandLineOptions = new Options();
		commandLineOptions.addOption(new Option(OPT_HELP, "print this message"));
		Option validator = new Option(OPT_VALIDATOR, true,
				"path to the XML file representing the Gazelle Cross Validator");
		validator.setRequired(true);
		commandLineOptions.addOption(validator);
		Option output = new Option(OPT_OUTPUT, true, "path to the XML file to save the report");
		output.setRequired(false);
		commandLineOptions.addOption(output);
		Option files = new Option(OPT_FILES, true,
				"path to the files required for the validation, separated by coma, and formatted as keyword=path");
		files.setRequired(false);
		files.setValueSeparator(',');
		commandLineOptions.addOption(files);
		return commandLineOptions;
	}
}
