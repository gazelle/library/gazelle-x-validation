/*
 * Copyright 2016 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package net.ihe.gazelle.xvalidation.core.model;

import junit.framework.Assert;
import junit.framework.AssertionFailedError;
import net.ihe.gazelle.xvalidation.core.exception.GazelleXValidationException;
import net.ihe.gazelle.xvalidation.core.report.Notification;
import net.ihe.gazelle.xvalidation.core.xpath.XpathUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <b>Class Description : </b>MathComparison<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 24/02/16
 * @class MathComparison
 * @package net.ihe.gazelle.xvalidation.core.model
 * @see anne-gaelle.berge@ihe-europe.net - http://gazelle.ihe.net
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MathComparison", propOrder = {"leftMember", "comparator", "rightMember", "delta"})
@XmlRootElement(name = "MathComparison")
@Entity
@DiscriminatorValue("mathComparison")
public class MathComparison extends AbstractExpression {

    private static Logger log = LoggerFactory.getLogger(BasicOperation.class);

    @XmlElements({ @XmlElement(name = "MathOperationLeft", type = MathOperation.class),
            @XmlElement(name = "SingleMemberLeft", type = SingleMember.class),
            @XmlElement(name = "ConstantMemberLeft", type = ConstantMember.class)})
    @OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "left_member_id")
    private MathMember leftMember;

    @XmlElement(name = "Comparator", required = true)
    @Column(name = "math_comparator")
    private MathComparatorType comparator;


    @XmlElements({ @XmlElement(name = "MathOperationRight", type = MathOperation.class),
            @XmlElement(name = "SingleMemberRight", type = SingleMember.class),
            @XmlElement(name = "ConstantMemberRight", type = ConstantMember.class)})
    @OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "right_member_id")
    private MathMember rightMember;

    @XmlAttribute(name = "delta", required = false)
    @Column(name = "delta")
    private Double delta;

    @XmlTransient
    @Transient
    private org.w3c.dom.Node xmlNodePresentation;

    public MathComparison() {
        super();
    }

    public MathComparison(MathComparatorType operator) {
        super();
        this.comparator = operator;
        this.leftMember = new MathOperation();
        this.rightMember = new MathOperation();
        this.delta = (double) 0;
    }

    public MathComparison(MathComparison operation) {
        super();
        this.comparator = operation.getComparator();
        this.leftMember = leftMember.createCopy();
        this.rightMember = rightMember.createCopy();
        this.delta = operation.getDelta();
    }

    public MathMember getLeftMember() {
        return leftMember;
    }

    public void setLeftMember(MathMember leftMember) {
        this.leftMember = leftMember;
    }

    public MathMember getRightMember() {
        return rightMember;
    }

    public void setRightMember(MathMember rightMember) {
        this.rightMember = rightMember;
    }

    public MathComparatorType getComparator() {
        return comparator;
    }

    public void setComparator(MathComparatorType comparator) {
        this.comparator = comparator;
    }

    public Double getDelta() {
        return delta;
    }

    public void setDelta(Double delta) {
        this.delta = delta;
    }

    @Override
    public Node getXmlNodePresentation() {
        if (xmlNodePresentation == null) {
            xmlNodePresentation = ObjectFactory.getXmlNodePresentation("", "MathComparison", this);
        }
        return xmlNodePresentation;
    }

    @Override
    public void setXmlNodePresentation(Node xmlNodePresentation) {
        this.xmlNodePresentation = xmlNodePresentation;
    }

    @Override
    public Notification evaluate(Map<String, Node> files, ValidatorConfiguration configuration, Level level) {
        Double leftValue;
        Double rightValue;
        StringBuilder leftXPathReport = new StringBuilder();
        StringBuilder rightXPathReport = new StringBuilder();
        StringBuilder xpathReport = new StringBuilder();

        try {
            leftValue = leftMember.evaluate(files, configuration, leftXPathReport);
            rightValue = rightMember.evaluate(files, configuration, rightXPathReport);
        } catch (GazelleXValidationException e){
            return abort("Error evaluating members : " + e.getMessage(), e.getLocator(), "Error evaluating members");
        }

        boolean result;
        switch (comparator) {
            case EQUALS:
                try {
                    Assert.assertEquals(leftValue, rightValue, delta);
                    result =  true;
                } catch (AssertionFailedError e) {
                    result =  false;
                }
                xpathReport.append(leftXPathReport.toString() + " = " + rightXPathReport.toString());
                break;
            case LT:
                result = (leftValue < rightValue);
                xpathReport.append(leftXPathReport.toString() + " < " + rightXPathReport.toString());
                break;
            case LEQ:
                result = (leftValue <= rightValue);
                xpathReport.append(leftXPathReport.toString() + " <= " + rightXPathReport.toString());
                break;
            case GT:
                result = (leftValue > rightValue);
                xpathReport.append(leftXPathReport.toString() + " > " + rightXPathReport.toString());
                break;
            case GEQ:
                result = (leftValue >= rightValue);
                xpathReport.append(leftXPathReport.toString() + " >= " + rightXPathReport.toString());
                break;
            default:
                String message = this.comparator.getValue() + " is not a supported operation";
                log.error(message);
                return abort(message, null, "Unsupported operation : " + comparator.getValue());
        }

        Notification resultNotification;
        if (!result){
            resultNotification = newNotificationForLevel(level);
        } else {
            resultNotification = newNotificationForLevel(null);
        }
        resultNotification.setXpathReport(xpathReport.toString());
        return resultNotification;
    }

    @Override
    public String getEditLink() {
        return "/xvalidation/expressions/mathComparison.xhtml";
    }

    @Override
    public void testCorrectness() throws AssertionFailedError {
        Assert.assertNotNull("[Expression #" + getFakeId() + "] left member is null", leftMember);
        leftMember.testCorrectness();
        Assert.assertNotNull("[Expression #" + getFakeId() + "] right member is null", rightMember);
        rightMember.testCorrectness();
        Assert.assertNotNull("[Expression #" + getFakeId() + "] no comparator defined", comparator);
    }

    @Override
    public ArrayList<String> appliesTo() throws AssertionFailedError {
        testCorrectness();
        ArrayList<String> keywords = new ArrayList<String>();
        ArrayList<String> leftKeywords = leftMember.appliesTo();
        if (leftKeywords != null) {
            keywords.addAll(leftMember.appliesTo());
        }
        ArrayList<String> rightKeywords = rightMember.appliesTo();
        if (rightKeywords != null) {
            for (String key : rightKeywords) {
                if (!keywords.contains(key)) {
                    keywords.add(key);
                }
            }
        }
        return keywords;
    }

    @Override
    public AbstractExpression createCopy() {
        return new MathComparison(this);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (leftMember != null) {
            builder.append('(');
            builder.append(leftMember.toString());
            builder.append(") ");
        }
        builder.append(comparator.getFriendlyName());
        if (rightMember != null) {
            builder.append(" (");
            builder.append(rightMember.toString());
            builder.append(")");
        }
        if (delta != null) {
            builder.append(" &plusmn;");
            builder.append(delta);
        }
        return builder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MathComparison)) return false;
        if (!super.equals(o)) return false;

        MathComparison that = (MathComparison) o;

        if (leftMember != null ? !leftMember.equals(that.leftMember) : that.leftMember != null) return false;
        if (rightMember != null ? !rightMember.equals(that.rightMember) : that.rightMember != null) return false;
        if (comparator != that.comparator) return false;
        return delta != null ? delta.equals(that.delta) : that.delta == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (leftMember != null ? leftMember.hashCode() : 0);
        result = 31 * result + (rightMember != null ? rightMember.hashCode() : 0);
        result = 31 * result + (comparator != null ? comparator.hashCode() : 0);
        result = 31 * result + (delta != null ? delta.hashCode() : 0);
        return result;
    }
}
