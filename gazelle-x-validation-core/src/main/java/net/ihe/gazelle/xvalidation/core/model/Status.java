/**
 * Status.java
 *
 * File generated from the XValidator4::Status uml Enumeration
 * Generated by IHE - europe, gazelle team
 */
package net.ihe.gazelle.xvalidation.core.model;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import net.ihe.gazelle.hql.FilterLabel;

/**
 * Description of the enumeration Status.
 *
 *
 */

@XmlType(name = "Status")
@XmlEnum
@XmlRootElement(name = "Status")
public enum Status {
	@XmlEnumValue("UNDERDEV")
	UNDERDEV("Under development"),
	@XmlEnumValue("AVAILABLE")
	AVAILABLE("Available"),
	@XmlEnumValue("DEPRECATED")
	DEPRECATED("Deprecated"),
	@XmlEnumValue("MAINTENANCE")
	MAINTENANCE("Maintenance");

	private final String value;

	Status(String v) {
		value = v;
	}

	public String value() {
		return value;
	}

	@FilterLabel
	public String getValue() {
		return value;
	}

	public static Status fromValue(String v) {
		for (Status c : Status.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		return null;
	}
	
}