package net.ihe.gazelle.xvalidation.core.xpath;

import java.util.Iterator;
import java.util.List;

import javax.xml.namespace.NamespaceContext;

import net.ihe.gazelle.xvalidation.core.model.Namespace;

public class ValidatorNamespaceContext implements NamespaceContext {

	private List<Namespace> registeredNamespaces;

	public ValidatorNamespaceContext(List<Namespace> namespaces) {
		this.registeredNamespaces = namespaces;
	}

	@Override
	public String getNamespaceURI(String prefix) {
		String uri = null;
		if (registeredNamespaces != null) {
			for (Namespace namespace : registeredNamespaces) {
				if (namespace.getPrefix().equals(prefix)) {
					uri = namespace.getUri();
				} else {
					continue;
				}
			}
		}
		return uri;
	}

	@Override
	public String getPrefix(String namespaceURI) {
		String prefix = null;
		if (registeredNamespaces != null) {
			for (Namespace namespace : registeredNamespaces) {
				if (namespace.getUri().equals(namespaceURI)) {
					prefix = namespace.getPrefix();
				} else {
					continue;
				}
			}
		}
		return prefix;
	}

	@Override
	public Iterator<?> getPrefixes(String namespaceURI) {
		return null;
	}

}
