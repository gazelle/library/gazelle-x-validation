package net.ihe.gazelle.xvalidation.core.model;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Description of the enumeration MessageType.
 *
 */

@XmlType(name = "MessageType")
@XmlEnum
@XmlRootElement(name = "MessageType")
public enum MessageType {

    @XmlEnumValue("REQUEST")
    REQUEST("REQUEST"),
    @XmlEnumValue("RESPONSE")
    RESPONSE("RESPONSE"),
    @XmlEnumValue("ATTACHMENT")
    ATTACHMENT("ATTACHMENT"),
    @XmlEnumValue("MANIFEST")
    MANIFEST("MANIFEST");

    private String value;

    MessageType(String value) {
        this.value=value;
    }

    public String getValue() {
        return value;
    }

    public static MessageType fromName(String v) {
        for (MessageType i : MessageType.values()) {
            if (i.value.equals(v)) {
                return i;
            }
        }
        throw new IllegalArgumentException(v);
    }

}

