package net.ihe.gazelle.xvalidation.core.utils;

import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.parser.DefaultXMLParser;
import ca.uhn.hl7v2.parser.PipeParser;
import ca.uhn.hl7v2.parser.XMLParser;
import com.pixelmed.dicom.AttributeList;
import com.pixelmed.dicom.XMLRepresentationOfDicomObjectFactory;
import net.ihe.gazelle.xvalidation.core.exception.GazelleXValidationException;
import net.ihe.gazelle.xvalidation.core.model.DICOMLibrary;
import net.ihe.gazelle.xvalidation.core.model.Kind;
import net.ihe.gazelle.xvalidation.core.xpath.XpathUtils;
import org.dcm4che2.tool.dcm2xml.Dcm2Xml;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.*;

/**
 * This class defines method to convert the content of some files into XML format
 *
 * @author aberge
 *
 */
public final class FileConverter {

	private static Logger log = LoggerFactory.getLogger(FileConverter.class);

	private static final String USAGE = "Usage : gazelle-x-validation-xmldump filepath HL7v2|dcm4che2|pixelmed";
	private static final String HL7V2 = "hl7v2";

	private FileConverter() {

	}

	/**
	 * Returns a DOM document which represents the HL7 message using XML encoding filePath
	 * 
	 * @return
	 */
	public static Document dumpER7Message(String filePath) {
		BufferedReader reader = null;
		try {
			try {
				reader = new BufferedReader(new FileReader(filePath));
				String line = null;
				StringBuilder stringBuilder = new StringBuilder();
				while ((line = reader.readLine()) != null) {
					stringBuilder.append(line);
					stringBuilder.append("\r");
				}
				String er7Message = stringBuilder.toString();
				XMLParser xmlParser = DefaultXMLParser.getInstanceWithNoValidation();
				PipeParser pipeParser = PipeParser.getInstanceWithNoValidation();
				Message hapiMessage = pipeParser.parse(er7Message);
				Document xmlMessage = xmlParser.encodeDocument(hapiMessage);
				return xmlMessage;
			} catch (FileNotFoundException e) {
				log.error(e.getMessage(), e);
				return null;
			} catch (IOException e) {
				log.error(e.getMessage(), e);
				return null;
			}  finally {
				if (reader != null) {
					reader.close();
				}
			}
		} catch (Exception e) {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException ioException) {
					log.warn(ioException.getMessage(), ioException);
				}
			}
			log.error(e.getMessage(), e);
			return null;
		}
	}

	/**
	 * returns a DOM document which represents the DICOM object (dumped with PixelMed)
	 * 
	 * @return
	 */
	public static Document dumpDICOMObject(String filePath, DICOMLibrary library) {
		DICOMLibrary dicomLib = library;
		if (DICOMLibrary.NA.equals(library)) {
			dicomLib = DICOMLibrary.DCM4CHE2;
		}
		switch (dicomLib) {
		case DCM4CHE2:
			return dumpDICOMObjectWithDcm4chee(filePath);
		case PIXELMED:
			return dumpDICOMObjectWithPixelmed(filePath);
		default:
			return null;
		}
	}

	public static Document dumpDICOMObjectWithPixelmed(String filePath) {
		AttributeList list = new AttributeList();
		try {
			list.read(filePath);
			return new XMLRepresentationOfDicomObjectFactory().getDocument(list);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return null;
		}
	}

	public static Document dumpDICOMObjectWithDcm4chee(String filePath) {
		Dcm2Xml converter = new Dcm2Xml();
		File dicomFile = new File(filePath);
		try {
			File result = File.createTempFile("xmlDump", ".xml");
			converter.convert(dicomFile, result);
			FileInputStream fis = new FileInputStream(result);
			Document doc = XpathUtils.parse(fis);
			fis.close();
			result.delete();
			return doc;
		} catch (IOException e) {
			log.error(e.getMessage(), e);
			return null;
		} catch (TransformerConfigurationException e) {
			log.error(e.getMessage(), e);
			return null;
		} catch (GazelleXValidationException e) {
			log.error(e.getMessage(), e);
			return null;
		}
	}

	/**
	 *
	 * @return
	 */
	public static Document dumpRESTRequest(String filePath) {
		return null;

	}

	/**
	 * The validation engine expects a node or a string representing the path to the file
	 * This methods converts DICOM, HL7v2 and REST to Node or returns the path to the XML file
	 * @param type
	 * @param path
	 * @param dicomLibrary
	 * @return
	 * @throws GazelleXValidationException
	 * @throws FileNotFoundException
	 */
	public static Object file2Object(Kind type, String path, DICOMLibrary dicomLibrary)
			throws GazelleXValidationException, FileNotFoundException {
		Object value = null;
		switch (type) {
		case DICOM:
			value = dumpDICOMObject(path, dicomLibrary);
			if (value == null) {
				throw new GazelleXValidationException(path + " does not reference a valid DICOM object");
			}
			break;
		case HL7V2:
			value = dumpER7Message(path);
			if (value == null) {
				throw new GazelleXValidationException(path + " does not reference a valid ER7-encoded HL7v2 message");
			}
			break;
		case REST:
			value = dumpRESTRequest(path);
			if (value == null) {
				throw new GazelleXValidationException(path + "does not reference a valid REST request");
			}
			break;
		default:
			value = path;
			break;
		}
		return value;
	}

	/**
	 * Converts the file to an XML node
	 * @param type
	 * @param path
	 * @param dicomLibrary
	 * @return
	 * @throws GazelleXValidationException
	 * @throws FileNotFoundException
	 */
	public static Node file2Node(Kind type, String path, DICOMLibrary dicomLibrary) throws GazelleXValidationException,
			FileNotFoundException {
		Object value = file2Object(type, path, dicomLibrary);
		if (value instanceof String) {
			FileInputStream fis = new FileInputStream(path);
			value = XpathUtils.parse(fis);
			try {
				fis.close();
			} catch (IOException e) {
				log.warn("Unable to close stream");
			}
		}
		if (value instanceof Node) {
			return (Node) value;
		} else {
			return null;
		}
	}

	/**
	 * Dumps the input file in XML and displays the resulting XML string
	 * 
	 * @param args
	 *            : fileToDump HL7v2|DICOM
	 * @throws TransformerConfigurationException
	 */
	public static void main(String[] args) { // throws IOException, TransformerException
		try {
			String filePath = null;
			String standard = null;
			if (args.length > 0) {
				filePath = args[0];
			} else {
				log.error(USAGE);
				System.exit(0);
			}
			if (args.length > 1) {
				standard = args[1];
			} else {
				log.error(USAGE);
				System.exit(0);
			}
			Document doc = null;
			if (standard.equalsIgnoreCase(HL7V2)) {
				doc = dumpER7Message(filePath);
			} else if (DICOMLibrary.fromValue(standard) != null) {
				doc = dumpDICOMObject(filePath, DICOMLibrary.fromValue(standard));
			} else {
				log.error(USAGE);
				System.exit(0);
			}
			if (doc != null) {
				ByteArrayOutputStream outStream = new ByteArrayOutputStream();
				XMLRepresentationOfDicomObjectFactory.write(outStream, doc);
				log.info(outStream.toString());
			} else {
				log.error("Unable to dump input file");
			}
		} catch (IOException i) {
			log.error("IOException in FileConverter main method" + i.getMessage());
		} catch (TransformerException t) {
			log.error("TransformerException in FileConverter main method" + t.getMessageAndLocation());
		}
	}
}
