/*
 * Copyright 2016 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package net.ihe.gazelle.xvalidation.core.model;

import junit.framework.AssertionFailedError;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * <b>Class Description : </b>CommonOperations<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 25/02/16
 * @class CommonOperations
 * @package net.ihe.gazelle.xvalidation.core.model
 * @see anne-gaelle.berge@ihe-europe.net - http://gazelle.ihe.net
 */
public interface CommonOperations extends Serializable {

    String getEditLink();

    void testCorrectness() throws AssertionFailedError;

    ArrayList<String> appliesTo() throws AssertionFailedError;

    int getFakeId();

}
