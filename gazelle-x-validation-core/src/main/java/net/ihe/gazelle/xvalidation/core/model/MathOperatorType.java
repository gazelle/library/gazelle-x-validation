/*
 * Copyright 2016 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package net.ihe.gazelle.xvalidation.core.model;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <b>Class Description : </b>MathOperatorType<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 24/02/16
 * @class MathOperatorType
 * @package net.ihe.gazelle.xvalidation.core.model
 * @see anne-gaelle.berge@ihe-europe.net - http://gazelle.ihe.net
 */

@XmlType(name = "MathOperatorType")
@XmlEnum
@XmlRootElement(name = "MathOperatorType")
public enum MathOperatorType {

    @XmlEnumValue("Addition") PLUS ("Addition", "+"),
    @XmlEnumValue("Soustraction") MINUS ("Soustraction", "-"),
    @XmlEnumValue("Multiplication") TIMES ("Multiplication", "*"),
    @XmlEnumValue("Division") DIVIDE ("Division", "/"),
    @XmlEnumValue("Power") POWER("Power", "^");

    private final String value;

    private final String friendlyName;

    MathOperatorType(String value, String friendlyName) {
        this.value = value;
        this.friendlyName = friendlyName;
    }

    public String value() {
        return value;
    }

    public String getValue() {
        return value;
    }

    public static MathOperatorType fromValue(String v) {
        for (MathOperatorType c : MathOperatorType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

    public String getFriendlyName() {
        return friendlyName;
    }
}
