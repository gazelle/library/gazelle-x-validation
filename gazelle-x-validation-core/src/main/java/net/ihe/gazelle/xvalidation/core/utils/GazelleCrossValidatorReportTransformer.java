package net.ihe.gazelle.xvalidation.core.utils;

import net.ihe.gazelle.xvalidation.core.model.GazelleCrossValidatorType;
import net.ihe.gazelle.xvalidation.core.report.GazelleCrossValidationReportType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.util.ResourceBundle;

public final class GazelleCrossValidatorReportTransformer {

	private static Logger log = LoggerFactory.getLogger(GazelleCrossValidatorReportTransformer.class);

	private GazelleCrossValidatorReportTransformer() {

	}

	public static GazelleCrossValidationReportType toObject(InputStream is) throws JAXBException {
		JAXBContext jc = JAXBContext.newInstance(GazelleCrossValidationReportType.class);
		Unmarshaller u = jc.createUnmarshaller();
		return (GazelleCrossValidationReportType) u.unmarshal(is);
	}

	public static void toXml(OutputStream out, GazelleCrossValidationReportType xValidatorReport) throws JAXBException {
		JAXBContext jc = JAXBContext.newInstance(GazelleCrossValidationReportType.class);
		Marshaller m = jc.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
		m.marshal(xValidatorReport, out);
	}

	public static String toXml(GazelleCrossValidationReportType report) {
		if (report != null) {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			String stringReport = null;
			try {
				GazelleCrossValidatorReportTransformer.toXml(baos, report);
				stringReport = baos.toString();
				baos.close();
			} catch (JAXBException e) {
				log.error(e.getMessage(), e);
			} catch (IOException e) {
				log.error(e.getMessage(), e);
			}
			return stringReport;
		} else {
			return null;
		}
	}


	public static void main(String[] args) {
		try {
			FileInputStream fis = null;
			try {
				ResourceBundle bundle = ResourceBundle.getBundle("FilePath");
				String filePath = bundle.getString("filepathReportTransformer");
				fis = new FileInputStream(filePath);
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				GazelleCrossValidatorType xValidator = GazelleCrossValidatorTransformer.toObject(fis);
				GazelleCrossValidatorTransformer.toXml(baos, xValidator);
				log.info(baos.toString());
			} catch (IOException e) {
				log.error(e.getMessage(), e);
			} finally {
				if (fis != null) {
					try {
						fis.close();
					} catch (IOException e) {
						log.error(e.getMessage(), e);
					}
				}
			}
		} catch (JAXBException e) {
			log.error(e.getMessage(), e);
		}
	}
}
