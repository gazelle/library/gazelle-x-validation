/*
 * Copyright 2016 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package net.ihe.gazelle.xvalidation.core.test;

import junit.framework.Assert;
import net.ihe.gazelle.xvalidation.core.exception.GazelleXValidationException;
import net.ihe.gazelle.xvalidation.core.model.*;
import net.ihe.gazelle.xvalidation.core.report.Report;
import net.ihe.gazelle.xvalidation.core.report.Error;
import net.ihe.gazelle.xvalidation.core.xpath.XpathUtils;
import org.apache.commons.io.IOUtils;
import org.junit.BeforeClass;
import org.junit.Test;
import org.w3c.dom.Node;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * <b>Class Description : </b>MathOperationTest<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 24/02/16
 */
public class MathComparisonTest {

    private static MathOperation op1; // worth: 6
    private static MathOperation op2; // worth: 9
    private static final String QUERY = "doc1";
    private static final String RESPONSE = "doc2";
    private static HashMap<String, Node> files;
    private static ValidatorConfiguration validatorConfig;

    @BeforeClass
    public static void initClass() {
        Locator locator1 = new Locator(QUERY, "/*[name()='PRPA_IN201305UV02']/*[name()='controlActProcess']/*[name()='queryByParameter']/*[name()='initialQuantity']/@value");
        Locator locator2 = new Locator(RESPONSE, "/*[name()='PRPA_IN201306UV02']/*[name()='controlActProcess']/*[name()='queryByParameter']/*[name()='initialQuantity']/@value");
        files = new HashMap<String, Node>();
        InputStream queryFile = null;
        InputStream responseFile = null;
        try {
            queryFile = new FileInputStream("src/test/resources/sampleQuery.xml");
            responseFile = new FileInputStream("src/test/resources/sampleResponse.xml");
            files.put(
                    QUERY,
                    XpathUtils.parse(queryFile));
            files.put(
                    RESPONSE,
                    XpathUtils.parse(responseFile));
            op1 = new MathOperation(MathOperatorType.PLUS);
            op1.setMember1(new SingleMember(locator1));
            op1.setMember2(new SingleMember(locator2));
            op2 = new MathOperation(MathOperatorType.TIMES);
            op2.setMember1(new SingleMember(locator1));
            op2.setMember2(new SingleMember(locator2));
            List<Namespace> namespaces = new ArrayList<Namespace>();
            namespaces.add(new Namespace("v3", "urn:hl7-org:v3"));
            validatorConfig = new ValidatorConfiguration();
            validatorConfig.setNamespaces(namespaces);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            IOUtils.closeQuietly(queryFile);
            IOUtils.closeQuietly(responseFile);
        }
    }

    @Test
    public void testEqualsTrue() throws GazelleXValidationException {
        MathComparison comp = new MathComparison(MathComparatorType.EQUALS);
        comp.setLeftMember(op1);
        comp.setRightMember(op1);
        System.out.println(comp.toString());
        Assert.assertTrue("test failed", comp.evaluate(files, validatorConfig, Level.ERROR) instanceof Report);
    }

    @Test
    public void testEqualsFalse() throws GazelleXValidationException {
        MathComparison comp = new MathComparison(MathComparatorType.EQUALS);
        comp.setLeftMember(op1);
        comp.setRightMember(op2);
        System.out.println(comp.toString());
        Assert.assertTrue("test failed", comp.evaluate(files, validatorConfig, Level.ERROR) instanceof Error);
    }

    @Test
    public void testLessThanTrue() throws GazelleXValidationException {
        MathComparison comp = new MathComparison(MathComparatorType.LT);
        comp.setLeftMember(op1);
        comp.setRightMember(op2);
        System.out.println(comp.toString());
        Assert.assertTrue("test failed", comp.evaluate(files, validatorConfig, Level.ERROR) instanceof Report);
    }

    @Test
    public void testLessThanFalse() throws GazelleXValidationException {
        MathComparison comp = new MathComparison(MathComparatorType.LT);
        comp.setLeftMember(op2);
        comp.setRightMember(op1);
        System.out.println(comp.toString());
        Assert.assertTrue("test failed", comp.evaluate(files, validatorConfig, Level.ERROR) instanceof Error);
    }

    @Test
    public void testLessOrEqualTrue() throws GazelleXValidationException {
        MathComparison comp = new MathComparison(MathComparatorType.LEQ);
        comp.setLeftMember(op1);
        comp.setRightMember(op1);
        System.out.println(comp.toString());
        Assert.assertTrue("test failed", comp.evaluate(files, validatorConfig, Level.ERROR) instanceof Report);
    }

    @Test
    public void testLessOrEqualFalse() throws GazelleXValidationException {
        MathComparison comp = new MathComparison(MathComparatorType.LEQ);
        comp.setLeftMember(op2);
        comp.setRightMember(op1);
        System.out.println(comp.toString());
        Assert.assertTrue("test failed", comp.evaluate(files, validatorConfig, Level.ERROR) instanceof Error);
    }

    @Test
    public void testGreaterThanTrue() throws GazelleXValidationException {
        MathComparison comp = new MathComparison(MathComparatorType.GT);
        comp.setLeftMember(op2);
        comp.setRightMember(op1);
        System.out.println(comp.toString());
        Assert.assertTrue("test failed", comp.evaluate(files, validatorConfig, Level.ERROR) instanceof Report);
    }

    @Test
    public void testGreaterThanFalse() throws GazelleXValidationException {
        MathComparison comp = new MathComparison(MathComparatorType.GT);
        comp.setLeftMember(op2);
        comp.setRightMember(op2);
        System.out.println(comp.toString());
        Assert.assertTrue("test failed", comp.evaluate(files, validatorConfig, Level.ERROR) instanceof Error);
    }

    @Test
    public void testGreaterOrEqualTrue() throws GazelleXValidationException {
        MathComparison comp = new MathComparison(MathComparatorType.GEQ);
        comp.setLeftMember(op2);
        comp.setRightMember(op2);
        System.out.println(comp.toString());
        Assert.assertTrue("test failed", comp.evaluate(files, validatorConfig, Level.ERROR) instanceof Report);
    }

    @Test
    public void testGreaterOrEqualFalse() throws GazelleXValidationException {
        MathComparison comp = new MathComparison(MathComparatorType.GEQ);
        comp.setLeftMember(op1);
        comp.setRightMember(op2);
        System.out.println(comp.toString());
        Assert.assertTrue("test failed", comp.evaluate(files, validatorConfig, Level.ERROR) instanceof Error);
    }

    @Test
    public void testWithDelta() throws GazelleXValidationException {
        MathComparison comp = new MathComparison(MathComparatorType.EQUALS);
        comp.setLeftMember(op1);
        comp.setRightMember(op2);
        comp.setDelta((double) 5);
        System.out.println(comp.toString());
        Assert.assertTrue("test failed", comp.evaluate(files, validatorConfig, Level.ERROR) instanceof Report);
    }

    @Test
    public void testForMISe() throws GazelleXValidationException {
        HashMap<String, Node> misefiles = new HashMap<String, Node>();
        String astA = "AST_A";
        String astB = "AST_B";
        try {
            misefiles.put(
                    astA,
                    XpathUtils.parse(new FileInputStream("src/test/resources/MISe/ast62-packetA.xml")));
            misefiles.put(
                    astB,
                    XpathUtils.parse(new FileInputStream("src/test/resources/MISe/ast62-packetB.xml")));
            List<Namespace> namespaces = new ArrayList<Namespace>();
            namespaces.add(new Namespace("default", "com:asterix"));
            ValidatorConfiguration config = new ValidatorConfiguration();
            config.setNamespaces(namespaces);
            SingleMember t1 = new SingleMember(astA, "/*[name()='pdml']/*[name()='packet']/*[name()='proto'][@name='geninfo']/*[name()='field'][@name='timestamp']/@value");
            System.out.println("t1: " + t1.evaluate(misefiles, config, new StringBuilder()));
            SingleMember lat1 = new SingleMember(astA, "/*[name()='pdml']/*[name()='packet']/*[name()='proto'][@name='asterix']/*[name()='field'][@name='asterix.message']/*[name()='field'][@name='asterix.062_105']/*[name()='field'][@name='asterix.062_105_LAT']/@show");
            System.out.println("lat1: " + lat1.evaluate(misefiles, config, new StringBuilder()));
            SingleMember long1 = new SingleMember(astA, "/*[name()='pdml']/*[name()='packet']/*[name()='proto'][@name='asterix']/*[name()='field'][@name='asterix.message']/*[name()='field'][@name='asterix.062_105']/*[name()='field'][@name='asterix.062_105_LON']/@show");
            System.out.println("long1: " + long1.evaluate(misefiles, config, new StringBuilder()));
            SingleMember lat2 = new SingleMember(astB, "/*[name()='pdml']/*[name()='packet']/*[name()='proto'][@name='asterix']/*[name()='field'][@name='asterix.message']/*[name()='field'][@name='asterix.062_105']/*[name()='field'][@name='asterix.062_105_LAT']/@show");
            System.out.println("lat2: " + lat2.evaluate(misefiles, config, new StringBuilder()));
            SingleMember long2 = new SingleMember(astB, "/*[name()='pdml']/*[name()='packet']/*[name()='proto'][@name='asterix']/*[name()='field'][@name='asterix.message']/*[name()='field'][@name='asterix.062_105']/*[name()='field'][@name='asterix.062_105_LON']/@show");
            System.out.println("long2: " + long2.evaluate(misefiles, config, new StringBuilder()));
            SingleMember t2 = new SingleMember(astB, "/*[name()='pdml']/*[name()='packet']/*[name()='proto'][@name='geninfo']/*[name()='field'][@name='timestamp']/@value");
            System.out.println("t2: " + t2.evaluate(misefiles, config, new StringBuilder()));

            MathOperation lats = new MathOperation(MathOperatorType.MINUS);
            lats.setMember1(lat1);
            lats.setMember2(lat2);
            System.out.println("lats: " + lats.evaluate(misefiles, config, new StringBuilder()));

            MathOperation latsSquare = new MathOperation(MathOperatorType.POWER);
            latsSquare.setMember1(lats);
            latsSquare.setMember2(new ConstantMember(2.0));
            System.out.println("latsquare: " + latsSquare.evaluate(misefiles, config, new StringBuilder()));

            MathOperation longs = new MathOperation(MathOperatorType.MINUS);
            longs.setMember1(long1);
            longs.setMember2(long2);
            System.out.println("longs: " + longs.evaluate(misefiles, config, new StringBuilder()));

            MathOperation longsSquare = new MathOperation(MathOperatorType.POWER);
            longsSquare.setMember1(longs);
            longsSquare.setMember2(new ConstantMember(2.0));
            System.out.println("longsSquare: " + longsSquare.evaluate(misefiles, config, new StringBuilder()));

            MathOperation times = new MathOperation(MathOperatorType.MINUS);
            times.setMember1(t1);
            times.setMember2(t2);
            System.out.println("times: " + times.evaluate(misefiles, config, new StringBuilder()));

            MathOperation timesSquare = new MathOperation(MathOperatorType.POWER);
            timesSquare.setMember1(times);
            timesSquare.setMember2(new ConstantMember(2.0));
            System.out.println("timesSquare: " + timesSquare.evaluate(misefiles, config, new StringBuilder()));

            MathOperation add = new MathOperation(MathOperatorType.PLUS);
            add.setMember1(latsSquare);
            add.setMember2(longsSquare);
            System.out.println("add: " + add.evaluate(misefiles, config, new StringBuilder()));

            MathOperation divide = new MathOperation(MathOperatorType.DIVIDE);
            divide.setMember1(add);
            divide.setMember2(timesSquare);

            System.out.println(divide.toString());
            System.out.println(divide.evaluate(misefiles, config, new StringBuilder()));

            MathComparison comp = new MathComparison(MathComparatorType.LT);
            comp.setLeftMember(divide);
            comp.setRightMember(new ConstantMember(3.0));

            Assert.assertTrue(comp.evaluate(misefiles, config, Level.WARNING) instanceof Report);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
