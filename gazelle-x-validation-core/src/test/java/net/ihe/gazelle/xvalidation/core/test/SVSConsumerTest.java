package net.ihe.gazelle.xvalidation.core.test;

import java.util.List;

import net.ihe.gazelle.xvalidation.core.exception.GazelleXValidationException;
import net.ihe.gazelle.xvalidation.core.utils.Concept;
import net.ihe.gazelle.xvalidation.core.utils.SVSConsumer;

import org.junit.Assert;
import org.junit.Test;

public class SVSConsumerTest {
	
	private SVSConsumer svsConsumer = new SVSConsumer();

	public void getConceptsListFromValueSetTest() throws GazelleXValidationException{
		List<Concept> concepts = svsConsumer.getConceptsListFromValueSet("1.3.6.1.4.1.21367.101.101", null);
		Assert.assertNotNull(concepts);
	}
	
}
