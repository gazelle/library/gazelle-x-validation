package net.ihe.gazelle.xvalidation.core.test;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import junit.framework.Assert;
import net.ihe.gazelle.xvalidation.core.engine.StandaloneValidator;

import org.junit.Test;

public class StandaloneValidatorTest {

	@Test
	public void validateDicomWithPixelmedTest() {
		String[] args = new String[7];
		args[0] = "program name";
		args[1] = "-validator";
		args[2] = "src/test/resources/validateDicomWorklist.xml";
		args[3] = "-files";
		args[4] = "MWLQuery=src/test/resources/MWLQuery.dcm,MWL=src/test/resources/dicomWorklist.wl";
		args[5] = "-output";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		String token = sdf.format(new Date());
		args[6] = "/tmp/validateDicomWorklistReport" + token + ".xml";
		StandaloneValidator.main(args);
		File report = new File(args[6]);
		Assert.assertTrue(args[6] + " does not exist", report.exists());
	}
	
	@Test
	public void validateDicomWithDcm4che2Test() {
		String[] args = new String[7];
		args[0] = "program name";
		args[1] = "-validator";
		args[2] = "src/test/resources/dicomManifest1.xml";
		args[3] = "-files";
		args[4] = "RAD-69_REQUEST=src/test/resources/rad69_ok.xml,MANIFEST=src/test/resources/kos.dcm";
		args[5] = "-output";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		String token = sdf.format(new Date());
		args[6] = "/tmp/validateManifestReport" + token + ".xml";
		StandaloneValidator.main(args);
		File report = new File(args[6]);
		Assert.assertTrue(args[6] + " does not exist", report.exists());
	}

	@Test
	public void testDis() {
		String[] args = new String[7];
		args[0] = "program name";
		args[1] = "-validator";
		args[2] = "src/test/resources/integration/X_validator.xml";
		args[3] = "-files";
		args[4] = "CDA_Prescription=src/test/resources/integration/PRE_2014.xml,CDA_Dispensation=src/test/resources/integration/DIS_2014_valid.xml";
		args[5] = "-output";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		String token = sdf.format(new Date());
		args[6] = "/tmp/DispensationReport" + token + ".xml";
		StandaloneValidator.main(args);
		File report = new File(args[6]);
		Assert.assertTrue(args[6] + " does not exist", report.exists());
	}

	@Test
	public void testKereval() {
		String[] args = new String[7];
		args[0] = "program name";
		args[1] = "-validator";
		args[2] = "src/test/resources/integration/ker_validator.xml";
		args[3] = "-files";
		args[4] = "Kereval=src/test/resources/integration/ker_reference.xml,Projects=src/test/resources/integration/ker_projects.xml";
		args[5] = "-output";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		String token = sdf.format(new Date());
		args[6] = "/tmp/KerevalReport" + token + ".xml";
		StandaloneValidator.main(args);
		File report = new File(args[6]);
		Assert.assertTrue(args[6] + " does not exist", report.exists());
	}

}
