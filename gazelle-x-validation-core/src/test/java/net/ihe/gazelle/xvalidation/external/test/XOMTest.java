package net.ihe.gazelle.xvalidation.external.test;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.NamespaceContext;

import junit.framework.Assert;
import junit.framework.AssertionFailedError;
import net.ihe.gazelle.xvalidation.core.model.Locator;
import net.ihe.gazelle.xvalidation.core.model.Namespace;
import net.ihe.gazelle.xvalidation.core.xpath.ValidatorNamespaceContext;
import net.ihe.gazelle.xvalidation.core.xpath.XpathUtils;
import net.sf.saxon.dom.DOMNodeList;
import nu.xom.Attribute;
import nu.xom.Builder;
import nu.xom.Nodes;
import nu.xom.ParsingException;
import nu.xom.ValidityException;
import nu.xom.XPathContext;
import nu.xom.converters.DOMConverter;
import nu.xom.tests.XOMTestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * used to evaluate the XON library
 * 
 * @author aberge
 *
 */

public class XOMTest {

	private static FileInputStream document;
	private static NamespaceContext nsContext;
	private static final Locator location = new Locator("FILE", "v3:PRPA_IN201305UV02/v3:controlActProcess");

	@BeforeClass
	public static void initClass() {
		List<Namespace> namespaces = new ArrayList<Namespace>();
		namespaces.add(new Namespace("v3", "urn:hl7-org:v3"));
		nsContext = new ValidatorNamespaceContext(namespaces);
	}

	@Before
	public void initTest() throws IOException {
		document = new FileInputStream(
				"src/test/resources/sampleQuery.xml");
	}

	@After
	public void determinate() throws IOException {
		document.close();
	}

	@Test
	public void testAssertEqual() throws Exception {
		Document documentDOM = XpathUtils.parse(document);
		DOMNodeList nodes = XpathUtils.getNodesFromDOM(documentDOM, location, nsContext);
		DOMNodeList nodes2 = XpathUtils.getNodesFromDOM(documentDOM, location, nsContext);
		Assert.assertTrue("nodes is null", nodes != null && nodes.getLength() > 0);
		Assert.assertTrue("nodes2 is null", nodes2 != null && nodes2.getLength() > 0);
		nu.xom.Element node1 = DOMConverter.convert((Element)nodes.item(0));
		nu.xom.Element node2 = DOMConverter.convert((Element)nodes2.item(0));
		XOMTestCase.assertEquals(node1, node2);
	}

	@Test
	public void testAssertEqual2() throws Exception {
		Document documentDOM = XpathUtils.parse(document);
		DOMNodeList nodes = XpathUtils.getNodesFromDOM(documentDOM, location, nsContext);
		DOMNodeList nodes2 = XpathUtils.getNodesFromDOM(documentDOM, new Locator("file2", "/v3:PRPA_IN201305UV02/v3:id"), nsContext);
		nu.xom.Element node1 = DOMConverter.convert((Element)nodes.item(0));
		nu.xom.Element node2 = DOMConverter.convert((Element)nodes2.item(0));
		try {
			XOMTestCase.assertEquals(node1, node2);
		} catch (AssertionFailedError e) {
			Assert.assertTrue(true);
		}
	}

	@Test
	public void testAssertEqualFromDifferentFiles() throws Exception {
		Document documentDOM = XpathUtils.parse(document);
		DOMNodeList nodes = XpathUtils.getNodesFromDOM(documentDOM, new Locator("file1", "/v3:PRPA_IN201305UV02/v3:controlActProcess/v3:queryByParameter"), nsContext);
		Document documentDOM2 = XpathUtils.parse(new FileInputStream("src/test/resources/sampleResponse.xml"));
		DOMNodeList nodes2 = XpathUtils.getNodesFromDOM(documentDOM2, new Locator ("file2", "/v3:PRPA_IN201306UV02/v3:controlActProcess/v3:queryByParameter"), nsContext);
		nu.xom.Element node1 = DOMConverter.convert((Element)nodes.item(0));
		nu.xom.Element node2 = DOMConverter.convert((Element)nodes2.item(0));
		XOMTestCase.assertEquals(node1, node2);
	}
	
	@Test
	public void testAssertAttributeEqual() throws Exception {
		Document documentDOM = XpathUtils.parse(document);
		DOMNodeList nodes = XpathUtils.getNodesFromDOM(documentDOM, new Locator("file1", "/v3:PRPA_IN201305UV02/v3:id/@root"), nsContext);
		DOMNodeList nodes2 = XpathUtils.getNodesFromDOM(documentDOM, new Locator("file1", "/v3:PRPA_IN201305UV02/v3:id/@root"), nsContext);
		Attribute node1 = DOMConverter.convert((Attr)nodes.item(0));
		Attribute node2 = DOMConverter.convert((Attr)nodes2.item(0));
		try {
			XOMTestCase.assertEquals(node1, node2);
		} catch (AssertionFailedError e) {
			Assert.assertTrue(true);
		}
	}
	
	@Test
	public void testXONBuilder() throws ValidityException, ParsingException, IOException{
		Builder builder = new Builder(false);
		nu.xom.Document xomDocument = builder.build(document);
		Assert.assertNotNull("No document has been built", xomDocument);
		XPathContext context = new XPathContext("v3", "urn:hl7-org:v3");
		Nodes nodes = xomDocument.query("/v3:PRPA_IN201305UV02/v3:id/@extension", context);
		Assert.assertNotNull("No node selected", nodes);
		Assert.assertEquals("Values are not equal", "35423", nodes.get(0).getValue());
		Nodes nodes2 = xomDocument.query("/v3:PRPA_IN201305UV02/v3:id", context);
		Nodes nodes3 = nodes2.get(0).query("@extension");
		Assert.assertEquals("Values are not equal", "35423", nodes3.get(0).getValue());
	}
}
