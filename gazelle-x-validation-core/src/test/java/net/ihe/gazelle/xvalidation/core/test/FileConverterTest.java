package net.ihe.gazelle.xvalidation.core.test;

import junit.framework.Assert;
import net.ihe.gazelle.xvalidation.core.model.DICOMLibrary;
import net.ihe.gazelle.xvalidation.core.utils.FileConverter;

import org.junit.Test;
import org.w3c.dom.Document;

public class FileConverterTest {

	@Test
	public void dumpER7MessageTest() {
		String filePath = "src/test/resources/rdeMessage.hl7";
		Document hl7Message = FileConverter.dumpER7Message(filePath);
		Assert.assertNotNull("hl7Message has not been dumped correctly", hl7Message);
		Assert.assertEquals("Dump result is not RDE_O11", "RDE_O11", hl7Message.getFirstChild().getLocalName());
		Assert.assertEquals("namespace URI is not urn:hl7-org:v2xml", "urn:hl7-org:v2xml", hl7Message.getFirstChild()
				.getNamespaceURI());
	}

	@Test
	public void dumpDICOMObjectWithPixelMedTest() {
		String filePath = "src/test/resources/dicomWorklist.wl";
		Document dicomWorklist = FileConverter.dumpDICOMObject(filePath, DICOMLibrary.PIXELMED);
		Assert.assertNotNull("DICOM object has not been dumped correctly", dicomWorklist);
		Assert.assertEquals("Dump result is not DicomObject", "DicomObject", dicomWorklist.getFirstChild()
				.getNodeName());
	}

	@Test
	public void dumpDICOMObjectWithDCM4Che2Test() {
		String filePath = "src/test/resources/dicomWorklist.wl";
		Document dicomWorklist = FileConverter.dumpDICOMObject(filePath, DICOMLibrary.DCM4CHE2);
		Assert.assertNotNull("DICOM object has not been dumped correctly", dicomWorklist);
		Assert.assertEquals("Dump result is not dicom", "dicom", dicomWorklist.getFirstChild()
				.getNodeName());
	}
	
	@Test
	public void mainTestWithER7() {
		String filePath = "src/test/resources/rdeMessage.hl7";
		String standard = "HL7v2";
		String[] args = new String[2];
		args[0] = filePath;
		args[1] = standard;
		try {
			FileConverter.main(args);
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}

	@Test
	public void mainTestWithDICOMPixelmed() {
		String filePath = "src/test/resources/dicomWorklist.wl";
		String standard = "pixelmed";
		String[] args = new String[2];
		args[0] = filePath;
		args[1] = standard;
		try {
			FileConverter.main(args);
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}
	
	@Test
	public void mainTestWithDICOMDcm4che2() {
		String filePath = "src/test/resources/dicomWorklist.wl";
		String standard = "dcm4che2";
		String[] args = new String[2];
		args[0] = filePath;
		args[1] = standard;
		try {
			FileConverter.main(args);
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}

	}
}
