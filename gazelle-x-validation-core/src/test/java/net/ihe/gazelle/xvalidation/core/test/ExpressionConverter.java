package net.ihe.gazelle.xvalidation.core.test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javax.xml.bind.JAXBException;

import net.ihe.gazelle.xvalidation.core.model.GazelleCrossValidatorType;
import net.ihe.gazelle.xvalidation.core.model.Rule;
import net.ihe.gazelle.xvalidation.core.utils.GazelleCrossValidatorTransformer;

public class ExpressionConverter {

	public static void main(String[] args) throws FileNotFoundException, JAXBException {
		FileInputStream fis = new FileInputStream(
				"src/test/resources/pdqv3QueryChecker.xml");
		GazelleCrossValidatorType validator = GazelleCrossValidatorTransformer.toObject(fis);
		for (Rule rule : validator.getRules()) {
			System.out.println(rule.getKeyword() + " : ");
			System.out.println(rule.getTextualExpression());
		}
	}
}
