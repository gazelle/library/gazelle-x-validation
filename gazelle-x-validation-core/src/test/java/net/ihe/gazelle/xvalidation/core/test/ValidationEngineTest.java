package net.ihe.gazelle.xvalidation.core.test;

import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;

import junit.framework.Assert;
import net.ihe.gazelle.xvalidation.core.engine.ValidationEngine;
import net.ihe.gazelle.xvalidation.core.model.GazelleCrossValidatorType;
import net.ihe.gazelle.xvalidation.core.utils.GazelleCrossValidatorTransformer;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ValidationEngineTest {

	
	private static Logger log = LoggerFactory.getLogger(ValidationEngineTest.class);
	
	@Test
	public void testValidate() throws Exception{
		FileInputStream fis = new FileInputStream("src/test/resources/pdqv3QueryChecker.xml");
		GazelleCrossValidatorType validator = GazelleCrossValidatorTransformer.toObject(fis);
		fis.close();
		Map<String, Object> files = new HashMap<String, Object>();
		files.put("QUERY", "src/test/resources/sampleQuery.xml");
		files.put("RESPONSE", "src/test/resources/sampleResponse.xml");
		ValidationEngine engine = new ValidationEngine(validator, files);
		engine.validate();
		String validationReport = engine.getReportAsString();
		Assert.assertNotNull("No validation report as been produced", validationReport);
		log.info(engine.getReportAsString());
	}
}
