package net.ihe.gazelle.xvalidation.core.test;

import net.ihe.gazelle.xvalidation.core.exception.GazelleXValidationException;
import net.ihe.gazelle.xvalidation.core.model.*;
import net.ihe.gazelle.xvalidation.core.report.*;
import net.ihe.gazelle.xvalidation.core.report.Error;
import net.ihe.gazelle.xvalidation.core.xpath.XpathUtils;
import net.sf.saxon.dom.DOMNodeList;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.validation.constraints.AssertTrue;
import javax.xml.bind.NotIdentifiableEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExpressionTest {

    private static Logger log = LoggerFactory.getLogger(ExpressionTest.class);

    private static Map<String, Node> files;
    private static final String QUERY = "query";
    private static final String RESPONSE = "response";
    private static final String WHITESPACE = "whitespace";
    private static final String NO_WHITESPACE = "noWhitespace";
    private static final String COMMENT = "comment";
    private static ValidatorConfiguration validatorConfig;


    @BeforeClass
    public static void initTestClass() {
        System.out.println("initializing Class");
        files = new HashMap<String, Node>();
        InputStream queryFile = null;
        InputStream responseFile = null;
        try {
            queryFile = new FileInputStream("src/test/resources/sampleQuery.xml");
            responseFile = new FileInputStream("src/test/resources/sampleResponse.xml");
            InputStream withComment = new FileInputStream("src/test/resources/testWithComment.xml");
            InputStream withoutWhitespaces = new FileInputStream("src/test/resources/withoutWhitespaces.xml");
            InputStream withWhitespaces = new FileInputStream("src/test/resources/testWithWhitespaces.xml");
            if (queryFile.available() > 0 && responseFile.available() > 0) {
                files.put(
                        QUERY,
                        XpathUtils.parse(queryFile));
                files.put(
                        RESPONSE,
                        XpathUtils.parse(responseFile));
                files.put(COMMENT, XpathUtils.parse(withComment));
                files.put(NO_WHITESPACE, XpathUtils.parse(withoutWhitespaces));
                files.put(WHITESPACE, XpathUtils.parse(withWhitespaces));
                List<Namespace> namespaces = new ArrayList<Namespace>();
                namespaces.add(new Namespace("v3", "urn:hl7-org:v3"));
                namespaces.add(new Namespace("soap", "http://www.w3.org/2003/05/soap-envelope"));
                validatorConfig = new ValidatorConfiguration();
                validatorConfig.setNamespaces(namespaces);
            } else {
                System.out.println("Cannot read files");
            }
        } catch (FileNotFoundException e) {
            log.error(e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage());
        } finally {
            IOUtils.closeQuietly(queryFile);
            IOUtils.closeQuietly(responseFile);
        }
    }

    @Test
    public void testNot() {
        Not expression = new Not();
        Present subExpression = new Present();
        subExpression.setLocator(new Locator(QUERY, "/v3:PRPA_IN201305UV02/v3:id"));
        expression.setExpression(subExpression);
        Notification subExpressionResult = subExpression.evaluate(files, validatorConfig, Level.ERROR);
        Notification expressionResult = expression.evaluate(files, validatorConfig, Level.ERROR);
        Assert.assertEquals("PRESENT [query::/v3:PRPA_IN201305UV02/v3:id matched DOMNodeList of size 1 containing following nodes : \n" +
                " - Node of type 1 with name id from path /#document/PRPA_IN201305UV02/id]\n", subExpressionResult.getXpathReport());
        Assert.assertTrue(subExpressionResult instanceof Report);
        Assert.assertEquals("NOT PRESENT [query::/v3:PRPA_IN201305UV02/v3:id matched DOMNodeList of size 1 containing following nodes : \n" +
                " - Node of type 1 with name id from path /#document/PRPA_IN201305UV02/id]\n", expressionResult.getXpathReport());
        Assert.assertTrue(expressionResult instanceof Error);
    }

    public void testInValueSet() {
        InValueSet expression = new InValueSet();
        expression.setValueSetId("1.3.6.1.4.1.21367.101.101");
        expression.setLocator(new Locator(
                        RESPONSE,
                        "/v3:PRPA_IN201306UV02/v3:controlActProcess[1]/v3:queryByParameter[1]/v3:parameterList[1]/v3" +
								":livingSubjectAdministrativeGender[1]/v3:value[1]/@code"));
        Notification result = expression.evaluate(files, validatorConfig, Level.WARNING);
        Assert.assertEquals("IN VALUE-SET (1.3.6.1.4.1.21367.101.101) [response::/v3:PRPA_IN201306UV02/v3:controlActProcess[1]/v3:queryByParameter[1]/v3:parameterList[1]/v3" +
                ":livingSubjectAdministrativeGender[1]/v3:value[1]/@code matched String with value \"M\"]\n", result.getXpathReport());
        Assert.assertTrue(result instanceof Report);
    }

    @Test
    public void testPresent() {
        Present expression = new Present();
        expression.setLocator(new Locator(QUERY, "/v3:PRPA_IN201305UV02/v3:id"));
        Notification result = expression.evaluate(files, validatorConfig, Level.INFO);
        Assert.assertEquals("PRESENT [query::/v3:PRPA_IN201305UV02/v3:id matched DOMNodeList of size 1 containing following nodes : \n" +
                " - Node of type 1 with name id from path /#document/PRPA_IN201305UV02/id]\n", result.getXpathReport());
        Assert.assertTrue(result instanceof Report);
    }

    @Test
    public void testForEach() {
        ForEach expression = new ForEach();
        expression.setVar("element");
        expression.setLocator(new Locator(QUERY, "//v3:id"));
        Present subExpression1 = new Present();
        subExpression1.setLocator(new Locator("element", "@root"));
        Present subExpression2 = new Present();
        subExpression2.setLocator(new Locator("element", "@extension"));
        LogicalOperation and = new LogicalOperation();
        and.setLogicalOperator(LogicalOperatorType.AND);
        and.addExpressionSet(subExpression1);
        and.addExpressionSet(subExpression2);
        expression.setExpressionSet(and);
        Notification result = expression.evaluate(files, validatorConfig, Level.ERROR);
        Assert.assertEquals("[query:://v3:id matched DOMNodeList of size 3 containing following nodes : \n" +
                " - Node of type 1 with name id from path /#document/PRPA_IN201305UV02/id\n" +
                " - Node of type 1 with name id from path /#document/PRPA_IN201305UV02/receiver/device/id\n" +
                " - Node of type 1 with name id from path /#document/PRPA_IN201305UV02/sender/device/id]\n" +
                "\n" +
                "Results forEach :\n" +
                "node 1 :\n" +
                "PRESENT [element::@root matched DOMNodeList of size 1 containing following nodes : \n" +
                " - Node of type 2 with name root from path /root with value : 1.2.840.114350.1.13.0.1.7.1.1]\n" +
                "AND PRESENT [element::@extension matched DOMNodeList of size 1 containing following nodes : \n" +
                " - Node of type 2 with name extension from path /extension with value : 35423]\n" +
                "node 2 :\n" +
                "PRESENT [element::@root matched DOMNodeList of size 1 containing following nodes : \n" +
                " - Node of type 2 with name root from path /root with value : 1.2.840.114350.1.13.999.234]\n" +
                "AND PRESENT [element::@extension matched nothing]\n" +
                "node 3 :\n" +
                "PRESENT [element::@root matched DOMNodeList of size 1 containing following nodes : \n" +
                " - Node of type 2 with name root from path /root with value : 1.2.840.114350.1.13.999.567]\n" +
                "AND PRESENT [element::@extension matched nothing]\n", result.getXpathReport());
        Assert.assertTrue(result instanceof Error);
    }

    @Test
    public void testIn() {
        BasicOperation expression = new BasicOperation();
        expression.setBasicOperator(BasicOperatorType.IN);
        expression.setLocator1(new Locator(QUERY, "/v3:PRPA_IN201305UV02/v3:controlActProcess/v3:queryByParameter"));
        expression.setLocator2(new Locator(RESPONSE, "/v3:PRPA_IN201306UV02/v3:controlActProcess/v3:queryByParameter"));
        Notification result = expression.evaluate(files, validatorConfig, Level.ERROR);
        Assert.assertEquals("[query::/v3:PRPA_IN201305UV02/v3:controlActProcess/v3:queryByParameter matched DOMNodeList of size 1 containing following nodes : \n" +
                " - Node of type 1 with name queryByParameter from path /#document/PRPA_IN201305UV02/controlActProcess/queryByParameter]\n" +
                "IN [response::/v3:PRPA_IN201306UV02/v3:controlActProcess/v3:queryByParameter matched DOMNodeList of size 1 containing following nodes : \n" +
                " - Node of type 1 with name queryByParameter from path /#document/PRPA_IN201306UV02/controlActProcess/queryByParameter]\n", result.getXpathReport());
        Assert.assertTrue(result instanceof Report);
    }

    @Test
    public void testNotIn() {
        BasicOperation expression = new BasicOperation();
        expression.setBasicOperator(BasicOperatorType.IN);
        expression.setLocator2(new Locator(QUERY, "/v3:PRPA_IN201305UV02/v3:controlActProcess"));
        expression.setLocator1(new Locator(RESPONSE, "/v3:PRPA_IN201306UV02/v3:controlActProcess/v3:subject"));
        Notification result = expression.evaluate(files, validatorConfig, Level.ERROR);
        Assert.assertEquals("[response::/v3:PRPA_IN201306UV02/v3:controlActProcess/v3:subject matched DOMNodeList of size 2 containing following " +
                "nodes : \n" +
                " - Node of type 1 with name subject from path /#document/PRPA_IN201306UV02/controlActProcess/subject[0]\n" +
                " - Node of type 1 with name subject from path /#document/PRPA_IN201306UV02/controlActProcess/subject[1]]\n" +
                "IN [query::/v3:PRPA_IN201305UV02/v3:controlActProcess matched DOMNodeList of size 1 containing following nodes : \n" +
                " - Node of type 1 with name controlActProcess from path /#document/PRPA_IN201305UV02/controlActProcess]\n", result.getXpathReport());
        Assert.assertTrue(result instanceof Error);
    }

    @Test
    public void testContain() {
        BasicOperation expression = new BasicOperation();
        expression.setBasicOperator(BasicOperatorType.CONTAIN);
        expression.setLocator2(new Locator(QUERY, "/v3:PRPA_IN201305UV02/v3:controlActProcess/v3:queryByParameter"));
        expression.setLocator1(new Locator(RESPONSE, "/v3:PRPA_IN201306UV02/v3:controlActProcess/v3:queryByParameter"));
        Notification result = expression.evaluate(files, validatorConfig, Level.ERROR);
        Assert.assertEquals("[response::/v3:PRPA_IN201306UV02/v3:controlActProcess/v3:queryByParameter matched DOMNodeList of size 1 containing " +
                "following nodes : \n" +
                " - Node of type 1 with name queryByParameter from path /#document/PRPA_IN201306UV02/controlActProcess/queryByParameter]\n" +
                "CONTAIN [query::/v3:PRPA_IN201305UV02/v3:controlActProcess/v3:queryByParameter matched DOMNodeList of size 1 containing following" +
                " nodes : \n" +
                " - Node of type 1 with name queryByParameter from path /#document/PRPA_IN201305UV02/controlActProcess/queryByParameter]\n", result.getXpathReport());
        Assert.assertTrue(result instanceof Report);
    }

    @Test
    public void testEq() {
        BasicOperation expression = new BasicOperation();
        expression.setBasicOperator(BasicOperatorType.EQUAL);
        expression.setLocator2(new Locator(QUERY, "/v3:PRPA_IN201305UV02/v3:controlActProcess/v3:queryByParameter"));
        expression.setLocator1(new Locator(RESPONSE, "/v3:PRPA_IN201306UV02/v3:controlActProcess/v3:queryByParameter"));
        Notification result = expression.evaluate(files, validatorConfig, Level.WARNING);
        Assert.assertEquals("[response::/v3:PRPA_IN201306UV02/v3:controlActProcess/v3:queryByParameter matched DOMNodeList of size 1 containing " +
                "following nodes : \n" +
                " - Node of type 1 with name queryByParameter from path /#document/PRPA_IN201306UV02/controlActProcess/queryByParameter]\n" +
                "EQUAL [query::/v3:PRPA_IN201305UV02/v3:controlActProcess/v3:queryByParameter matched DOMNodeList of size 1 containing following " +
                "nodes : \n" +
                " - Node of type 1 with name queryByParameter from path /#document/PRPA_IN201305UV02/controlActProcess/queryByParameter]\n", result.getXpathReport());
        Assert.assertTrue(result instanceof Report);
    }

    @Test
    public void testSizeEq() {
        BasicOperation expression = new BasicOperation();
        expression.setBasicOperator(BasicOperatorType.SIZEEQUAL);
        expression.setLocator2(new Locator(QUERY, "/v3:PRPA_IN201305UV02/v3:receiver"));
        expression.setLocator1(new Locator(RESPONSE, "/v3:PRPA_IN201306UV02/v3:sender"));
        Notification result = expression.evaluate(files, validatorConfig, Level.INFO);
        Assert.assertEquals("[response::/v3:PRPA_IN201306UV02/v3:sender matched DOMNodeList of size 1 containing following nodes : \n" +
                " - Node of type 1 with name sender from path /#document/PRPA_IN201306UV02/sender]\n" +
                "SIZE EQUAL [query::/v3:PRPA_IN201305UV02/v3:receiver matched DOMNodeList of size 1 containing following nodes : \n" +
                " - Node of type 1 with name receiver from path /#document/PRPA_IN201305UV02/receiver]\n", result.getXpathReport());
        Assert.assertTrue(result instanceof Report);
    }

    @Test
    public void testSizeGreaterOrEqual() {
        BasicOperation expression = new BasicOperation();
        expression.setBasicOperator(BasicOperatorType.SIZEGREATEROREQUAL);
        expression.setLocator1(new Locator(QUERY,
                "/v3:PRPA_IN201305UV02/v3:controlActProcess/v3:queryByParameter/v3:parameterList/v3:otherIDsScopingOrganization"));
        expression.setLocator2(new Locator(RESPONSE, "/v3:PRPA_IN201306UV02/v3:sender"));
        Notification result = expression.evaluate(files, validatorConfig, Level.ERROR);
        Assert.assertEquals("[query::/v3:PRPA_IN201305UV02/v3:controlActProcess/v3:queryByParameter/v3:parameterList/v3:otherIDsScopingOrganization" +
                " matched DOMNodeList of size 3 containing following nodes : \n" +
                " - Node of type 1 with name otherIDsScopingOrganization from path " +
                "/#document/PRPA_IN201305UV02/controlActProcess/queryByParameter/parameterList/otherIDsScopingOrganization[0]\n" +
                " - Node of type 1 with name otherIDsScopingOrganization from path " +
                "/#document/PRPA_IN201305UV02/controlActProcess/queryByParameter/parameterList/otherIDsScopingOrganization[1]\n" +
                " - Node of type 1 with name otherIDsScopingOrganization from path " +
                "/#document/PRPA_IN201305UV02/controlActProcess/queryByParameter/parameterList/otherIDsScopingOrganization[2]]\n" +
                "SIZE >= [response::/v3:PRPA_IN201306UV02/v3:sender matched DOMNodeList of size 1 containing following nodes : \n" +
                " - Node of type 1 with name sender from path /#document/PRPA_IN201306UV02/sender]\n", result.getXpathReport());
        Assert.assertTrue(result instanceof Report);
    }

    @Test
    public void testSizeLessOrEqual() {
        BasicOperation expression = new BasicOperation();
        expression.setBasicOperator(BasicOperatorType.SIZELESSOREQUAL);
        expression.setLocator2(new Locator(QUERY,
                "/v3:PRPA_IN201305UV02/v3:controlActProcess/v3:queryByParameter/v3:parameterList/v3:otherIDsScopingOrganization"));
        expression.setLocator1(new Locator(RESPONSE, "/v3:PRPA_IN201306UV02/v3:sender"));
        Notification result = expression.evaluate(files, validatorConfig, Level.ERROR);
        Assert.assertEquals("[response::/v3:PRPA_IN201306UV02/v3:sender matched DOMNodeList of size 1 containing following nodes : \n" +
                " - Node of type 1 with name sender from path /#document/PRPA_IN201306UV02/sender]\n" +
                "SIZE <= [query::/v3:PRPA_IN201305UV02/v3:controlActProcess/v3:queryByParameter/v3:parameterList/v3:otherIDsScopingOrganization " +
                "matched DOMNodeList of size 3 containing following nodes : \n" +
                " - Node of type 1 with name otherIDsScopingOrganization from path " +
                "/#document/PRPA_IN201305UV02/controlActProcess/queryByParameter/parameterList/otherIDsScopingOrganization[0]\n" +
                " - Node of type 1 with name otherIDsScopingOrganization from path " +
                "/#document/PRPA_IN201305UV02/controlActProcess/queryByParameter/parameterList/otherIDsScopingOrganization[1]\n" +
                " - Node of type 1 with name otherIDsScopingOrganization from path " +
                "/#document/PRPA_IN201305UV02/controlActProcess/queryByParameter/parameterList/otherIDsScopingOrganization[2]]\n", result.getXpathReport());
        Assert.assertTrue(result instanceof Report);
    }

    @Test
    public void testAnd() {
        Present expression1 = new Present();
        expression1.setLocator(new Locator(QUERY, "/v3:PRPA_IN201305UV02/v3:id"));
        Notification result1 = expression1.evaluate(files, validatorConfig, Level.ERROR);
        Present expression2 = new Present();
        expression2.setLocator(new Locator(RESPONSE, "/v3:PRPA_IN201306UV02/v3:id"));
        Notification result2 = expression2.evaluate(files, validatorConfig, Level.ERROR);
        LogicalOperation and = new LogicalOperation();
        and.addExpressionSet(expression1);
        and.addExpressionSet(expression2);
        and.setLogicalOperator(LogicalOperatorType.AND);
        Notification andResult = and.evaluate(files, validatorConfig, Level.ERROR);
        Assert.assertEquals("PRESENT [query::/v3:PRPA_IN201305UV02/v3:id matched DOMNodeList of size 1 containing following nodes : \n" +
                " - Node of type 1 with name id from path /#document/PRPA_IN201305UV02/id]\n", result1.getXpathReport());
        Assert.assertEquals("PRESENT [response::/v3:PRPA_IN201306UV02/v3:id matched DOMNodeList of size 1 containing following nodes : \n" +
                " - Node of type 1 with name id from path /#document/PRPA_IN201306UV02/id]\n", result2.getXpathReport());
        Assert.assertEquals("PRESENT [query::/v3:PRPA_IN201305UV02/v3:id matched DOMNodeList of size 1 containing following nodes : \n" +
                " - Node of type 1 with name id from path /#document/PRPA_IN201305UV02/id]\n" +
                "AND PRESENT [response::/v3:PRPA_IN201306UV02/v3:id matched DOMNodeList of size 1 containing following nodes : \n" +
                " - Node of type 1 with name id from path /#document/PRPA_IN201306UV02/id]\n", andResult.getXpathReport());
        Assert.assertEquals((result1 instanceof Report && result2 instanceof Report), andResult instanceof Report);
    }

    @Test
    public void testOr() {
        Present expression1 = new Present();
        expression1.setLocator(new Locator(QUERY, "/v3:PRPA_IN201305UV02/v3:id"));
        Notification result1 = expression1.evaluate(files, validatorConfig, Level.WARNING);
        Present expression2 = new Present();
        expression2.setLocator(new Locator(RESPONSE, "/v3:PRPA_IN201306UV02/v3:id2"));
        Notification result2 = expression2.evaluate(files, validatorConfig, Level.INFO);
        LogicalOperation or = new LogicalOperation();
        or.addExpressionSet(expression1);
        or.addExpressionSet(expression2);
        or.setLogicalOperator(LogicalOperatorType.OR);
        Notification orResult = or.evaluate(files, validatorConfig, Level.INFO);
        Assert.assertEquals("PRESENT [query::/v3:PRPA_IN201305UV02/v3:id matched DOMNodeList of size 1 containing following nodes : \n" +
                " - Node of type 1 with name id from path /#document/PRPA_IN201305UV02/id]\nOR " +
                "PRESENT [response::/v3:PRPA_IN201306UV02/v3:id2 matched nothing]\n", orResult.getXpathReport());
        Assert.assertEquals((result1 instanceof Report || result2 instanceof Report), orResult instanceof Report);
    }

    @Test
    public void testXor() {
        Present expression1 = new Present();
        expression1.setLocator(new Locator(QUERY, "/v3:PRPA_IN201305UV02/v3:id"));
        Notification result1 = expression1.evaluate(files, validatorConfig, Level.INFO);
        Present expression2 = new Present();
        expression2.setLocator(new Locator(RESPONSE, "/v3:PRPA_IN201306UV02/v3:id"));
        Notification result2 = expression2.evaluate(files, validatorConfig, Level.ERROR);
        LogicalOperation xor = new LogicalOperation();
        xor.addExpressionSet(expression1);
        xor.addExpressionSet(expression2);
        xor.setLogicalOperator(LogicalOperatorType.XOR);
        Notification xorResult = xor.evaluate(files, validatorConfig, Level.ERROR);
        Assert.assertEquals("PRESENT [query::/v3:PRPA_IN201305UV02/v3:id matched DOMNodeList of size 1 containing following nodes : \n" +
                " - Node of type 1 with name id from path /#document/PRPA_IN201305UV02/id]\n" +
                "XOR PRESENT [response::/v3:PRPA_IN201306UV02/v3:id matched DOMNodeList of size 1 containing following nodes : \n" +
                " - Node of type 1 with name id from path /#document/PRPA_IN201306UV02/id]\n", xorResult.getXpathReport());
        Assert.assertEquals((result1 instanceof Report ^ result2 instanceof Report), xorResult instanceof Report);
    }

    @Test
    public void testTrue() {
        True expression = new True();
        expression.setXpath(new Locator(QUERY, "//v3:id/@extension = '35423'"));
        Notification result = expression.evaluate(files, validatorConfig, Level.ERROR);
        Assert.assertEquals("[query:://v3:id/@extension = '35423' matched boolean with value true]\n", result.getXpathReport());
        Assert.assertTrue(result instanceof Report);
    }

    @Test
    public void testIgnoreWhitespacesAndTabs() {
        Locator locatorWithWhitespace = new Locator(WHITESPACE, "//v3:queryByParameter");
        Locator locatorWithoutWhitespace = new Locator(NO_WHITESPACE, "//v3:queryByParameter");
        BasicOperation operation = new BasicOperation();
        operation.setBasicOperator(BasicOperatorType.EQUAL);
        operation.setLocator1(locatorWithWhitespace);
        operation.setLocator2(locatorWithoutWhitespace);
        Notification result = operation.evaluate(files, validatorConfig, Level.ERROR);
        Assert.assertEquals("[whitespace:://v3:queryByParameter matched DOMNodeList of size 1 containing following nodes : \n" +
                " - Node of type 1 with name queryByParameter from path " +
                "/#document/soap:Envelope/soap:Body/PRPA_IN201310UV02/controlActProcess/queryByParameter]\n" +
                "EQUAL [noWhitespace:://v3:queryByParameter matched DOMNodeList of size 1 containing following nodes : \n" +
                " - Node of type 1 with name queryByParameter from path " +
                "/#document/env:Envelope/env:Body/PRPA_IN201309UV02/controlActProcess/queryByParameter]\n", result.getXpathReport());
        Assert.assertTrue(result instanceof Report);
    }

    @Test
    public void testIgnoreComments() {
        BasicOperation operation = new BasicOperation();
        operation.setBasicOperator(BasicOperatorType.EQUAL);
        operation.setLocator1(new Locator(COMMENT, "//v3:queryByParameter"));
        operation.setLocator2(new Locator(WHITESPACE, "//v3:queryByParameter"));
        Notification result = operation.evaluate(files, validatorConfig, Level.WARNING);
        Assert.assertEquals("[comment:://v3:queryByParameter matched DOMNodeList of size 1 containing following nodes : \n" +
                " - Node of type 1 with name queryByParameter from path " +
                "/#document/soap:Envelope/soap:Body/PRPA_IN201310UV02/controlActProcess/queryByParameter]\n" +
                "EQUAL [whitespace:://v3:queryByParameter matched DOMNodeList of size 1 containing following nodes : \n" +
                " - Node of type 1 with name queryByParameter from path " +
                "/#document/soap:Envelope/soap:Body/PRPA_IN201310UV02/controlActProcess/queryByParameter]\n", result.getXpathReport());
        Assert.assertTrue(result instanceof Report);
    }

    @Test
    public void testEquivalentXPath(){
        BasicOperation expression = new BasicOperation();
        expression.setBasicOperator(BasicOperatorType.EQUAL);
        expression.setLocator2(new Locator(QUERY, "/v3:PRPA_IN201305UV02/v3:controlActProcess/v3:queryByParameter"));
        expression.setLocator1(new Locator(QUERY, "//v3:queryByParameter"));
        Notification result = expression.evaluate(files, validatorConfig, Level.WARNING);
        Assert.assertEquals("[query:://v3:queryByParameter matched DOMNodeList of size 1 containing following nodes : \n" +
                " - Node of type 1 with name queryByParameter from path /#document/PRPA_IN201305UV02/controlActProcess/queryByParameter]\n" +
                "EQUAL [query::/v3:PRPA_IN201305UV02/v3:controlActProcess/v3:queryByParameter matched DOMNodeList of size 1 containing following " +
                "nodes : \n" +
                " - Node of type 1 with name queryByParameter from path /#document/PRPA_IN201305UV02/controlActProcess/queryByParameter]\n", result.getXpathReport());
        Assert.assertTrue(result instanceof Report);
    }

    @Test
    public void testXPathDoesNotSelectAnything(){
        BasicOperation expression = new BasicOperation();
        expression.setBasicOperator(BasicOperatorType.EQUAL);
        expression.setLocator2(new Locator(QUERY, "/v3:PRPA_IN201305UV02/v3:controlAct"));
        expression.setLocator1(new Locator(RESPONSE, "/v3:PRPA_IN201306UV02/v3:controlActProcess/v3:queryByParameter"));
        Notification result = expression.evaluate(files, validatorConfig, Level.WARNING);
        Assert.assertEquals("[query::/v3:PRPA_IN201305UV02/v3:controlAct matched DOMNodeList of size 0]\n", result.getXpathReport());
        Assert.assertTrue(result instanceof Aborted);
    }
}
