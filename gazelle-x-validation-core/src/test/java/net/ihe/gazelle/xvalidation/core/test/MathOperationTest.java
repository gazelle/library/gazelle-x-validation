/*
 * Copyright 2016 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package net.ihe.gazelle.xvalidation.core.test;

import junit.framework.Assert;
import net.ihe.gazelle.xvalidation.core.exception.GazelleXValidationException;
import net.ihe.gazelle.xvalidation.core.model.*;
import net.ihe.gazelle.xvalidation.core.xpath.XpathUtils;
import org.apache.commons.io.IOUtils;
import org.junit.BeforeClass;
import org.junit.Test;
import org.w3c.dom.Node;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * <b>Class Description : </b>MathOperationTest<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 24/02/16
 * @class MathOperationTest
 * @package net.ihe.gazelle.xvalidation.core.test
 * @see anne-gaelle.berge@ihe-europe.net - http://gazelle.ihe.net
 */
public class MathOperationTest {

    private static SingleMember member1;
    private static SingleMember member2;
    private static final String QUERY = "doc1";
    private static final String RESPONSE = "doc2";
    private static HashMap<String, Node> files;
    private static ValidatorConfiguration validatorConfig;

    @BeforeClass
    public static void initClass() {
        Locator locator1 = new Locator(QUERY, "/*[name()='PRPA_IN201305UV02']/*[name()='controlActProcess']/*[name()='queryByParameter']/*[name()='initialQuantity']/@value");
        Locator locator2 = new Locator(RESPONSE, "/*[name()='PRPA_IN201306UV02']/*[name()='controlActProcess']/*[name()='queryByParameter']/*[name()='initialQuantity']/@value");
        member1 = new SingleMember(locator1);
        member2 = new SingleMember(locator2);
        files = new HashMap<String, Node>();
        InputStream queryFile = null;
        InputStream responseFile = null;
        try {
            queryFile = new FileInputStream("src/test/resources/sampleQuery.xml");
            responseFile = new FileInputStream("src/test/resources/sampleResponse.xml");
            files.put(
                    QUERY,
                    XpathUtils.parse(queryFile));
            files.put(
                    RESPONSE,
                    XpathUtils.parse(responseFile));
            List<Namespace> namespaces = new ArrayList<Namespace>();
            namespaces.add(new Namespace("v3", "urn:hl7-org:v3"));
            validatorConfig = new ValidatorConfiguration();
            validatorConfig.setNamespaces(namespaces);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            IOUtils.closeQuietly(queryFile);
            IOUtils.closeQuietly(responseFile);
        }
    }

    @Test
    public void testPlus() throws GazelleXValidationException {
        MathOperation op = new MathOperation(MathOperatorType.PLUS);
        op.setMember1(member1);
        op.setMember2(member2);
        System.out.println(op.toString());
        Assert.assertEquals((double) 6, op.evaluate(files, validatorConfig, new StringBuilder()));
    }

    @Test
    public void testSingle() throws GazelleXValidationException {
        System.out.println(member1.toString());
        Assert.assertEquals((double) 3, member1.evaluate(files, validatorConfig, new StringBuilder()));
    }

    @Test
    public void testMinus() throws GazelleXValidationException {
        MathOperation op = new MathOperation(MathOperatorType.MINUS);
        op.setMember1(member1);
        op.setMember2(member2);
        System.out.println(op.toString());
        Assert.assertEquals((double) 0, op.evaluate(files, validatorConfig, new StringBuilder()));
    }

    @Test
    public void testTimes() throws GazelleXValidationException {
        MathOperation op = new MathOperation(MathOperatorType.TIMES);
        op.setMember1(member1);
        op.setMember2(member2);
        System.out.println(op.toString());
        Assert.assertEquals((double) 9, op.evaluate(files, validatorConfig, new StringBuilder()));
    }

    @Test
    public void testDivide() throws GazelleXValidationException {
        MathOperation op = new MathOperation((MathOperatorType.DIVIDE));
        op.setMember2(member2);
        op.setMember1(member1);
        System.out.println(op.toString());
        Assert.assertEquals((double) 1, op.evaluate(files, validatorConfig, new StringBuilder()));
    }

    @Test
    public void testPlusWithFixedXPath() throws GazelleXValidationException{
        MathOperation op = new MathOperation(MathOperatorType.PLUS);
        op.setMember1(member1);
        op.setMember2(new ConstantMember((double) 10));
        op.evaluate(files, validatorConfig, new StringBuilder());
        System.out.println(op.toString());
        Assert.assertEquals((double) 13, op.evaluate(files, validatorConfig, new StringBuilder()));
    }

    @Test
    public void testPower() throws GazelleXValidationException{
        MathOperation op = new MathOperation(MathOperatorType.POWER);
        op.setMember1(member1);
        op.setMember2(new ConstantMember((double) 3));
        op.evaluate(files, validatorConfig, new StringBuilder());
        System.out.println(op.toString());
        Assert.assertEquals((double) 27, op.evaluate(files, validatorConfig, new StringBuilder()));
    }

}
