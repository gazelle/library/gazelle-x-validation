package net.ihe.gazelle.xvalidation.external.test;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.NamespaceContext;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import javax.xml.xpath.XPathFactoryConfigurationException;

import junit.framework.Assert;
import net.ihe.gazelle.xvalidation.core.model.Namespace;
import net.ihe.gazelle.xvalidation.core.xpath.ValidatorNamespaceContext;
import net.ihe.gazelle.xvalidation.core.xpath.XpathUtils;
import net.sf.saxon.dom.DOMNodeList;
import net.sf.saxon.lib.NamespaceConstant;
import net.sf.saxon.xpath.XPathFactoryImpl;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

public class XPathTest {

	private static FileInputStream document;
	private static NamespaceContext nsContext;
	private static final String location = "v3:PRPA_IN201305UV02/v3:controlActProcess";
	
	@BeforeClass
	public static void initClass(){
		List<Namespace> namespaces = new ArrayList<Namespace>();
		namespaces.add(new Namespace("v3", "urn:hl7-org:v3"));
		namespaces.add(new Namespace("titi", "urn:hl7-org:v3"));
		namespaces.add(new Namespace("fn", "http://www.w3.org/2005/xpath-functions"));
		nsContext = new ValidatorNamespaceContext(namespaces);
	}
	
	@Before
	public void initTest() throws IOException{
		document = new FileInputStream("src/test/resources/sampleQuery.xml");
	}
		
	@After
	public void determinate() throws IOException{
		document.close();
	}
	
	@Test
	public void testGetStringWithInputSource() throws XPathFactoryConfigurationException, XPathExpressionException{
		InputSource inputSource = new InputSource(document);

		XPathFactoryImpl factory = XpathUtils.getFactory();

		XPath xpath = factory.newXPath();
		xpath.setNamespaceContext(nsContext);
		String value = (String) xpath.evaluate("/v3:PRPA_IN201305UV02/v3:titi", inputSource, XPathConstants.STRING);
		Assert.assertNotNull("returned value is null", value);
		Assert.assertEquals("returned value is equal to " + value, "", value);
	}
	
	@Test
	public void testGetNodeSetWithInputSource() throws XPathFactoryConfigurationException, XPathExpressionException{
		InputSource inputSource = new InputSource(document);
		XPathFactoryImpl factory = XpathUtils.getFactory();

		XPath xpath = factory.newXPath();
		xpath.setNamespaceContext(nsContext);
		ArrayList<?> value = (ArrayList<?>) xpath.evaluate("//v3:id", inputSource, XPathConstants.NODESET);
		for (Object item : value){
			System.out.println(item.getClass().getCanonicalName());
		}
		System.out.println("nb of nodes: " + value.size());
		Assert.assertNotNull("returned value is null", value);
		Assert.assertTrue("nb of item : " + value.size(), value.size() > 0);
	}
	
	@Test
	public void testGetNodeSetWithNode() throws Exception{
		Document domDocument = XpathUtils.parse(document);
		XPathFactoryImpl factory = XpathUtils.getFactory();

		XPath xpath = factory.newXPath();
		xpath.setNamespaceContext(nsContext);
		DOMNodeList value = (DOMNodeList) xpath.evaluate("//v3:id", domDocument, XPathConstants.NODESET);
		System.out.println("nb of nodes: " + value.getLength());
		Assert.assertNotNull("returned value is null", value);
		Assert.assertTrue("nb of item : " + value.getLength(), value.getLength() > 0);
	}
}
